/**
 * $Revision$
 * $Date$
 *
 * Copyright 2006-2010 Daniel Henninger.  All rights reserved.
 *
 * This software is published under the terms of the GNU Public License (GPL),
 * a copy of which is included in this distribution.
 */

package net.sf.kraken.protocols.facebook;

import java.util.ArrayList;

import net.sf.kraken.BaseTransport;
import net.sf.kraken.registration.Registration;
import net.sf.kraken.session.TransportSession;
import net.sf.kraken.type.ChatStateType;
import net.sf.kraken.type.ConnectionFailureReason;
import net.sf.kraken.type.PresenceType;
import net.sf.kraken.type.TransportLoginStatus;

import org.apache.log4j.Logger;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.interceptor.InterceptorManager;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.JID;
import org.xmpp.packet.Packet;
import org.xmpp.packet.Presence;

/**
 * Handles XMPP transport session.
 *
 * @author Daniel Henninger
 * @author Mehmet Ecevit
 */
public class FacebookSession extends TransportSession<FacebookBuddy> {

	static Logger Log = Logger.getLogger(FacebookSession.class);
	
    public FacebookSession(Registration registration, JID jid,
			BaseTransport<FacebookBuddy> transport, Integer priority) {
		
    	super(registration, jid, transport, priority);
    }

	@Override
	public void updateStatus(PresenceType presenceType, String verboseStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addContact(JID jid, String nickname, ArrayList<String> groups) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeContact(FacebookBuddy contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateContact(FacebookBuddy contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void acceptAddContact(JID jid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendMessage(JID jid, String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendChatState(JID jid, ChatStateType chatState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendBuzzNotification(JID jid, String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateLegacyAvatar(String type, byte[] data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void logIn(PresenceType presenceType, String verboseStatus) {
		
		String serverName = ComponentManagerFactory.getComponentManager().getServerName();
		
		String transportType = this.getTransport().getType().toString();
		Presence online = new Presence();
    	online.setPriority(1);
    	online.setStatus("Online");
    	
    	online.setFrom(getJID());
    	online.setTo(transportType + "." + serverName);
    	
    	XMPPServer.getInstance().getPresenceRouter().route(online);
    	
//    	InterceptorManager.getInstance().addUserInterceptor(getJID().toBareJID(), 0, this);
	}

	@Override
	public void logOut() {
		
		String serverName = ComponentManagerFactory.getComponentManager().getServerName();
		
		String transportType = this.getTransport().getType().toString();
		Presence offline = new Presence();
		offline.setType(Presence.Type.unavailable);
    	
    	offline.setFrom(getJID());
    	offline.setTo(transportType + "." + serverName);
    	
    	XMPPServer.getInstance().getPresenceRouter().route(offline);
		
		cleanUp();
        sessionDisconnectedNoReconnect(null);
	}

	@Override
	public void cleanUp() {
	}
}
