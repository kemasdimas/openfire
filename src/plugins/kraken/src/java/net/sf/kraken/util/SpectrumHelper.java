package net.sf.kraken.util;

import java.util.HashMap;
import java.util.Map;

import net.sf.kraken.type.TransportType;

import org.dom4j.Element;
import org.jivesoftware.openfire.SessionManager;
import org.jivesoftware.openfire.XMPPServer;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.component.IQResultListener;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;

public class SpectrumHelper {
	
	private static SpectrumHelper instance;
	
	public static SpectrumHelper getInstance() {
		if (instance == null) {
			instance = new SpectrumHelper();
		}
		
		return instance;
	}
	
	private boolean isRemoteServer(TransportType type) {
		
		// Don't invoke spectrum if no S2S connection, otherwise the registration will be handled twice!
    	SessionManager sessionManager = SessionManager.getInstance();
        String fullJID = type.toString() + "." + XMPPServer.getInstance().getServerInfo().getXMPPDomain();
		
		if (sessionManager.getComponentSession(fullJID) == null) {
        	return false;
        } else {
        	return true;
        }
	}
	
	public void invokeSpectrumRegistration(JID jid, TransportType type, String username, String password) {
    	
		if (!isRemoteServer(type)) {
    		return;
    	}
        
    	String bareJid = jid.toBareJID();
    	String serverName = ComponentManagerFactory.getComponentManager().getServerName();
    	
    	Map<String, String> attributes = new HashMap<String, String>();
    	if (username != null) {
            attributes.put("username", username);
        }
        if (password != null) {
            attributes.put("password", password);
        }
        // registration.setAttributes(attributes);
        
        IQ registration = new IQ();
        registration.setType(IQ.Type.set);
        registration.setTo(type.toString() + "." + serverName);
        registration.setFrom(bareJid);
        
        Element query = registration.setChildElement("query", "jabber:iq:register");
        if (attributes.size() > 0) {
        	for (String name : attributes.keySet()) {
        		String value = attributes.get(name);
        		
        		query.addElement(name).setText(value);
        	}
        }
        
        query.addElement("x", "jabber:iq:gateway:register");

        // TODO: Seharusnya ada handling IQ timeout
//        XMPPServer.getInstance().getIQRouter().addIQResultListener(registration.getID(), new IQResultListener() {
//			
//			@Override
//			public void receivedAnswer(IQ iq) {
//			}
//			
//			@Override
//			public void answerTimeout(String id) {
//				System.out.println("DELETE THE USER!");
//			}
//		}, 2000);
        
        XMPPServer.getInstance().getIQRouter().route(registration);
    }
	
	public void invokeSpectrumDeregistration(JID jid, TransportType type) {
    	
    	if (!isRemoteServer(type)) {
    		return;
    	}
    	
    	String bareJid = jid.toBareJID();
    	String serverName = ComponentManagerFactory.getComponentManager().getServerName();
    	
    	Map<String, String> attributes = new HashMap<String, String>();
    	attributes.put("remove", "");
        
        IQ registration = new IQ();
        registration.setType(IQ.Type.set);
        registration.setTo(type.toString() + "." + serverName);
        registration.setFrom(bareJid);
        
        Element query = registration.setChildElement("query", "jabber:iq:register");
        if (attributes.size() > 0) {
        	for (String name : attributes.keySet()) {
        		String value = attributes.get(name);
        		
        		query.addElement(name).setText(value);
        	}
        }
        
        XMPPServer.getInstance().getIQRouter().route(registration);
    }
	
}
