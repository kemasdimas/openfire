/**
 * $Revision$
 * $Date$
 *
 * Copyright 2006-2010 Daniel Henninger.  All rights reserved.
 *
 * This software is published under the terms of the GNU Public License (GPL),
 * a copy of which is included in this distribution.
 */

package net.sf.kraken.protocols.facebook;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import net.sf.kraken.BaseTransport;
import net.sf.kraken.protocols.xmpp.packet.AttentionExtension;
import net.sf.kraken.registration.Registration;
import net.sf.kraken.session.TransportSession;
import net.sf.kraken.type.ConnectionFailureReason;
import net.sf.kraken.type.PresenceType;
import net.sf.kraken.type.TransportLoginStatus;

import org.jivesoftware.openfire.session.Session;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.util.LocaleUtils;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;
import org.xmpp.packet.Packet;
import org.xmpp.packet.Presence;

/**
 * XMPP Transport Interface.
 *
 * This handles the bulk of the XMPP work via BaseTransport and provides
 * some gateway specific interactions.
 *
 * @author Daniel Henninger
 * @author Mehmet Ecevit
 */
public class FacebookTransport extends BaseTransport<FacebookBuddy> {

	private static final String COMPONENT_JID 	= "facebook." + ComponentManagerFactory.getComponentManager().getServerName();
	private static final long COMPONENT_TIMEOUT	= 10 * 60 * 1000; 
	
	private Map<String, FacebookSession> sessions;
	private TimerTask facebookAutoLoginTimer;
	
	public FacebookTransport() {
		sessions = new HashMap<String, FacebookSession>();
		
		// Auto login after 10 minutes
		facebookAutoLoginTimer = new TimerTask() {
			@Override
			public void run() {
				Set<String> keys = sessions.keySet();
				
				for (Object key : keys) {
					FacebookSession fSession = sessions.get(key);
					fSession.logIn(PresenceType.available, "");
					
					System.out.println("[facebook] Re-logging in: " + fSession.getRegistration().getUsername());
				}
			}
		};
		
		(new Timer(true)).scheduleAtFixedRate(facebookAutoLoginTimer, COMPONENT_TIMEOUT, COMPONENT_TIMEOUT);
	}
	
    static {
        final ProviderManager pm = ProviderManager.getInstance();
        pm.addExtensionProvider(AttentionExtension.ELEMENT_NAME, AttentionExtension.NAMESPACE, new AttentionExtension.Provider());
    }
    
    /*
     * @see net.sf.kraken.BaseTransport#getTerminologyUsername()
     */
	@Override
    public String getTerminologyUsername() {
        return LocaleUtils.getLocalizedString("gateway." + getType().toString() + ".username", "kraken");
    }

    /*
     * @see net.sf.kraken.BaseTransport#getTerminologyPassword()
     */
	@Override
    public String getTerminologyPassword() {
        return LocaleUtils.getLocalizedString("gateway." + getType().toString() + ".password", "kraken");
    }

    /*
     * @see net.sf.kraken.BaseTransport#getTerminologyNickname()
     */
	@Override
    public String getTerminologyNickname() {
        return null;
    }

    /*
     * @see net.sf.kraken.BaseTransport#getTerminologyRegistration()
     */
	@Override
    public String getTerminologyRegistration() {
        return LocaleUtils.getLocalizedString("gateway." + getType().toString() + ".registration", "kraken");
    }

    /*
     * @see net.sf.kraken.BaseTransport#isPasswordRequired()
     */
	@Override
    public Boolean isPasswordRequired() { return true; }

    /*
     * @see net.sf.kraken.BaseTransport#isNicknameRequired()
     */
	@Override
    public Boolean isNicknameRequired() { return false; }

    /*
     * @see net.sf.kraken.BaseTransport#isUsernameValid(String)
     */
	@Override
    public Boolean isUsernameValid(String username) {
        return true;
    }

    /**
     * Handles creating an OSCAR session and triggering a login.
     *
     * @param registration Registration information to be used to log in.
     * @param jid JID that is logged into the transport.
     * @param presenceType Type of presence.
     * @param verboseStatus Longer status description.
     */
	@Override
    public TransportSession<FacebookBuddy> registrationLoggedIn(Registration registration, JID jid, PresenceType presenceType, String verboseStatus, Integer priority) {
		TransportSession<FacebookBuddy> session = new FacebookSession(registration, jid, this, priority);
        // session.setLoginStatus(TransportLoginStatus.LOGGING_IN);
        session.logIn(presenceType, verboseStatus);
        
        sessions.put(jid.getNode(), (FacebookSession) session);
        
        return session;
	}

    /**
     * Handles logging out of a Yahoo session.
     *
     * @param session The session to be disconnected.
     */
	@Override
    public void registrationLoggedOut(TransportSession<FacebookBuddy> session) {
		sessions.remove(session.getJID().getNode());
		
        session.setLoginStatus(TransportLoginStatus.LOGGING_OUT);
        session.logOut();
    }
	
	@Override
	public void interceptPacket(Packet packet, Session session,
			boolean incoming, boolean processed) {
		
		
		super.interceptPacket(packet, session, incoming, processed);
		
		// Custom facebook interceptor
		if (packet.getFrom().toBareJID().equals(COMPONENT_JID)) {
			if (packet instanceof Presence) {
				FacebookSession transportSession = sessions.get(packet.getTo().getNode());
				
				if (transportSession != null) {
					Presence pres = (Presence) packet;
					
					if (pres.getType() == Presence.Type.unavailable && !pres.getStatus().equalsIgnoreCase("Connecting")) {
						transportSession.setFailureStatus(ConnectionFailureReason.USERNAME_OR_PASSWORD_INCORRECT);
						transportSession.setLoginStatus(TransportLoginStatus.LOGGING_OUT);
						
						registrationLoggedOut(transportSession);
					} else if (pres.getStatus().equalsIgnoreCase("Online")) {
						transportSession.setLoginStatus(TransportLoginStatus.LOGGED_IN);
					}
				}
			} else if (packet instanceof IQ) {
				IQ iq = (IQ) packet;
				if (iq.getType() == IQ.Type.result) {
					(new Date()).getTime();
				}
			}
		}
	}
}
