package org.jivesoftware.openfire.plugin;

import java.io.File;
import java.util.Map;
import java.util.TimerTask;

import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.PropertyEventDispatcher;
import org.jivesoftware.util.PropertyEventListener;
import org.jivesoftware.util.TaskEngine;
import org.jivesoftware.openfire.IQHandlerInfo;
import org.jivesoftware.openfire.IQRouter;
import org.jivesoftware.openfire.MessageRouter;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.event.SessionEventDispatcher;
import org.jivesoftware.openfire.event.SessionEventListener;
import org.jivesoftware.openfire.handler.IQHandler;
import org.jivesoftware.openfire.session.Session;
import org.xmpp.component.Component;
import org.xmpp.component.ComponentException;
import org.xmpp.component.ComponentManager;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;

import com.kemasdimas.bots.BotzConnection;
import com.kemasdimas.bots.BotzPacketReceiver;

/**
 * WgchatBridge plugin.
 * 
 * @author <a href="mailto:ryan@version2software.com">Ryan Graham</a>
 */
public class WgchatBridgePlugin implements Plugin, Component, PropertyEventListener {
   
	private BotzConnection connection;
	private PluginManager pluginManager;
	private ComponentManager componentManager;
	private String serviceName;
	
	public WgchatBridgePlugin() {
		serviceName = "wgchat";
	}
	
	@Override
	public void initializePlugin(PluginManager manager, File pluginDirectory) {
		System.out.println("Starting WgchatBridge Plugin");
		
		pluginManager = manager;
		componentManager = ComponentManagerFactory.getComponentManager();
		try {
			componentManager.addComponent(serviceName, this);
		} catch (Exception e) {
		}
		PropertyEventDispatcher.addListener(this);
	}
	
	
	@Override
	public void destroyPlugin() {
		PropertyEventDispatcher.removeListener(this);
		if (componentManager != null) {
			try {
				componentManager.removeComponent(serviceName);
			} catch (Exception e) {
			}
		}
		
		componentManager = null;
		pluginManager = null;
	}

	@Override
	public String getDescription() {
		return pluginManager.getDescription(this);
	}


	@Override
	public String getName() {
		return pluginManager.getName(this);
	}


	@Override
	public void initialize(JID arg0, ComponentManager arg1)
			throws ComponentException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void processPacket(Packet packet) {
		// TODO Auto-generated method stub
		System.out.println("WGCHAT: " + packet.toString());
	}


	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void propertySet(String property, Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void propertyDeleted(String property, Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void xmlPropertySet(String property, Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void xmlPropertyDeleted(String property, Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}
}
