package org.jivesoftware.xmpp.workgroup.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jivesoftware.database.ConnectionProvider;
import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.database.ProfiledConnection;
import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;

public final class FastpathDBManager {

	private static Map<String, ConnectionProvider> databasesMap = new ConcurrentHashMap<String, ConnectionProvider>();
	
	public static ConnectionProvider getProvider(String wgname) {
		ConnectionProvider provider = databasesMap.get(wgname);
		
		if (provider == null && wgname.trim().length() > 0) {
			// Initiate the provider!
			provider = new FastpathMultiDBConnectionProvider(wgname);
			
			// 12 November 2012
			// Changing the multiconnection logic
			// Every workgroup that doesn't have any related connection detail in wgDatabaseMap
			// Must use default faspath_db connection, stored to hashmap as "*" for the key
			if (((FastpathMultiDBConnectionProvider) provider).getServerURL() != null) {
				provider.start();
			} else {
				// If a worgkroup doesn't have an associated database map, use the default with "*" key
				provider = databasesMap.get("*");
				
				if (provider == null) {
					// Create new provider for the default connection
					provider = new FaspathDefaultConnectionProvider();
					provider.start();
					
					databasesMap.put("*", provider);
				}
			}
			
			databasesMap.put(wgname, provider);
		}
		
		return provider;
	}
	
	public static void removeProvider(String wgName) {
		databasesMap.remove(wgName);
	}
	
	public static Connection getConnection(String wgname) throws SQLException {
		
		ConnectionProvider provider = getProvider(wgname);
		if (provider == null) {
			throw new SQLException("Connection provider for " + wgname + " can not be found.");
		}
		
		return provider.getConnection();
		// TODO: May want to make these settings configurable
//        Integer retryCnt = 0;
//        Integer retryMax = 10;
//        Integer retryWait = 250; // milliseconds
//        Connection con = null;
//        SQLException lastException = null;
//        do {
//            try {
//            	con = provider.getConnection();
//                if (con != null) {
//                    // Got one, lets hand it off.
//                    // Usually profiling is not enabled. So we return a normal 
//                    // connection unless profiling is enabled. If yes, wrap the
//                    // connection with a profiled connection.
//                	return con;
//                }
//            } catch (SQLException e) {
//            	// TODO distinguish recoverable from non-recoverable exceptions.
//            	lastException = e;
//			}
//            try {
//                Thread.sleep(retryWait);
//            }
//            catch (Exception e) {
//                // Ignored
//            }
//            retryCnt++;
//        } while (retryCnt <= retryMax);
//        throw new SQLException("ConnectionManager.getConnection() " +
//                "failed to obtain a connection after " + retryCnt +" retries. " +
//                "The exception from the last attempt is as follows: "+lastException);
		
//		 return DbConnectionManager.getConnection();
	}
	
	public static Connection getConnection() throws SQLException {
		return DbConnectionManager.getConnection();
	}
	
	public static Connection getTransactionConnection() throws SQLException {
		return DbConnectionManager.getTransactionConnection();
	}
	
	public static Connection getTransactionConnection(String workgroupname) throws SQLException {
		Connection con = getConnection(workgroupname);
        if (DbConnectionManager.isTransactionsSupported()) {
            con.setAutoCommit(false);
        }
        
        return con;
	}
	
	public static void closeTransactionConnection(Connection con,
            boolean abortTransaction)
    {
		DbConnectionManager.closeTransactionConnection(con, abortTransaction);
    }
	
	public static void closeTransactionConnection(PreparedStatement pstmt, Connection con,
            boolean abortTransaction)
    {
		DbConnectionManager.closeTransactionConnection(pstmt, con, abortTransaction);
    }
	
	public static void closeConnection(Connection con) {
		DbConnectionManager.closeConnection(con);
    }
	
	public static void closeConnection(Statement stmt, Connection con) {
		DbConnectionManager.closeConnection(stmt, con);
    }
	
	public static void closeConnection(ResultSet rs, Statement stmt, Connection con) {
		DbConnectionManager.closeConnection(rs, stmt, con);
    }
	
	public static String getLargeTextField(ResultSet rs, int columnIndex) throws SQLException {
		return DbConnectionManager.getLargeTextField(rs, columnIndex);
	}
	
	public static void setLargeTextField(PreparedStatement pstmt, int parameterIndex,
            String value) throws SQLException {
		DbConnectionManager.setLargeTextField(pstmt, parameterIndex, value);
	}
	
}
