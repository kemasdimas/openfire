/**
 * $RCSfile$
 * $Revision: 11608 $
 * $Date: 2010-02-07 15:03:12 -0600 (Sun, 07 Feb 2010) $
 *
 * Copyright (C) 2005-2008 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jivesoftware.xmpp.workgroup.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.jivesoftware.database.ConnectionProvider;
import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.util.JiveGlobals;
import org.logicalcobwebs.proxool.ConnectionPoolDefinitionIF;
import org.logicalcobwebs.proxool.ProxoolException;
import org.logicalcobwebs.proxool.ProxoolFacade;
import org.logicalcobwebs.proxool.admin.SnapshotIF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import snaq.db.ConnectionPool;

/**
 * Default Jive connection provider, which uses an internal connection pool.<p>
 *
 * @author Jive Software
 */
public class FaspathDefaultConnectionProvider implements ConnectionProvider {

	private static final Logger Log = LoggerFactory.getLogger(FaspathDefaultConnectionProvider.class);

	private ConnectionPool connectionPool;
    private String driver;
    private String serverURL;
    private String username;
    private String password;
    private String host;
    private int minConnections = 10;
    private int maxConnections = 30;
    private int tryConnectTimeout = 3000; 
    private String testSQL = "";
    private Boolean testBeforeUse = true;
    private Boolean testAfterUse = true;

    /**
     * Maximum time a connection can be open before it's reopened (in days)
     */
    private long connectionTimeout = 12 * 60 * 60;

    /**
     * MySQL doesn't currently support Unicode. However, a workaround is
     * implemented in the mm.mysql JDBC driver. Setting the Jive property
     * database.mysql.useUnicode to true will turn this feature on.
     */
    private boolean mysqlUseUnicode;

    /**
     * Creates a new DefaultConnectionProvider.
     */
    public FaspathDefaultConnectionProvider() {
        loadProperties();
       }

    public boolean isPooled() {
        return true;
    }

    public Connection getConnection() throws SQLException {
        return connectionPool.getConnection(tryConnectTimeout);
    }

    public void start() {
    	// Initiate the pool
        connectionPool = new ConnectionPool(
        		getUsername(), 
        		getMinConnections(), 
        		getMaxConnections(), 
        		getMaxConnections(), 
        		getConnectionTimeout(), 
        		getServerURL(), 
        		getUsername(), 
        		getPassword());
    }

    public void restart() {
    	destroy();
    	start();
    }

    public void destroy() {
    	connectionPool.releaseForcibly();
    }

    /**
     * Returns the JDBC driver classname used to make database connections.
     * For example: com.mysql.jdbc.Driver
     *
     * @return the JDBC driver classname.
     */
    public String getDriver() {
        return driver;
    }
    
    /**
     * Returns the JDBC connection URL used to make database connections.
     *
     * @return the JDBC connection URL.
     */
    public String getServerURL() {
        return serverURL;
    }

    /**
     * Returns the username used to connect to the database. In some cases,
     * a username is not needed so this method will return null.
     *
     * @return the username used to connect to the datbase.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the password used to connect to the database. In some cases,
     * a password is not needed so this method will return null.
     *
     * @return the password used to connect to the database.
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * Returns the minimum number of connections that the pool will use. This
     * should probably be at least three.
     *
     * @return the minimum number of connections in the pool.
     */
    public int getMinConnections() {
        return minConnections;
    }
    
    /**
     * Returns the maximum number of connections that the pool will use. The
     * actual number of connections in the pool will vary between this value
     * and the minimum based on the current load.
     *
     * @return the max possible number of connections in the pool.
     */
    public int getMaxConnections() {
        return maxConnections;
    }

    /**
     * Returns the amount of time between connection recycles in days. For
     * example, a value of .5 would correspond to recycling the connections
     * in the pool once every half day.
     *
     * @return the amount of time in days between connection recycles.
     */
    public long getConnectionTimeout() {
        return connectionTimeout;
    }

    /**
     * Returns the SQL statement used to test if a connection is valid.
     *
     * @return the SQL statement that will be run to test a connection.
     */
    public String getTestSQL() {
        return testSQL;
    }

    /**
     * Sets the SQL statement used to test if a connection is valid.  House keeping
     * and before/after connection tests make use of this.  This
     * should be something that causes the minimal amount of work by the database
     * server and is as quick as possible.
     *
     * @param testSQL the SQL statement that will be run to test a connection.
     */
    public void setTestSQL(String testSQL) {
        this.testSQL = testSQL;
    }

    /**
     * Returns whether returned connections will be tested before being handed over
     * to be used.
     *
     * @return True if connections are tested before use.
     */
    public Boolean getTestBeforeUse() {
        return testBeforeUse;
    }

    /**
     * Sets whether connections will be tested before being handed over to be used.
     *
     * @param testBeforeUse True or false if connections are to be tested before use.
     */
    public void setTestBeforeUse(Boolean testBeforeUse) {
        this.testBeforeUse = testBeforeUse;
    }

    /**
     * Returns whether returned connections will be tested after being returned to
     * the pool.
     *
     * @return True if connections are tested after use.
     */
    public Boolean getTestAfterUse() {
        return testAfterUse;
    }

    /**
     * Sets whether connections will be tested after being returned to the pool.
     *
     * @param testAfterUse True or false if connections are to be tested after use.
     */
    public void setTestAfterUse(Boolean testAfterUse) {
        this.testAfterUse = testAfterUse;
    }

    public boolean isMysqlUseUnicode() {
        return mysqlUseUnicode;
    }

    /**
     * Load properties that already exist from Jive properties.
     */
    private void loadProperties() {

        driver = JiveGlobals.getXMLProperty("database.faspathProvider.driver");
        serverURL = JiveGlobals.getXMLProperty("database.faspathProvider.serverURL");
        username = JiveGlobals.getXMLProperty("database.faspathProvider.username");
        password = JiveGlobals.getXMLProperty("database.faspathProvider.password");
        
        // Disabling minConection & maxConnection properties
//        String minCons = JiveGlobals.getXMLProperty("database.faspathProvider.minConnections");
//        String maxCons = JiveGlobals.getXMLProperty("database.faspathProvider.maxConnections");
        testSQL = JiveGlobals.getXMLProperty("database.faspathProvider.testSQL", DbConnectionManager.getTestSQL(driver));
        testBeforeUse = JiveGlobals.getXMLProperty("database.faspathProvider.testBeforeUse", true);
        testAfterUse = JiveGlobals.getXMLProperty("database.faspathProvider.testAfterUse", true);

        // See if we should use Unicode under MySQL
        mysqlUseUnicode = Boolean.valueOf(JiveGlobals.getXMLProperty("database.mysql.useUnicode"));
//        try {
//            if (minCons != null) {
//                minConnections = Integer.parseInt(minCons);
//            }
//            if (maxCons != null) {
//                maxConnections = Integer.parseInt(maxCons);
//            }
//        }
//        catch (Exception e) {
//            Log.error("Error: could not parse default pool properties. " +
//                    "Make sure the values exist and are correct.", e);
//        }
    }
    
    @Override
	public String toString() {
        try {
            ConnectionPoolDefinitionIF poolDef = ProxoolFacade.getConnectionPoolDefinition("openfire");
            SnapshotIF poolStats = ProxoolFacade.getSnapshot("openfire", true);
            return poolDef.getMinimumConnectionCount()+","+poolDef.getMaximumConnectionCount()+","
                    +poolStats.getAvailableConnectionCount()+","+poolStats.getActiveConnectionCount();
        }
        catch (ProxoolException e) {
            return "Default Connection Provider";
        }
    }
}
