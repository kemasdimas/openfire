package org.jivesoftware.xmpp.workgroup.utils;

public final class FastpathMultiDBUtil {

	/**
	 * Get a workgroup name from username
	 * 
	 * @param username
	 * @return
	 */
	public static String getWorkgroupName(String username) {
		String[] fragment = username.split("@")[0].split("-");
		
		return fragment[fragment.length - 1];
	}
	
}
