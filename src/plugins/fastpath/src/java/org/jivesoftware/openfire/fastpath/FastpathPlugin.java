/**
 * $Revision: $
 * $Date: $
 *
 * Copyright (C) 2004-2008 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jivesoftware.openfire.fastpath;

import java.io.File;
import java.io.FileFilter;
import java.util.Collection;

import org.jivesoftware.openfire.cluster.ClusterEventListener;
import org.jivesoftware.openfire.cluster.ClusterManager;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.fastpath.util.TaskEngine;
import org.jivesoftware.openfire.user.UserNameManager;
import org.jivesoftware.openfire.user.UserNameProvider;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.StringUtils;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.component.ComponentException;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.JID;

/**
 * Openfire Fastpath plugin.
 *
 * @author Matt Tucker
 */
public class FastpathPlugin implements Plugin, ClusterEventListener {

	private static final Logger Log = LoggerFactory.getLogger(FastpathPlugin.class);
	
	/**
	 * These values depends on userservice plugin value! beware!
	 */
	private String secret;
    private boolean enabled;
    private Collection<String> allowedIPs;
	
    /**
     * Keep a reference to Fastpath only when the service is up and running in this JVM.
     */
    private WorkgroupManager workgroupManager;

    public void initializePlugin(PluginManager manager, File pluginDirectory) {
        System.out.println("Starting Fastpath Server");

        // Check if we Enterprise is installed and stop loading this plugin if found
        File pluginDir = new File(JiveGlobals.getHomeDirectory(), "plugins");
        File[] jars = pluginDir.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                String fileName = pathname.getName().toLowerCase();
                return (fileName.equalsIgnoreCase("enterprise.jar"));
            }
        });
        if (jars.length > 0) {
            // Do not load this plugin since Enterprise is still installed
            System.out.println("Enterprise plugin found. Stopping Fastpath Plugin");
            throw new IllegalStateException("This plugin cannot run next to the Enterprise plugin");
        }

        // Make sure that the fastpath folder exists under the home directory
        File fastpathDir = new File(JiveGlobals.getHomeDirectory() +
            File.separator + "fastpath");
        if (!fastpathDir.exists()) {
            fastpathDir.mkdirs();
        }

        secret = JiveGlobals.getProperty("plugin.userservice.secret", "");
        // If no secret key has been assigned to the user service yet, assign a random one.
        if (secret.equals("")){
            secret = StringUtils.randomString(8);
            setSecret(secret);
        }
        
        // See if the service is enabled or not.
        enabled = JiveGlobals.getBooleanProperty("plugin.userservice.enabled", false);

        // Get the list of IP addresses that can use this service. An empty list means that this filter is disabled.
        allowedIPs = StringUtils.stringToCollection(JiveGlobals.getProperty("plugin.userservice.allowedIPs", ""));
        
        workgroupManagerStart();

        // Listen to cluster events
        ClusterManager.addListener(this);
    }

    public void destroyPlugin() {
        workgroupManagerStop();

        // Stop listen to cluster events
        ClusterManager.removeListener(this);

        // Dispose pending tasks
        TaskEngine.getInstance().dispose();
    }

    private void workgroupManagerStart() {
        workgroupManager = WorkgroupManager.getInstance();
        // Register Fastpath service
        try {
            ComponentManagerFactory.getComponentManager().addComponent("workgroup", workgroupManager);
        }
        catch (ComponentException e) {
            // Do nothing. Should never happen.
            Log.error(e.getMessage(), e);
        }
        // Register the provider of workgroup names
        UserNameManager.addUserNameProvider(workgroupManager.getAddress().toString(),
            new UserNameProvider() {
                public String getUserName(JID entity) {
                    try {
                        Workgroup workgroup = workgroupManager.getWorkgroup(entity);
                        return workgroup.getDisplayName();
                    }
                    catch (UserNotFoundException e) {
                        return entity.toString();
                    }
                }

            });
        // Start the Fastpath module
        workgroupManager.start();
    }

    private void workgroupManagerStop() {
        // Unregister workgroup component
        try {
            ComponentManagerFactory.getComponentManager().removeComponent("workgroup");
        }
        catch (ComponentException e) {
            Log.error("Error unregistering workgroup component", e);
        }
        if (workgroupManager != null) {
            // Unregister the provider of workgroup names
            UserNameManager.removeUserNameProvider(workgroupManager.getAddress().toString());
            // Stop the Fastpath module
            workgroupManager.stop();
        }
        // Clean up the reference to the workgroup manager as a way to say that FP is no longer running in this JVM
        workgroupManager = null;
    }

    public void joinedCluster() {
        workgroupManagerStop();
    }

    public void joinedCluster(byte[] nodeID) {
        // Do nothing
    }

    public void leftCluster() {
        workgroupManagerStart();
    }

    public void leftCluster(byte[] nodeID) {
        // Do nothing
    }

    public void markedAsSeniorClusterMember() {
        workgroupManagerStart();
    }
    
    /**
     * Returns the secret key that only valid requests should know.
     *
     * @return the secret key.
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Sets the secret key that grants permission to use the userservice.
     *
     * @param secret the secret key.
     */
    public void setSecret(String secret) {
        JiveGlobals.setProperty("plugin.userservice.secret", secret);
        this.secret = secret;
    }

    public Collection<String> getAllowedIPs() {
        return allowedIPs;
    }

    public void setAllowedIPs(Collection<String> allowedIPs) {
        JiveGlobals.setProperty("plugin.userservice.allowedIPs", StringUtils.collectionToString(allowedIPs));
        this.allowedIPs = allowedIPs;
    }

    /**
     * Returns true if the user service is enabled. If not enabled, it will not accept
     * requests to create new accounts.
     *
     * @return true if the user service is enabled.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Enables or disables the user service. If not enabled, it will not accept
     * requests to create new accounts.
     *
     * @param enabled true if the user service should be enabled.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        JiveGlobals.setProperty("plugin.userservice.enabled",  enabled ? "true" : "false");
    }
}
