/**
 * $RCSfile$
 * $Revision: 25826 $
 * $Date: 2006-01-17 10:00:38 -0800 (Tue, 17 Jan 2006) $
 *
 * Copyright (C) 1999-2008 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jivesoftware.openfire.fastpath.macros;

import java.util.HashMap;
import java.util.Map;

import org.jivesoftware.xmpp.workgroup.Agent;
import org.jivesoftware.xmpp.workgroup.DbProperties;
import org.jivesoftware.xmpp.workgroup.UnauthorizedException;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupManager;
import org.jivesoftware.xmpp.workgroup.utils.ModelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

public class AgentMacros {

	private static final Logger Log = LoggerFactory.getLogger(AgentMacros.class);

    private static AgentMacros singleton;

    private static final Object LOCK = new Object();
    private XStream xstream = new XStream();

    public static AgentMacros getInstance() {
        // Synchronize on LOCK to ensure that we don't end up creating
        // two singletons.
        synchronized (LOCK) {
            if (singleton == null) {
                AgentMacros agentMacros = new AgentMacros();
                singleton = agentMacros;
                return agentMacros;
            }
        }
        return singleton;
    }

    private AgentMacros() {
        // Load macros
        WorkgroupManager workgroupManager = WorkgroupManager.getInstance();
        xstream.alias("macro", Macro.class);
        xstream.alias("macrogroup", MacroGroup.class);
    }

    public MacroGroup getMacroGroup(String wgName, Agent agent) {
    	DbProperties props = agent.getProperties(wgName);
        String macros = props.getProperty("personal.macro");
        
        if (ModelUtil.hasLength(macros)) {
            MacroGroup group = (MacroGroup) xstream.fromXML(macros);
           	
            return group;
        }
        
        return null;
    }

    public void saveMacros(String wgName, Agent agent, MacroGroup group) {
        String saveString = xstream.toXML(group);

        DbProperties props = agent.getProperties(wgName);
        try {
        	props.deleteProperty("personal.macro");
            props.setProperty("personal.macro", saveString);
        }
        catch (UnauthorizedException e) {
           Log.error(e.getMessage(), e);
        }
    }

}
