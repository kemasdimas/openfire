package com.kemasdimas.workgroupservice.component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.jivesoftware.openfire.fastpath.dataforms.FormElement;
import org.jivesoftware.openfire.fastpath.dataforms.WorkgroupForm;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSetting;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSettingsManager;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSettings.SettingType;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.NotFoundException;
import org.jivesoftware.xmpp.workgroup.Agent;
import org.jivesoftware.xmpp.workgroup.AgentManager;
import org.jivesoftware.xmpp.workgroup.AgentNotFoundException;
import org.jivesoftware.xmpp.workgroup.RequestQueue;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.xmpp.packet.JID;

import com.kemasdimas.workgroupservice.dto.WorkgroupAutomessageDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupAutomessageTypeDTO;
import com.kemasdimas.workgroupservice.manager.WorkgroupChannelManager;

public final class QueueServiceModule extends BaseWorkgroupServiceModule {

	protected static QueueServiceModule instance;
	private UserManager userManager;
	
	protected QueueServiceModule() {
		userManager = UserManager.getInstance();
	}
	
	public static QueueServiceModule getInstance() {
		if (instance == null) {
			instance = new QueueServiceModule();
		}
		
		return (QueueServiceModule) instance;
	}
	
	@Override
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		// This is used to validate workgroup name!
		Workgroup wg = findWorkgroup(wgName);
		
		if ("list-channel".equals(type)) {
			return listChannel(wg);
		} else if ("list-channel-automessage-type".equals(type)) {
			return listChannelAutomessageType();
		}
		
		String channelName = request.getParameter("channelName");
		String description = request.getParameter("description");
		
		if (channelName == null) {
			throw new Exception("Channel name can not be null");
		} 
		
		if ("create-channel".equals(type)) {
			return createChannel(wg, channelName, description);
		} else if ("delete-channel".equals(type)) {
			return deleteChannel(wg, channelName);
		} else if ("edit-channel".equals(type)) {
			String newName = request.getParameter("newName");
			
			return editChannel(wg, channelName, newName, description);
		} else if ("list-channel-agent".equals(type)) {
			return listChannelAgent(wg, channelName);
		} else if ("list-channel-automessage".equals(type)) {
			return listChannelAutomessage(wg, channelName);
		} else if ("create-channel-automessage".equals(type) || 
				"delete-channel-automessage".equals(type)) {
			
			String messageType = request.getParameter("messageType");
			if (messageType == null) {
				throw new Exception("messageType name can not be null");
			}
			
			if ("delete-channel-automessage".equals(type)) {
				return deleteChannelAutomessage(wg, messageType, channelName);
			} else if ("create-channel-automessage".equals(type)) {
				String messageLabel = request.getParameter("messageLabel");
				String message = request.getParameter("message");
				if (messageLabel == null || message == null) {
					throw new Exception("messageLabel / message name can not be null");
				}
				
				return createChannelAutomessage(wg, messageType, messageLabel, message, channelName);
			}
		}
		
		String agentNames = request.getParameter("agentName");
		if (agentNames == null) {
			throw new Exception("Agent name(s) can not be null");
		}
		
		if ("add-channel-agent".equals(type)) {
			return modifyChannelAgent(wg, channelName, agentNames, "add");
		} else if ("delete-channel-agent".equals(type)) {
			return modifyChannelAgent(wg, channelName, agentNames, "delete");
		}
		
		throw new Exception("Type not supported");
	}
	
	private String listChannelAutomessageType() {
		List<WorkgroupAutomessageTypeDTO> types = WorkgroupChannelManager.getInstance().fetchAutomessageType();
		
		JSONArray jsonArray = new JSONArray();
		for (WorkgroupAutomessageTypeDTO type : types) {
			JSONObject json = new JSONObject();
			
			json.put("label", type.getLabel());
			json.put("name", type.getName());
			
			jsonArray.add(json);
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", jsonArray.size());
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	/**
	 * List all custom division message in a queue
	 * 
	 * @param wg
	 * @param channelName
	 * @return
	 * @throws Exception
	 */
	private String listChannelAutomessage(Workgroup wg, String channelName) throws Exception {
		try {		
			// Check the channel name validity
			RequestQueue queue = wg.getRequestQueue(channelName);
			
			List<WorkgroupAutomessageDTO> messages = WorkgroupChannelManager.getInstance().
					fetchAutomessageInWorkgroup(wg.getWorkgroupName(), queue.getID());
			
			JSONArray jsonArray = new JSONArray();
			for (WorkgroupAutomessageDTO message : messages) {
				JSONObject json = new JSONObject();
				
				json.put("workgroupNode", message.getWorkgroupNode());
				json.put("name", message.getName());
				json.put("queueID", message.getQueueId());
				json.put("label", message.getLabel());
				json.put("value", message.getValue());
				
				jsonArray.add(json);
			}
			
			JSONObject finalJson = new JSONObject();
			finalJson.put("total", jsonArray.size());
			finalJson.put("data", jsonArray);
			
			return finalJson.toString();
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Add new custom division message to a queue
	 * 
	 * @param wg
	 * @param type
	 * @param label
	 * @param message
	 * @param channelName
	 * @return
	 * @throws Exception
	 */
	private String createChannelAutomessage(Workgroup wg, String type, String label, String message, String channelName) throws Exception {
		try {
			RequestQueue queue = wg.getRequestQueue(channelName);
			
			ChatSetting newSetting = new ChatSetting(type);
			newSetting.setWorkgroupNode(wg.getWorkgroupName());
			newSetting.setLabel(label);
			newSetting.setType(SettingType.bot_settings);
			newSetting.setValue(message);
			newSetting.setQueueId(queue.getID());
			
			ChatSettingsManager.getInstance().addChatSetting(newSetting);
			
			return "\"ok\""; 
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Delete custom division message from a queue
	 * 
	 * @param wg
	 * @param type
	 * @param channelName
	 * @return
	 * @throws Exception
	 */
	private String deleteChannelAutomessage(Workgroup wg, String type, String channelName) throws Exception {
		try {
			RequestQueue queue = wg.getRequestQueue(channelName);
			
			ChatSetting newSetting = new ChatSetting(type);
			newSetting.setWorkgroupNode(wg.getWorkgroupName());
			newSetting.setQueueId(queue.getID());
			
			ChatSettingsManager.getInstance().removeChatSetting(newSetting);
			
			return "\"ok\""; 
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Helper function to delete an agent in a queue / channel
	 * 
	 * @param agentManager
	 * @param agentJID
	 * @param queue
	 * @throws AgentNotFoundException
	 */
	private void deleteChannelAgent(AgentManager agentManager, JID agentJID, RequestQueue queue) throws AgentNotFoundException {
		Agent agent = agentManager.getAgent(agentJID);
		queue.removeMember(agent);
	}
	
	/**
	 * Helper function to add an agent in a queue / channel
	 * 
	 * @param agentManager
	 * @param agentJID
	 * @param queue
	 * @throws AgentNotFoundException
	 */
	private void addChannelAgent(AgentManager agentManager, JID agentJID, RequestQueue queue) throws AgentNotFoundException {
		Agent agent = null;
		if (agentManager.hasAgent(agentJID)) {
			agent = agentManager.getAgent(agentJID);
		} else {
			agent = agentManager.createAgent(agentJID);
		}
		queue.addMember(agent);
	}
	
	/**
	 * Add / delete agent in a queue / channel
	 * 
	 * @param wg
	 * @param channelName
	 * @param agentNames
	 * @param type (add / delete)
	 * @return
	 * @throws Exception
	 */
	public String modifyChannelAgent(Workgroup wg, String channelName, String agentNames, String type) throws Exception {
		RequestQueue queue = wg.getRequestQueue(channelName);
		String wgName = wg.getJID().getNode();
		StringTokenizer tokenizer = new StringTokenizer(agentNames, ", \t\n\r\f");
		ArrayList<String> errors = new ArrayList<String>();
		boolean containsError = false;
		AgentManager agentManager = wg.getAgentManager();
		JID agentJID = null;
		
		while (tokenizer.hasMoreElements()) {
			
			
			String userNameToken = "";
			try {
				
				// wgName always appended to the agent name, to provide more security
				userNameToken = tokenizer.nextToken().split("-")[0];
				agentJID = prepJID(wgName, userNameToken);
				userManager.getUser(agentJID.getNode());
				
				if ("add".equals(type)) {
					addChannelAgent(agentManager, agentJID, queue);
				} else {
					deleteChannelAgent(agentManager, agentJID, queue);
				}
			} catch (UserNotFoundException e) {
				errors.add("User " + userNameToken + " not exists");
				containsError = true;
			} catch (AgentNotFoundException e) {
				errors.add("Agent " + agentJID.toBareJID() + " not exists");
				containsError = true;
			} catch (Exception e) {
				errors.add("Exception " + e.getMessage());
				containsError = true;
			}
		}
		
		if (containsError) {
			StringWriter out = new StringWriter();
			String jsonText = "";
			try {
				JSONValue.writeJSONString(errors, out);
				jsonText = out.toString();
			} catch (IOException e) {
				throw new Exception("IO Error: writing JSON object");
			}
			
			throw new Exception(jsonText);
		} else {
			return "\"ok\""; 
		}
	}
	
	/**
	 * List all agents in a queue / channel
	 * 
	 * @param wg
	 * @param channelName
	 * @return
	 * @throws NotFoundException
	 */
	private String listChannelAgent(Workgroup wg, String channelName) throws NotFoundException {
		RequestQueue queue = wg.getRequestQueue(channelName);
		int total = queue.getMemberCount();
		Iterator<Agent> iterator = queue.getMembers().iterator();
		StringBuffer buffer = new StringBuffer("{\"total\":" + total + ", \"data\":[");
		
		while (iterator.hasNext()) {
			Agent a = iterator.next();
			
			long id = a.getID();
			String name = a.getAgentJID().getNode();
			String nickname = (a.getNickname() == null ? "" : a.getNickname());
			
			buffer.append("{ \"id\": " + id + ", ");
			buffer.append("\"name\": \"" + name + "\", ");
			buffer.append("\"nickname\": \"" + nickname + "\"},");
		}
		
		if (total > 0)
			buffer.setCharAt(buffer.length() - 1, ']');
		else
			buffer.append("]");
		
		buffer.append('}');
		return buffer.toString();
	}
	
	/**
	 * Check whether a channel name is available for use
	 * 
	 * @param wg
	 * @param channelName
	 * @return
	 */
	private boolean isChannelAvailable(Workgroup wg, String channelName) {
		try {
			wg.getRequestQueue(channelName);
		} catch (NotFoundException e) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Create channel / queue in a workgroup
	 * 
	 * @param wg
	 * @param channelName
	 * @param description
	 * @return
	 * @throws Exception 
	 */
	private String createChannel(Workgroup wg, String channelName, String description) throws Exception {
		if ("Default Queue".equals(channelName)) {
			throw new Exception("Channel name can not be \"Default Queue\"");
		}
		
		if (isChannelAvailable(wg, channelName)) {
			RequestQueue queue = wg.createRequestQueue(channelName);
			queue.setDescription(description);
			
			return "{\"id\": " + queue.getID() + "}";
		} else {
			throw new Exception("Channel " + channelName + " not available");
		}
	}
	
	/**
	 * Delete a channel in a workgroup
	 * 
	 * @param wg
	 * @param channelName
	 * @return
	 * @throws Exception 
	 */
	private String deleteChannel(Workgroup wg, String channelName) throws Exception {
		if ("Default Queue".equals(channelName)) {
			throw new Exception("Channel name can not be \"Default Queue\"");
		}
		
		RequestQueue queue = wg.getRequestQueue(channelName);
		wg.deleteRequestQueue(queue);
		
		return "\"ok\"";
	}
	
	/**
	 * Edit a channel in a workgroup
	 * 
	 * @param wg
	 * @param channelName
	 * @param newName, can be null
	 * @param description, can be null
	 * @return
	 * @throws Exception 
	 */
	private String editChannel(Workgroup wg, String channelName, String newName, String description) throws Exception {
		if ("Default Queue".equals(channelName)) {
			throw new Exception("Channel name can not be \"Default Queue\"");
		}
		
		RequestQueue queue = wg.getRequestQueue(channelName);
		
		if (description != null && !description.equals(queue.getDescription())) {
			queue.setDescription(description);
		}
		if (newName != null && !channelName.equals(newName) && isChannelAvailable(wg, newName)) {
			queue.setName(newName);
		} else {
			throw new Exception("Channel " + channelName + " not available");
		}
		
		return "{\"id\": " + queue.getID() + "}";
	}
	
	/**
	 * List all channel / queues in a workgroup
	 * 
	 * @param wg
	 * @return
	 */
	private String listChannel(Workgroup wg) {
		int total = wg.getRequestQueueCount();
		Iterator<RequestQueue> iterator = wg.getRequestQueues().iterator();
		StringBuffer buffer = new StringBuffer("{\"total\":" + total + ", \"data\":[");
		
		while (iterator.hasNext()) {
			RequestQueue q = iterator.next();
			
			long id = q.getID();
			String name = q.getName();
			String description = (q.getDescription() == null ? "" : q.getDescription());
			int totalAgent = q.getMemberCount();
			int onlineAgent = q.getAgentSessionList().getAvailableAgentCount();
			int requestCount = q.getRequestCount();
			int averageTime = q.getAverageTime();
			
			buffer.append("{ \"id\": " + id + ", ");
			buffer.append("\"name\": \"" + name + "\", ");
			buffer.append("\"description\": \"" + description + "\", ");
			buffer.append("\"online-agent\": " + onlineAgent + ", ");
			buffer.append("\"total-agent\": " + totalAgent + ", ");
			buffer.append("\"request-count\": " + requestCount + ", ");
			buffer.append("\"average-time\": " + averageTime + "},");
		}
		
		buffer.setCharAt(buffer.length() - 1, ']');
		buffer.append('}');
		return buffer.toString();
	}
	
	@Override
	public void close() {
		userManager = null;
		
		super.close();
	}
}
