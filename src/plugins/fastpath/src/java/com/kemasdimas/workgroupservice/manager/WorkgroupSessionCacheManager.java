package com.kemasdimas.workgroupservice.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;

import com.kemasdimas.workgroupservice.dto.WorkgroupAnswerCacheDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupPreviousSessionDTO;

public final class WorkgroupSessionCacheManager {
	private final static String GET_PREVIOUS_ANSWER = "SELECT m.* FROM fpSessionMetadata m " +
			"WHERE m.sessionID = (SELECT sessionID FROM fpSession f WHERE workgroupID = ? " +
			"AND userID = ? ORDER BY startTime DESC LIMIT 1)";
	private final static String INSERT_RESET_VALUE = "INSERT INTO fpSessionMetadata VALUES (?, 'reset', 'true')";
	private final static String GET_PREVIOUS_SESSION = "SELECT count(userID) session_counter, LEFT(RIGHT(max(startTime), 13), 10) previous_timestamp, " + 
			"LEFT(RIGHT(min(startTime), 13), 10) first_timestamp FROM fpSession WHERE userID = ? ORDER BY startTime DESC LIMIT 1";
	
	private static WorkgroupSessionCacheManager instance;
	
	public static WorkgroupSessionCacheManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupSessionCacheManager();
		}
		
		return instance;
	}
	
	public void addResetValue(String sessionId, String workgroupName) throws SQLException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection(workgroupName);
            pstmt = con.prepareStatement(INSERT_RESET_VALUE);
            pstmt.setString(1, sessionId);
            
            pstmt.executeUpdate();
        }
        catch (SQLException sqle) {
            throw sqle;
        }
        finally {
            FastpathDBManager.closeConnection(pstmt, con);
        }
    }
	
	public WorkgroupAnswerCacheDTO fetchAnswerCache(long wgId, String userName, String workgroupName) {
		HashMap<String, String> metaData = new HashMap<String, String>();
		String sessionId = null;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroupName);
			statement = con.prepareStatement(GET_PREVIOUS_ANSWER);
			statement.setLong(1, wgId);
			statement.setString(2, userName);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				sessionId = result.getString(1);
				metaData.put(result.getString(2), result.getString(3));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return new WorkgroupAnswerCacheDTO(sessionId, metaData);
	}
	
	public WorkgroupPreviousSessionDTO fethPreviousSession(String userName, String workgroupName) {
		WorkgroupPreviousSessionDTO previousSession = null;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroupName);
			statement = con.prepareStatement(GET_PREVIOUS_SESSION);
			statement.setString(1, userName);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				if (result.getInt(1) > 0) {
					previousSession = new WorkgroupPreviousSessionDTO(
							result.getInt(1),
							result.getLong(2),
							result.getLong(3));
				}
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return previousSession;
	}
	
}
