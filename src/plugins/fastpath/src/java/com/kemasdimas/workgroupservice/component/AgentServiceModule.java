package com.kemasdimas.workgroupservice.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.fastpath.macros.AgentMacros;
import org.jivesoftware.openfire.fastpath.macros.Macro;
import org.jivesoftware.openfire.fastpath.macros.MacroGroup;
import org.jivesoftware.openfire.group.Group;
import org.jivesoftware.openfire.group.GroupManager;
import org.jivesoftware.openfire.group.GroupNotFoundException;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.xmpp.workgroup.Agent;
import org.jivesoftware.xmpp.workgroup.AgentManager;
import org.jivesoftware.xmpp.workgroup.AgentNotFoundException;
import org.jivesoftware.xmpp.workgroup.RequestQueue;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.JID;

import com.kemasdimas.workgroupservice.dto.WorkgroupUserDTO;
import com.kemasdimas.workgroupservice.manager.WorkgroupAgentManager;

public final class AgentServiceModule extends BaseWorkgroupServiceModule {
		
	protected static AgentServiceModule instance;
	private UserManager userManager;
	private GroupManager groupManager;
	private AgentMacros agentMacros;
	
	protected AgentServiceModule() {
		super();

		userManager = XMPPServer.getInstance().getUserManager();
		groupManager = GroupManager.getInstance();
		agentMacros = AgentMacros.getInstance();
	}
	
	public static AgentServiceModule getInstance() {
		if (instance == null) {
			instance = new AgentServiceModule();
		}
		
		return (AgentServiceModule) instance;
	}
	
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		String agentName = request.getParameter("agentName");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		// This is used to validate workgroup name!
		Workgroup wg = findWorkgroup(wgName);
		
		if ("list-agent".equals(type)) {
			String limit = request.getParameter("limit");
			String offset = request.getParameter("offset");
			
			if (limit == null || offset == null) {
				throw new Exception("Limit / offset can not be null");
			}
			
			int limitInt = Integer.parseInt(limit);
			int offsetInt = Integer.parseInt(offset);
			
			return listAgent(wg, limitInt, offsetInt);
		}
		
		if (agentName == null) {
			throw new Exception("Agent name can not be null");
		}
		agentName = agentName.split("-")[0];
		
		if ("pass-agent".equals(type)) {
			String pass1 = request.getParameter("password");
			String pass2 = request.getParameter("confirmation");
			
			if (pass1 == null || pass2 == null) {
				throw new Exception("Password / confirmation can not be null");
			}
			
			return changePassword(wgName, agentName, pass1, pass2);
		} else if ("create-agent".equals(type) || "edit-agent".equals(type)) {
			String realName = request.getParameter("realName");
			String email = request.getParameter("email");
			
			if (email == null || realName == null) {
				throw new Exception("Email / real name can not be null");
			}
			
			if ("create-agent".equals(type)) {
				return createAgent(wg, agentName, realName, email);
			} else {
				return editAgent(wgName, agentName, realName, email);
			}
		} else if ("delete-agent".equals(type)) {
			return deleteAgent(wg, wgName, agentName);
		} else if ("search-agent".equals(type)) {
			return searchAgent(wgName, agentName);
		} else if ("list-agent-macro".equals(type)) {
			return listAgentMacro(wg, agentName);
		} else if ("add-agent-macro".equals(type)) {
			String macroTitle = request.getParameter("macroTitle");
			String macroResponse = request.getParameter("macroResponse");
			
			if (macroTitle == null || macroResponse == null) {
				throw new Exception("macroTitle / macroResponse can not be null");
			}
			
			return addAgentMacro(wg, agentName, macroTitle, macroResponse);
		} else if ("edit-agent-macro".equals(type)) {
			int macroIndex = Integer.parseInt(request.getParameter("macroIndex"));
			String macroTitle = request.getParameter("macroTitle");
			String macroResponse = request.getParameter("macroResponse");
			
			if (macroTitle == null || macroResponse == null) {
				throw new Exception("macroTitle / macroResponse can not be null");
			}
			
			return editAgentMacro(wg, agentName, macroTitle, macroResponse, macroIndex);
		} else if ("delete-agent-macro".equals(type)) {
			int macroIndex = Integer.parseInt(request.getParameter("macroIndex"));
			
			return deleteAgentMacro(wg, agentName, macroIndex);
		}
		
		throw new Exception("Type not supported");
	}
	
	private String addAgentMacro(Workgroup wg, String agentName, String title, String response) throws Exception {
		JID agentJID = prepJID(wg.getWorkgroupName(), agentName);
		Agent agent = wg.getAgentManager().getAgent(agentJID);
		
		MacroGroup group = agentMacros.getMacroGroup(wg.getWorkgroupName(), agent);
		if (group == null) {
			group = new MacroGroup();
			group.setTitle("Personal");
		}
		
		Macro macro = new Macro();
		macro.setType(Macro.TEXT);
		macro.setTitle(title);
		macro.setResponse(response);
		group.addMacro(macro);
		agentMacros.saveMacros(wg.getWorkgroupName(), agent, group);
		
		return "\"ok\"";
	}
	
	private String editAgentMacro(Workgroup wg, String agentName, String title, String response, int index) throws Exception {
		JID agentJID = prepJID(wg.getWorkgroupName(), agentName);
		Agent agent = wg.getAgentManager().getAgent(agentJID);
		
		MacroGroup parentGroup = agentMacros.getMacroGroup(wg.getWorkgroupName(), agent);
		if (parentGroup == null) {
			throw new Exception("Parent group not found");
		}
		
		Macro macro = parentGroup.getMacro(index);
		if (macro == null) {
			throw new Exception("Macro not found");
		}
		macro.setTitle(title);
		macro.setResponse(response);
		agentMacros.saveMacros(wg.getWorkgroupName(), agent, parentGroup);
		
		return "\"ok\"";
	}
	
	private String deleteAgentMacro(Workgroup wg, String agentName, int index) throws Exception {
		JID agentJID = prepJID(wg.getWorkgroupName(), agentName);
		Agent agent = wg.getAgentManager().getAgent(agentJID);
		
		MacroGroup parentGroup = agentMacros.getMacroGroup(wg.getWorkgroupName(), agent);
		if (parentGroup == null) {
			throw new Exception("Parent group not found");
		}
		
		Macro macro = parentGroup.getMacro(index);
		if (macro == null) {
			throw new Exception("Macro not found");
		}
		parentGroup.removeMacro(macro);
		agentMacros.saveMacros(wg.getWorkgroupName(), agent, parentGroup);
		
		return "\"ok\"";
	}
	
	private String listAgentMacro(Workgroup wg, String agentName) throws Exception {
		JID agentJID = prepJID(wg.getWorkgroupName(), agentName);
		Agent agent = wg.getAgentManager().getAgent(agentJID);
		
		JSONArray jsonArray = new JSONArray();
		MacroGroup group = agentMacros.getMacroGroup(wg.getWorkgroupName(), agent);
		if (group != null) {
			List<Macro> macros = group.getMacros();
			int counter = 0;
			for (Macro macro : macros) {
				JSONObject json = new JSONObject();
				
				json.put("title", macro.getTitle());
				json.put("body", macro.getResponse());
				json.put("index", counter++);
				
				jsonArray.add(json);
			}
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", jsonArray.size());
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	/**
	 * List all user in certain workgroup
	 * 
	 * @param wgName
	 * @param limit
	 * @param offet
	 * @return
	 * @throws Exception
	 */
	private String listAgent(Workgroup wg, int limit, int offet) throws Exception {
		List<WorkgroupUserDTO> users = WorkgroupAgentManager.getInstance().fetchUserInWorkgroup(wg.getWorkgroupName(), limit, offet);
		
		JSONArray jsonArray = new JSONArray();
		JID agentJID;
		Map<Long, ArrayList<String>> agentQueue = WorkgroupAgentManager.getInstance().fetchAgentQueue(wg.getID(), wg.getWorkgroupName());
		for (WorkgroupUserDTO user: users) {
			JSONObject json = new JSONObject();
			
			json.put("username", user.getUsername());
			json.put("name", user.getName());
			json.put("email", user.getEmail());
			
			List<String> divisions = new ArrayList<String>();
			try {
				agentJID = new JID(user.getUsername(), ComponentManagerFactory.getComponentManager().getServerName(), null);
				Agent agent = wg.getAgentManager().getAgent(agentJID);
				
				json.put("isAgent", true);
				
				divisions = agentQueue.get(agent.getID());
			} catch (Exception e) {
				json.put("isAgent", false);
			}
			json.put("divisions", divisions);
			
			jsonArray.add(json);
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", WorkgroupAgentManager.getInstance().countUserInWorkgroup(wg.getWorkgroupName()));
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	/**
	 * Search individual agent
	 * 
	 * @param wgName
	 * @param agentName
	 * @return
	 * @throws UserNotFoundException
	 */
	private String searchAgent(String wgName, String agentName) throws UserNotFoundException {
		JID agentJID = prepJID(wgName, agentName);
		User user = userManager.getUser(agentJID.getNode());
		StringBuffer buffer = new StringBuffer("");
		
		buffer.append('{');
		buffer.append("\"username\": \"" + user.getUsername() + "\"");
		buffer.append(", \"name\": \"" + user.getName() + "\"");
		buffer.append(", \"email\": \"" + user.getEmail() + "\"");
		buffer.append('}');
		
		return buffer.toString();
	}
	
	/**
	 * Edit individual agent
	 * 
	 * @param wgName
	 * @param agentName
	 * @param realName
	 * @param email
	 * @return
	 * @throws Exception
	 */
	private String editAgent(String wgName, String agentName, String realName, String email) throws Exception {
		JID agentJID = prepJID(wgName, agentName);
		User user = userManager.getUser(agentJID.getNode());
		
		user.setName(realName);
		user.setEmail(email);
		
		return "\"ok\"";
	}
	
	/**
	 * Change user password from admin panel, doesn't require old password
	 * 
	 * @param wgName
	 * @param agentName
	 * @param password1
	 * @param password2
	 * @return
	 * @throws Exception
	 */
	public String changePassword(String wgName, String agentName, String password1, String password2) throws Exception {
		if (!password1.equals(password2)) {
			throw new Exception("Password confirmation doesn't match");
		}
		
		JID agentJID = prepJID(wgName, agentName);
		User user = userManager.getUser(agentJID.getNode());
		
		user.setPassword(password1);
		
		return "\"ok\"";
	}
	
	/**
	 * Create agent in certain workgroup, using user-workgroupname as a template and also default password
	 * 
	 * @param wgName
	 * @param agentName
	 * @param realName
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public String createAgent(Workgroup wg, String agentName, String password, String realName, String email) throws Exception {
		JID agentJID = prepJID(wg.getWorkgroupName(), agentName);
		userManager.createUser(agentJID.getNode(), password, realName, email);
		
		// Add the new agent to Internal Group
		try {
			Group group = groupManager.getGroup(wg.getWorkgroupName() + "-group");
			group.getMembers().add(agentJID);
		} catch (GroupNotFoundException e) {
		}
		
		// Add the new agent to Default Queue
		try {
			RequestQueue queue = wg.getRequestQueue("Default Queue");
			Agent agent = wg.getAgentManager().createAgent(agentJID);
			
			queue.addMember(agent);
			
			// Add an "Auto Greeting" personal macro
			addAgentMacro(wg, agentName, "Auto Greeting", "Hello, I'm " + realName + " what can I help you with?");
		} catch (Exception e) {
		}
		
		return "\"ok\"";
	}
	
	public String createAgent(Workgroup wg, String agentName, String realName, String email) throws Exception {
		return createAgent(wg, agentName, agentName + "-" + wg.getWorkgroupName(), realName, email);
	}
	
	/**
	 * Delete agent in a certain workgroup
	 * 
	 * @param wg
	 * @param wgName
	 * @param agentName
	 * @return
	 * @throws Exception
	 */
	private String deleteAgent(Workgroup wg, String wgName, String agentName) throws Exception {
		JID agentJID = prepJID(wgName, agentName);
		User user = userManager.getUser(agentJID.getNode());
		userManager.deleteUser(user);
		
		// Delete the agent from group
		try {
			Group group = groupManager.getGroup(wgName + "-group");
			group.getMembers().remove(agentJID);
			group.getAdmins().remove(agentJID);
		} catch (GroupNotFoundException e) {
		}
		
		// Also delete it from Queue / Division
		AgentManager agentManager = wg.getAgentManager();
		try {
			Agent agent = agentManager.getAgent(agentJID);
			
			for (RequestQueue queue : wg.getRequestQueues()) {
				queue.removeMember(agent);
			}
		} catch (Exception e) {
		} finally {
			agentManager.deleteAgent(agentJID);
		}
		
		return "\"ok\"";
	}
	
	private boolean agentExist(Workgroup wg, JID agentJID) throws Exception {
		try {
			wg.getAgentManager().getAgent(agentJID);
			return true;
		} catch (AgentNotFoundException e) {
			return false;
		}
	}
	
	@Override
	public void close() {
		super.close();
		
		userManager = null;
	}
	
}
