package com.kemasdimas.workgroupservice.dto;

public class WorkgroupAutomessageDTO {
	private String workgroupNode;
	private long queueId;
	private String name;
	private String label;
	private String value;
	
	public WorkgroupAutomessageDTO(String workgroupNode, long queueId,
			String name, String label, String value) {
	
		super();
		this.workgroupNode = workgroupNode;
		this.queueId = queueId;
		this.name = name;
		this.label = label;
		this.value = value;
	}

	public String getWorkgroupNode() {
		return workgroupNode;
	}
	
	public void setWorkgroupNode(String workgroupNode) {
		this.workgroupNode = workgroupNode;
	}
	
	public long getQueueId() {
		return queueId;
	}
	
	public void setQueueId(long queueId) {
		this.queueId = queueId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
