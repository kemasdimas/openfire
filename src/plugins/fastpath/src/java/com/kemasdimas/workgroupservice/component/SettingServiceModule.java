package com.kemasdimas.workgroupservice.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.jivesoftware.openfire.fastpath.dataforms.FormElement;
import org.jivesoftware.openfire.fastpath.dataforms.FormManager;
import org.jivesoftware.openfire.fastpath.dataforms.WorkgroupForm;
import org.jivesoftware.openfire.fastpath.dataforms.WorkgroupForm.FormEnum;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSetting;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSettings;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSettingsManager;
import org.jivesoftware.openfire.fastpath.settings.chat.KeyEnum;
import org.jivesoftware.xmpp.workgroup.RequestQueue;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.dispatcher.DispatcherInfo;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.kemasdimas.wgchat.dataforms.SurveyFormManager;

import sun.reflect.generics.tree.FieldTypeSignature;

public final class SettingServiceModule extends BaseWorkgroupServiceModule {
		
	protected static SettingServiceModule instance;
	private ChatSettingsManager chatSettingManager;
	private FormManager formManager;
	private SurveyFormManager surveyFormManager;
	private final static ArrayList<String> prohibitedFormVariables = new ArrayList<String>(); 
	{
		prohibitedFormVariables.add("reset");
		prohibitedFormVariables.add("username");
	}
	private final static Pattern hiddenChatSettingPattern = Pattern.compile("(command|help|email|acceptable|_sub|_ads)");
	private final static Comparator<ChatSetting> wgChatSettingComparator = new Comparator<ChatSetting>() {
		
		private HashMap<String, Integer> chatSettingSortOrder = new HashMap<String, Integer>();
		{
			// This mapping is used to sort chat setting messages
			int counter = 0;
			
			chatSettingSortOrder.put("welcome_message", counter++);
			
			chatSettingSortOrder.put("join_question", counter++);
			chatSettingSortOrder.put("form_reset_confirmation", counter++);
			chatSettingSortOrder.put("fillout_form_message", counter++);
			
			chatSettingSortOrder.put("position_message", counter++);
			
			chatSettingSortOrder.put("cannot_join_message", counter++);
			chatSettingSortOrder.put("send_invitation_question", counter++);
			chatSettingSortOrder.put("invitation_resent_message", counter++);
			chatSettingSortOrder.put("not_in_queue_message", counter++);
			chatSettingSortOrder.put("departure_confirmed_message", counter++);
			chatSettingSortOrder.put("routing_message", counter++);
			chatSettingSortOrder.put("invitation_sent_message", counter++);
			chatSettingSortOrder.put("bye_message", counter++);
			
			chatSettingSortOrder.put("workgroup_closed_message", counter++);
			chatSettingSortOrder.put("survey_join_message", counter++);
			chatSettingSortOrder.put("survey_bye_message", counter++);
			chatSettingSortOrder.put("one_way_message_info", counter++);
		}
		
		@Override
		public int compare(ChatSetting o1, ChatSetting o2) {
			String key1 = o1.getKey().toString();
			String key2 = o2.getKey().toString();
			
			Integer priority1 = chatSettingSortOrder.get(key1);
			Integer priority2 = chatSettingSortOrder.get(key2);
			
			return priority1.compareTo(priority2);
		}
	};
	
	public SettingServiceModule() {
		super();
		
		chatSettingManager = ChatSettingsManager.getInstance();
	}
	
	public static SettingServiceModule getInstance() {
		if (instance == null) {
			instance = new SettingServiceModule();
		}
		
		return (SettingServiceModule) instance;
	}
	
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		Workgroup wg = findWorkgroup(wgName);
				
		if ("whitelabel-setting".equals(type)) {
			boolean flag = Boolean.parseBoolean(request.getParameter("adsFlag"));
			
			return modifyWorkgroupWhitelabel(wg, flag);
		} else if ("list-chat-setting".equals(type)) {
			return listChatSettings(wg);
		} else if ("edit-chat-setting".equals(type)) {
			String key = request.getParameter("key");
			String value = request.getParameter("value");
			
			if (key == null && value == null) {
				throw new Exception("Key - value pair can not be null");
			}
			
			return editChatSetting(wgName, key, value);
		} else if ("list-general-setting".equals(type)) {
			return listGeneralSetting(wg);
		} else if ("edit-general-setting".equals(type)) {
			
			int maxSession = Integer.parseInt(request.getParameter("maxSession"));
			int minSession = Integer.parseInt(request.getParameter("minSession"));
			int reqTimeout = Integer.parseInt(request.getParameter("requestTimeout"));
			int offerTimeout = Integer.parseInt(request.getParameter("offerTimeout"));
			int idleTimeout = Integer.parseInt(request.getParameter("botIdleTimeout"));
			
			return editGeneralSetting(wg, maxSession, minSession, reqTimeout, offerTimeout, idleTimeout);
			
		} else if (type.contains("question") || type.contains("survey")) {
			if (formManager == null) {
				formManager = FormManager.getInstance();
			}
			
			if (surveyFormManager == null) {
				surveyFormManager = SurveyFormManager.getInstance();
			}
			
			// Check whether it is a question / survey
			WorkgroupForm wgForm = null;
			boolean isSurvey = false;
			if (type.contains("survey")) {
				wgForm = surveyFormManager.getWebForm(wg);
				isSurvey = true;
				
				// Replace the 'question' substring with 'survey' to reuse existing
				// conditional block.
				type = type.replace("survey", "question");
			} else {
				wgForm = formManager.getWebForm(wg);
			}
			
			if ("list-question-setting".equals(type)) {
				return listFormElement(wgForm);
			} 
			
			int position = -1;
			if (request.getParameter("fieldPosition") != null) {
				try {
					position = Integer.parseInt(request.getParameter("fieldPosition"));
				} catch (Exception e) {
				}
			}
			
			if ("delete-question-setting".equals(type)) {
				return deleteFormElement(isSurvey, wg, wgForm, position);
			}else if ("order-question-setting".equals(type)) {
				String direction = request.getParameter("direction");
				if (direction == null) {
					throw new Exception("Direction can not be null");
				}
				
				return orderFormElement(isSurvey, wg, wgForm, position, direction);
			}
			
			String name = request.getParameter("fieldName");
			
			// If it is a survey field, add 'survey_' prefix to the name
			if (isSurvey) {
				name = "survey_" + name;
			}
			
			String label = request.getParameter("fieldLabel");
			String description = request.getParameter("fieldDescription");
			String fieldType = request.getParameter("fieldType");
			String listValue = request.getParameter("listValue");
			boolean isRequired = Boolean.parseBoolean(request.getParameter("fieldRequired"));
			
			if (name == null || label == null || fieldType == null) {
				throw new Exception("fieldName / fieldLabel / fieldType can not be null");
			}
			
			if ("create-question-setting".equals(type)) {
				return saveFormElement(isSurvey, wg, wgForm, false, name, label, description, isRequired, -1, fieldType, listValue);
			} else if ("edit-question-setting".equals(type)) {
				return saveFormElement(isSurvey, wg, wgForm, true, name, label, description, isRequired, position, fieldType, listValue);
			}
		}
		
		throw new Exception("Type not supported");
	}
	
	/**
	 * Add / remove the ads in a workgroup
	 * 
	 * @param wg
	 * @param isWhitelabel
	 * @return
	 * @throws Exception
	 */
	private String modifyWorkgroupWhitelabel(Workgroup wg, boolean isWhitelabel) throws Exception {
		
		if (isWhitelabel) {
			editChatSetting(wg.getWorkgroupName(), KeyEnum.workgroup_bottom_ads.toString(), "");
		} else {
			ChatSetting adsSetting = chatSettingManager.getChatSetting(wg, KeyEnum.workgroup_bottom_ads.toString());
			editChatSetting(wg.getWorkgroupName(), KeyEnum.workgroup_bottom_ads.toString(), adsSetting.getDefaultValue());
		}
		
		return "\"ok\""; 
	}
	
	/**
	 * List general setting (max sessions, min sessions, request timeout, offer timeout)
	 * 
	 * @param wg
	 * @return
	 */
	private String listGeneralSetting(Workgroup wg) {
		JSONObject json = new JSONObject();
		
		json.put("maxSession", wg.getMaxChats());
		json.put("minSession", wg.getMinChats());
		json.put("requestTimeout", wg.getRequestTimeout() / 1000);
		json.put("offerTimeout", wg.getOfferTimeout() / 1000);
		
		if (wg.isChatbotEnabled()) {
			// Show in minutes
			json.put("botIdleTimeout", wg.getChatBot().getIdleTimeout() / 60000);
		}
		
		return json.toString();
	}
	
	/**
	 * Edit general setting
	 * 
	 * @param wg
	 * @param maxSession
	 * @param minSession
	 * @param reqTimeout
	 * @param offerTimeout
	 * @return
	 * @throws Exception
	 */
	private String editGeneralSetting(Workgroup wg, int maxSession, int minSession, int reqTimeout, int offerTimeout, int idleTimeout) throws Exception {
		if (maxSession < 0 || minSession < 0 || reqTimeout < 0 || offerTimeout < 0 || idleTimeout < 0) {
			throw new Exception("Numbers can not be null");
		}
		
		long reqTimeoutL = reqTimeout * 1000L;
		long offerTimeoutL = offerTimeout * 1000L;
		boolean requestChanged = (wg.getRequestTimeout() != reqTimeoutL);
		boolean offerChanged = (wg.getOfferTimeout() != offerTimeoutL);
		
		wg.setRequestTimeout(reqTimeoutL);
		wg.setOfferTimeout(offerTimeoutL);
		wg.setMaxChats(maxSession);
		wg.setMinChats(minSession);
		
		if (wg.isChatbotEnabled()) {
			if (idleTimeout < 3) {
				idleTimeout = 3;
			}
			
			wg.getChatBot().setIdleTimeout(idleTimeout * 60 * 1000);
		}
		
		// Iterate all queues and set the timeout accordingly
		for (RequestQueue queue : wg.getRequestQueues()) {
			if (requestChanged || offerChanged) {
				DispatcherInfo infos = queue.getDispatcher().getDispatcherInfo();
				
				if (requestChanged) {
					infos.setRequestTimeout(reqTimeoutL);
				}
				
				if (offerChanged) {
					infos.setOfferTimeout(offerTimeoutL);
				}
				
				queue.getDispatcher().setDispatcherInfo(infos);
			}
		}
		
		return "\"ok\"";
	}
	
	private void saveForm(boolean isSurvey, Workgroup wg) {
		if (isSurvey) {
			surveyFormManager.saveWorkgroupForm(wg);
		} else {
			formManager.saveWorkgroupForm(wg);
		}
	}
	
	/**
	 * Delete a bot question in a workgroup
	 * 
	 * @param wg
	 * @param wgForm
	 * @param position
	 * @return
	 * @throws Exception
	 */
	private String deleteFormElement(boolean isSurvey, Workgroup wg, WorkgroupForm wgForm, int position) throws Exception {
		if (position < 0) {
			throw new Exception("Position not valid");
		}
		
		FormElement element = wgForm.getFormElementAt(position);
		if (element.getVariable().equals("username")) {
			// Variable UserName cannot be deleted
			throw new Exception("Variable UserName cannot be deleted");
		}
		
		wgForm.removeFormElement(position);
		saveForm(isSurvey, wg);
		
		return "\"ok\"";
	}
	
	/**
	 * Check whether a variable name has been used in a FormElement
	 * 
	 * @param wgForm
	 * @param name
	 * @return
	 */
	private boolean isVariableExists(WorkgroupForm wgForm, String name) {
		Collection<FormElement> elements = Collections.unmodifiableCollection(wgForm.getFormElements());
		
		for (FormElement formElement : elements) {
			if (formElement.getVariable().equalsIgnoreCase(name)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Add / edit a bot question in a workgroup, currently only supports the textfield answerType
	 * 
	 * @param wg
	 * @param wgForm
	 * @param isEdit
	 * @param name
	 * @param label
	 * @param description
	 * @param isRequired
	 * @param position
	 * @return
	 * @throws Exception
	 */
	private String saveFormElement(boolean isSurvey, Workgroup wg, WorkgroupForm wgForm, boolean isEdit, String name, String label, String description, boolean isRequired, int position, String type, String listValues) throws Exception {	
		if (isEdit && position < 0) {
			throw new Exception("Position not valid");
		}
		
		// Replace whitespace with _
		name = name.replace(" ", "_");
		boolean isProhibited = prohibitedFormVariables.contains(name);
		boolean isExists = isVariableExists(wgForm, name);
		
		if (!isEdit && isProhibited) {
			throw new Exception("Variable name can not be (" + prohibitedFormVariables + ")");
		} else if (!isEdit && isExists) {
			throw new Exception("Variable " + name + " has already been used.");
		}
		
		FormElement element;
		if (isEdit) {
			element = wgForm.getFormElementAt(position);
			isProhibited = prohibitedFormVariables.contains(element.getVariable());
		} else {
			element = new FormElement();
			element.setVisible(true);
			wgForm.addFormElement(element);
		}
		
		if (!isProhibited && !isExists) {
			element.setVariable(name);
		}
		element.setLabel(label);
		element.setDescription((description == null ? "" : description));
		element.setRequired(isRequired);
		
		// Check whether the form is textfield / radiobutton
		if (type.equals(FormEnum.radio_button.toString())) {
			element.setAnswerType(FormEnum.radio_button);
			
			if (listValues == null) {
				throw new Exception("Radio button must have listValue");
			}
			
			// Split the listValue by ',' and put it into the field answers!
			String values[] = listValues.split(",");
			List<String> vList = new ArrayList<String>(values.length);
			
			for (int i = 0; i < values.length; i++) {
				vList.add(values[i].trim());
			}
			element.setAnswers(vList);
		} else {
			element.setAnswerType(FormEnum.textfield);
			element.getAnswers().clear();
		}
		saveForm(isSurvey, wg);
		
		return "\"ok\"";
	}
	
	/**
	 * List all bot questions in a workgroup
	 * 
	 * @param wgForm
	 * @return
	 */
	private String listFormElement(WorkgroupForm wgForm) {		
		JSONArray jsonArray = new JSONArray();
		for (FormElement element : wgForm.getFormElements()) {
			boolean isHidden = (element.getAnswerType() == WorkgroupForm.FormEnum.hidden);
			
			if (!isHidden) {
				JSONObject json = new JSONObject();
				FormEnum type = element.getAnswerType();
				String name = element.getVariable();

				// Remove 'survey_' prefix
				if (name.startsWith("survey_")) {
					name = name.replace("survey_", "");
				}

				json.put("name", name);
				json.put("label", element.getLabel());
				json.put("description", (element.getDescription() == null ? "" : element.getDescription()));
				json.put("type", type.toString());
				json.put("required", element.isRequired());
				
				if (type == FormEnum.radio_button) {
					List<String> answersList = element.getAnswers();
					StringBuffer buffer = new StringBuffer();
					
					for (String answer : answersList) {
						buffer.append(answer + ", ");
					}
					json.put("answerList", buffer.toString().substring(0, buffer.length() - 2));
				} else {
					json.put("answerList", "-");
				}
				
				jsonArray.add(json);
			}
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", jsonArray.size());
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	private String orderFormElement(boolean isSurvey, Workgroup wg, WorkgroupForm wgForm, int position, String direction) throws Exception {
		if (position < 0) {
			throw new Exception("Position not valid");
		} else if (!("up".equals(direction) || "down".equals(direction))) {
			throw new Exception("Direction must be up / down");
		}
		
		FormElement element = wgForm.getFormElementAt(position);
		int count = wgForm.getFormElements().size();
		
		if ("up".equals(direction) && position > 0) {
			
			wgForm.removeFormElement(position);
			wgForm.addFormElement(element, position - 1);
		} else if ("down".equals(direction) && position < count - 1) {
			
			wgForm.removeFormElement(position);
			wgForm.addFormElement(element, position + 1);
		}
		saveForm(isSurvey, wg);
		
		return "\"ok\"";
	}
	
	/**
	 * Edit a chat setting in a workgroup
	 * 
	 * @param wgName
	 * @param key
	 * @param value
	 * @return
	 */
	public String editChatSetting(String wgName, String key, String value) {
		ChatSetting setting = new ChatSetting(key);
		setting.setWorkgroupNode(wgName);
		setting.setValue(value);
		
		chatSettingManager.updateChatSetting(setting);
		
		return "\"ok\"";
	}
	
	private List<ChatSetting> simplifyChatSettings(List<ChatSetting> oldSettings) throws Exception {
		// List<ChatSetting> tempSettings = new ArrayList<ChatSetting>(oldSettings);
		
		Matcher keyMatcher = null;
		for (int i = 0; i < oldSettings.size(); i++) {
			keyMatcher = hiddenChatSettingPattern.matcher(oldSettings.get(i).getKey().toString());
			
			if (keyMatcher.find()) {
				oldSettings.remove(i);
				i--;
			}
		}
		
		Collections.sort(oldSettings, wgChatSettingComparator);
		
		return oldSettings;
	}
	
	/**
	 * List bot chat settings in a workgroup
	 * 
	 * @param wg
	 * @return
	 * @throws Exception
	 */
	private String listChatSettings(Workgroup wg) throws Exception {
		List<ChatSetting> settings = chatSettingManager.getChatSettings(wg).
				getChatSettingsByType(ChatSettings.SettingType.bot_settings);
		
		settings = simplifyChatSettings(settings);
		
		JSONArray jsonArray = new JSONArray();
		for (ChatSetting setting : settings) {
			JSONObject json = new JSONObject();
			
			json.put("key", setting.getKey().toString());
			json.put("value", setting.getValue());
			json.put("label", setting.getLabel());
			
			jsonArray.add(json);
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", jsonArray.size());
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	@Override
	public void close() {
		chatSettingManager = null;
		formManager = null;
		surveyFormManager = null;
		
		super.close();
	}
}
