package com.kemasdimas.workgroupservice.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jivesoftware.util.StringUtils;
import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;

import com.kemasdimas.workgroupservice.dto.WorkgroupAgentLoginHistoryDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupReportItemDTO;

/**
 * This class has an architectural flaw, the JSONArray should be used outside this class,
 * but in the trade of this flaw, better memory performance is expected.
 * 
 * @author kemas
 *
 */
public final class WorkgroupReportManager {
	
	private static String GROUP_MONTHLY = "%Y-%m";
	private static String GROUP_DAILY = "%Y-%m-%d";
	private static String GROUP_HOURLY = "%H";
	
	// This mechanism won't allow for flexibel query by timezone, should be changed!
	private static String MIN_HOURS = "00:00:00";
	private static String MAX_HOURS = "23:59:59";
	private static String MIN_DATE = "1989-01-01";
	
	private static String DIVISION_META = "Division";
	private static String TAG_META = "tag#%";
	private static String IM_META = "imUsername";
	
	private static int MAX_DAILY_GROUP = 365;
	
	/* VERSION 1
	private static String COUNT_BY_TIMEFRAME = "SELECT count(*) as count, DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time " +
			"FROM (SELECT	sessionID, startTime FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) " +
			"BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND state = 2) s " +
			"GROUP BY DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) ORDER BY startTime ASC";
	
	private static String COUNT_UNANSWERED_BY_TIMEFRAME = "SELECT count(*) as count, DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time " +
			"FROM (SELECT	sessionID, startTime FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) " +
			"BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND state != 2) s " +
			"GROUP BY DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) ORDER BY startTime ASC";
	
	private static String COUNT_PER_AGENT = "SELECT count(*) as count, agentJID FROM (SELECT sessionID, startTime " +
			"FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) " +
			"AND CAST(? AS DATETIME)) s JOIN fpAgentSession a ON a.sessionID = s.sessionID GROUP BY agentJID";
	
	private static String COUNT_BY_TIMEFRAME_BY_AGENT = "SELECT count(*) as count, DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time " +
			"FROM (SELECT sessionID, startTime FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) " +
			"BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)) s JOIN fpAgentSession a ON a.sessionID = s.sessionID " +
			"GROUP BY DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?), agentJID HAVING	agentJID = ? ORDER BY startTime ASC";
	
	private static String COUNT_PER_METADATA = "SELECT count(*) as count, metadataValue as value FROM (SELECT sessionID, startTime " +
			"FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)) s " +
			"JOIN fpSessionMetadata m ON m.sessionID = s.sessionID GROUP BY metadataValue, metadataName HAVING metadataName LIKE ?";
	
	private static String COUNT_BY_TIMEFRAME_BY_METADATA = "SELECT count(*) as count, DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time " +
			"FROM (SELECT sessionID, startTime FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) " +
			"AND CAST(? AS DATETIME)) s JOIN fpSessionMetadata m ON m.sessionID = s.sessionID GROUP BY DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?), " +
			"metadataValue, metadataName HAVING metadataName LIKE ? AND metadataValue = ? ORDER BY startTime ASC";
	*/
	
	// VERSION 2
	private static String COUNT_BY_TIMEFRAME = "SELECT DISTINCT IFNULL(count, 0) as count, DATE_FORMAT(c.datefield, ?) as time FROM (SELECT count(*) as count, " +
			"DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time FROM (SELECT sessionID, startTime FROM fpSession WHERE workgroupID = ? AND " +
			"FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND state = 2) s GROUP BY DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) " +
			"ORDER BY startTime ASC) a RIGHT JOIN calendar c ON time = DATE_FORMAT(c.datefield, ?) WHERE c.datefield BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)";
	
	private static String COUNT_UNANSWERED_BY_TIMEFRAME = "SELECT DISTINCT IFNULL(count, 0) as count, DATE_FORMAT(c.datefield, ?) as time FROM (SELECT count(*) as count, " +
			"DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time FROM (SELECT sessionID, startTime FROM fpSession WHERE workgroupID = ? AND " +
			"FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND state != 2) s GROUP BY DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) " +
			"ORDER BY startTime ASC) a RIGHT JOIN calendar c ON time = DATE_FORMAT(c.datefield, ?) WHERE c.datefield BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)";
	
	private static String COUNT_PER_AGENT = "SELECT count(*) as count, agentJID FROM (SELECT sessionID, startTime " +
			"FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) " +
			"AND CAST(? AS DATETIME) AND state = 2) s JOIN fpAgentSession a ON a.sessionID = s.sessionID GROUP BY agentJID ORDER BY count DESC";
	
	private static String COUNT_BY_TIMEFRAME_BY_AGENT = "SELECT DISTINCT IFNULL(count, 0) as count, DATE_FORMAT(c.datefield, ?) as time FROM (SELECT count(*) as count, " +
			"DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time FROM (SELECT sessionID, startTime FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) " +
			"BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND state = 2) s JOIN fpAgentSession a ON a.sessionID = s.sessionID GROUP BY " +
			"DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?), agentJID HAVING agentJID = ? ORDER BY startTime ASC) a RIGHT JOIN calendar c " +
			"ON time = DATE_FORMAT(c.datefield, ?) WHERE c.datefield BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)";
	
	private static String COUNT_PER_METADATA = "SELECT count(*) as count, metadataValue as value FROM (SELECT sessionID, startTime " +
			"FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND state = 2) s " +
			"JOIN fpSessionMetadata m ON m.sessionID = s.sessionID WHERE metadataName LIKE ? GROUP BY metadataValue ORDER BY count DESC";
	
	private static String COUNT_BY_TIMEFRAME_BY_METADATA = "SELECT DISTINCT IFNULL(count, 0) as count, DATE_FORMAT(c.datefield, ?) as time FROM (SELECT count(*) as count, " +
			"DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?) time FROM (SELECT sessionID, startTime FROM fpSession WHERE workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) " +
			"BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND state = 2) s JOIN fpSessionMetadata m ON m.sessionID = s.sessionID  WHERE metadataName LIKE ? GROUP BY	" +
			"DATE_FORMAT(FROM_UNIXTIME(startTime / 1000), ?), metadataValue HAVING metadataValue = ? ORDER BY startTime ASC) a " +
			"RIGHT JOIN calendar c ON time = DATE_FORMAT(c.datefield, ?) WHERE c.datefield BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)";
	
	private static String GET_FORM_METADATA = "SELECT DISTINCT metadataName FROM fpSessionMetadata m JOIN (SELECT sessionID FROM fpSession WHERE workgroupID = ? " +
			"AND transcript IS NOT NULL ORDER BY startTime DESC LIMIT 10000) s ON s.sessionID = m.sessionID WHERE metadataName NOT LIKE 'tag#%' AND metadataName " +
			"NOT IN ('Division', 'userID', 'username', 'TransportType', 'channel', 'imUsername');";
	
	private static String COUNT_LOGIN_HISTORIES = "SELECT count(*) total FROM fpAgentSession WHERE agentJID LIKE ? AND sessionID = '' AND " +
			"FROM_UNIXTIME(joinTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)";
	
	private static String GET_LOGIN_HISTORIES = "SELECT agentJID, joinTime FROM fpAgentSession WHERE agentJID LIKE ? AND sessionID = '' AND " +
			"FROM_UNIXTIME(joinTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) ORDER BY joinTime DESC";
	
	// Update script
	private static String INSERT_CHATBOT_REQUEST = "INSERT INTO fpChatbotRequest VALUES (?, ?, ?)";
	
	private static WorkgroupReportManager instance;
	
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	
	public static WorkgroupReportManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupReportManager();
		}
		
		return instance;
	}
	
	private String getTimeGrouping(String date1, String date2) {
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		try {
			start.setTime(dateFormatter.parse(date1));
			end.setTime(dateFormatter.parse(date2));
		} catch (ParseException e) {
		}
		long diff = end.getTimeInMillis() - start.getTimeInMillis();
		long diffDays = Math.abs(diff / (24 * 60 * 60 * 1000));
		
		if (date1.equals(date2)) {
			return GROUP_HOURLY;
		} else if (diffDays >= MAX_DAILY_GROUP) {
			return GROUP_MONTHLY;
		} else {
			return GROUP_DAILY;
		}
	}
	
	private List<WorkgroupReportItemDTO> fetchGeneralCount(String wgName, long wgId, String start, String end, String sql) {
		String grouping = getTimeGrouping(start, end);
		String dummyStart = start;
		String dummyEnd = end;
		if (start.equals(end)) {
			dummyStart = MIN_DATE;
			dummyEnd = MIN_DATE;
		}
		
		ArrayList<WorkgroupReportItemDTO> reportItems = new ArrayList<WorkgroupReportItemDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(sql);
			
			statement.setString(1, grouping);
			statement.setString(2, grouping);
			statement.setLong(3, wgId);
			statement.setString(4, start + " " + MIN_HOURS);
			statement.setString(5, end + " " + MAX_HOURS);
			statement.setString(6, grouping);
			statement.setString(7, grouping);
			statement.setString(8, dummyStart + " " + MIN_HOURS);
			statement.setString(9, dummyEnd + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {				
				reportItems.add(new WorkgroupReportItemDTO(result.getString("time"), result.getLong("count")));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return reportItems;
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerTimeframe(String wgName, long wgId, String start, String end) {
		return fetchGeneralCount(wgName, wgId, start, end, COUNT_BY_TIMEFRAME);
	}
	
	public List<WorkgroupReportItemDTO> fetchUnansweredCountPerTimeframe(String wgName, long wgId, String start, String end) {
		return fetchGeneralCount(wgName, wgId, start, end, COUNT_UNANSWERED_BY_TIMEFRAME);
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerAgent(String wgName, long wgId, String start, String end) {
		String grouping = getTimeGrouping(start, end);
		ArrayList<WorkgroupReportItemDTO> reportItems = new ArrayList<WorkgroupReportItemDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(COUNT_PER_AGENT);
			
			statement.setLong(1, wgId);
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, end + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				reportItems.add(new WorkgroupReportItemDTO(result.getString("agentJID"), result.getLong("count")));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return reportItems;
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerTimeframeByAgent(String wgName, long wgId, String start, String end, String filter) {
		String grouping = getTimeGrouping(start, end);
		String dummyStart = start;
		String dummyEnd = end;
		if (start.equals(end)) {
			dummyStart = MIN_DATE;
			dummyEnd = MIN_DATE;
		}
		ArrayList<WorkgroupReportItemDTO> reportItems = new ArrayList<WorkgroupReportItemDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(COUNT_BY_TIMEFRAME_BY_AGENT);
			
			statement.setString(1, grouping);
			statement.setString(2, grouping);
			statement.setLong(3, wgId);
			statement.setString(4, start + " " + MIN_HOURS);
			statement.setString(5, end + " " + MAX_HOURS);
			statement.setString(6, grouping);
			statement.setString(7, filter);
			statement.setString(8, grouping);
			statement.setString(9, dummyStart + " " + MIN_HOURS);
			statement.setString(10, dummyEnd + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				reportItems.add(new WorkgroupReportItemDTO(result.getString("time"), result.getLong("count")));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return reportItems;
	}

	public List<WorkgroupReportItemDTO> fetchCountPerMetadata(String wgName, long wgId, String start, String end, String metadata) {
		String grouping = getTimeGrouping(start, end);
		ArrayList<WorkgroupReportItemDTO> reportItems = new ArrayList<WorkgroupReportItemDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(COUNT_PER_METADATA);
			
			statement.setLong(1, wgId);
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, end + " " + MAX_HOURS);
			statement.setString(4, metadata);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				reportItems.add(new WorkgroupReportItemDTO(result.getString("value"), result.getLong("count")));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return reportItems;
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerDivision(String wgName, long wgId, String start, String end) {
		return fetchCountPerMetadata(wgName, wgId, start, end, DIVISION_META);
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerTag(String wgName, long wgId, String start, String end) {
		return fetchCountPerMetadata(wgName, wgId, start, end, TAG_META);
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerIm(String wgName, long wgId, String start, String end) {
		return fetchCountPerMetadata(wgName, wgId, start, end, IM_META);
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerTimeframeByMetadata(String wgName, long wgId, String start, String end, String metadataName, String metadataValue) {
		String grouping = getTimeGrouping(start, end);
		String dummyStart = start;
		String dummyEnd = end;
		if (start.equals(end)) {
			dummyStart = MIN_DATE;
			dummyEnd = MIN_DATE;
		}
		ArrayList<WorkgroupReportItemDTO> reportItems = new ArrayList<WorkgroupReportItemDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(COUNT_BY_TIMEFRAME_BY_METADATA);
			
			statement.setString(1, grouping);
			statement.setString(2, grouping);
			statement.setLong(3, wgId);
			statement.setString(4, start + " " + MIN_HOURS);
			statement.setString(5, end + " " + MAX_HOURS);
			statement.setString(6, metadataValue);
			statement.setString(7, grouping);
			statement.setString(8, metadataName);
			statement.setString(9, grouping);
			statement.setString(10, dummyStart + " " + MIN_HOURS);
			statement.setString(11, dummyEnd + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {				
				reportItems.add(new WorkgroupReportItemDTO(result.getString("time"), result.getLong("count")));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return reportItems;
	}
	
	/**
	 * Get the form and survey form metadata list from database
	 * 
	 * @param wgName
	 * @return list of queriable metadata
	 */
	public List<String> fetchFormMetadataList(String wgName, long wgId) {
		ArrayList<String> buffer = new ArrayList<String>(10);
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(GET_FORM_METADATA);
			statement.setLong(1, wgId);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {				
				buffer.add(result.getString("metadataName"));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerTimeframeByDivision(String wgName, long wgId, String start, String end, String filter) {
		return fetchCountPerTimeframeByMetadata(wgName, wgId, start, end, filter, DIVISION_META);
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerTimeframeByTag(String wgName, long wgId, String start, String end, String filter) {
		return fetchCountPerTimeframeByMetadata(wgName, wgId, start, end, filter, TAG_META);
	}
	
	public List<WorkgroupReportItemDTO> fetchCountPerTimeframeByIm(String wgName, long wgId, String start, String end, String filter) {
		return fetchCountPerTimeframeByMetadata(wgName, wgId, start, end, filter, IM_META);
	}
	
	public List<WorkgroupAgentLoginHistoryDTO> fetchLoginHistories(String wgName, String start) {
		ArrayList<WorkgroupAgentLoginHistoryDTO> reportItems = new ArrayList<WorkgroupAgentLoginHistoryDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(GET_LOGIN_HISTORIES);
			
			statement.setString(1, "%-" + wgName + "@%");
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, start + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				reportItems.add(new WorkgroupAgentLoginHistoryDTO(result.getString("agentJID"), result.getLong("joinTime")));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return reportItems;
	}
	
	public long countLoginHistories(String wgName, String start) {
		long histories = 0;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(COUNT_LOGIN_HISTORIES);
			
			statement.setString(1, "%-" + wgName + "@%");
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, start + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				histories = result.getLong("total");
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return histories;
	}
	
	public void insertChatbotRequest(String wgName, long wgId, String customerId) {
        String tempDate = StringUtils.dateToMillis(new Date());

        // Gather all information needed.
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection(wgName);
            pstmt = con.prepareStatement(INSERT_CHATBOT_REQUEST);
            pstmt.setString(1, customerId);
            pstmt.setString(2, tempDate);
            pstmt.setLong(3, wgId);
            
            pstmt.executeUpdate();
        }
        catch (Exception ex) {
        	
        }
        finally {
           FastpathDBManager.closeConnection(pstmt, con);
        }
    }
}
