package com.kemasdimas.workgroupservice.dto;

public final class WorkgroupTranscriptDetailDTO extends WorkgroupTranscriptDTO {

	public WorkgroupTranscriptDetailDTO(String sessionID) {
		super(sessionID);
	}

	@Override
	public String toJSONString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("{");
		buffer.append("\"sessionId\":\"" + getSessionID() + "\"");
		buffer.append(",\"start\":" + getStartTime());
		buffer.append(",\"end\":" + getEndTime());
		buffer.append(",\"body\":" + getBody());
		buffer.append(",\"note\":" + getNotes());
		buffer.append("}");
		
		return buffer.toString();
	}
}
