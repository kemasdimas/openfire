package com.kemasdimas.workgroupservice.component;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.json.simple.JSONObject;

import com.kemasdimas.workgroupservice.dto.WorkgroupSMSCompositeDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupSMSDto;
import com.kemasdimas.workgroupservice.manager.WorkgroupSmsManager;

public final class SmsServiceModule extends BaseWorkgroupServiceModule {
	
	protected static SmsServiceModule instance;
	private WorkgroupSmsManager smsManager;
	
	private SmsServiceModule() {
		super();
		
		smsManager = WorkgroupSmsManager.getInstance();
	}
	
	public static SmsServiceModule getInstance() {
		if (instance == null) {
			instance = new SmsServiceModule();
		}
		
		return instance;
	}
	
	@Override
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		final Workgroup wg = findWorkgroup(wgName);
		
		// Get all additional parameter before conditional branches
		String registeredNumber = request.getParameter("registeredNumber");
		String customerNumber = request.getParameter("customerNumber");
		
		long limit = 50;
		long offset = 0;
		
		try {
			limit = Long.parseLong(request.getParameter("limit"));
		} catch (Exception e) { }
		
		try {
			offset = Long.parseLong(request.getParameter("offset"));
		} catch (Exception e) { }
		
		if ("sms-list-registered-number".equals(type)) {
			return getRegisteredNumberList(wg);
		} else if ("sms-list-registered-number-message".equals(type)) {
			if (registeredNumber == null) {
				throw new Exception("registeredNumber can not be null");
			}
			
			return getSmsesByRegisteredNumber(wg, registeredNumber, limit, offset);
		} else if ("sms-list-customer-message".equals(type)) {
			if (registeredNumber == null || customerNumber == null) {
				throw new Exception("registeredNumber / customerNumber can not be null");
			}
			
			return getSmsesByCustomerNumber(wg, registeredNumber, customerNumber);
		}
		
		throw new Exception("Type not supported");
	}
	
	private String getSmsesByCustomerNumber(final Workgroup workgroup, final String registeredNumber, final String customerNumber) {
		List<WorkgroupSMSDto> smses = smsManager.getSmsByCustomerNumber(workgroup, registeredNumber, customerNumber);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("list", smses);
		
		return finalJson.toString();
	}
	
	private String getSmsesByRegisteredNumber(final Workgroup workgroup, final String registeredNumber, final long limit, final long offset) {
		List<WorkgroupSMSCompositeDTO> smses = smsManager.getIncomingSmsByRegisteredNumber(workgroup, registeredNumber, limit, offset);
		long smsesCount = smsManager.countSmsForRegisteredNumber(workgroup, registeredNumber);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", smsesCount);
		finalJson.put("data", smses);
		
		return finalJson.toString();
	}
	
	private String getRegisteredNumberList(final Workgroup workgroup) {
		List<String> numbers = smsManager.getRegisteredSmsNumbers(workgroup);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("list", numbers);
		
		return finalJson.toString();
	}
}
