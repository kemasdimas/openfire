package com.kemasdimas.workgroupservice.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.jivesoftware.openfire.fastpath.settings.chat.KeyEnum;
import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;

import com.kemasdimas.workgroupservice.dto.WorkgroupAutomessageDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupAutomessageTypeDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupTranscriptDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupTranscriptDetailDTO;

/**
 * This class mainly uses JSONArray
 * 
 * @author kemas
 *
 */
public final class WorkgroupTranscriptManager {
	
	// This mechanism won't allow for flexibel query by timezone, should be changed!
	private static String MIN_HOURS = "00:00:00";
	private static String MAX_HOURS = "23:59:59";
	
	private static String GET_FILTER_KEY = "SELECT DISTINCT metadataName FROM fpSession s JOIN fpSessionMetadata m ON s.sessionID = m.sessionID AND workgroupID = ? " +
			"AND (m.metadataName NOT LIKE 'tag#%' AND m.metadataName NOT IN ('channel', 'userID', 'reset'));";
	
	/* LOG QUERY VERSION 1
	private static String LOG_FILTER = "SELECT DISTINCT	a.sessionID, startTime, endTime, agentJID, metadataName, metadataValue FROM " +
			"fpSession s JOIN (SELECT DISTINCT a.sessionID FROM (SELECT a.sessionID as sessionID, 'agentName' as metadataName, agentJID as metadataValue, " +
			"startTime FROM fpSession s JOIN fpAgentSession a ON s.sessionID = a.sessionID WHERE state = 2 AND workgroupID = ?	UNION SELECT m.sessionID as sessionID, " +
			"metadataName, metadataValue, startTime FROM fpSession s JOIN fpSessionMetadata m ON s.sessionID = m.sessionID WHERE state = 2 AND workgroupID = ? " +
			"ORDER BY startTime DESC) a JOIN fpSessionMetadata m on a.sessionID = m.sessionID JOIN fpAgentSession ag ON ag.sessionID = a.sessionID WHERE a.metadataName LIKE ? AND a.metadataValue LIKE ? AND FROM_UNIXTIME(a.startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) LIMIT ? OFFSET ?) " +
			"a on s.sessionID = a.sessionID LEFT JOIN fpSessionMetadata m ON a.sessionID = m.sessionID LEFT JOIN fpAgentSession ag ON ag.sessionID = a.sessionID WHERE (m.metadataName LIKE 'tag#%' " +
			"OR m.metadataName IN ('Division', 'RealUserID', 'TransportType', 'username')) ORDER BY startTime DESC, a.sessionID, agentJID, metadataName ASC";
	
	private static String COUNT_TOTAL_LOG = "SELECT COUNT(*) total FROM (SELECT DISTINCT a.sessionID FROM (SELECT a.sessionID as sessionID, 'agentName' as metadataName, agentJID as metadataValue, " +
			"startTime FROM fpSession s JOIN fpAgentSession a ON s.sessionID = a.sessionID WHERE state = 2 AND workgroupID = ? UNION SELECT m.sessionID as sessionID, metadataName, metadataValue, " +
			"startTime FROM fpSession s JOIN fpSessionMetadata m ON s.sessionID = m.sessionID WHERE state = 2 AND workgroupID = ?) a JOIN fpSessionMetadata m on a.sessionID = m.sessionID JOIN fpAgentSession ag ON ag.sessionID = a.sessionID WHERE a.metadataName LIKE ? AND " +
			"a.metadataValue LIKE ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)) session";
	
	private static String UNANSWERED_LOG_FILTER = "SELECT DISTINCT a.sessionID, startTime, endTime, '-' agentJID, metadataName, metadataValue FROM fpSession s JOIN (SELECT DISTINCT a.sessionID FROM (SELECT m.sessionID as sessionID, metadataName, metadataValue, startTime " +
			"FROM fpSession s JOIN fpSessionMetadata m ON s.sessionID = m.sessionID WHERE state = 1 AND workgroupID = ? ORDER BY startTime DESC) a JOIN fpSessionMetadata m on a.sessionID = m.sessionID WHERE m.metadataName LIKE ? AND m.metadataValue LIKE ? AND " +
			"FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) LIMIT ? OFFSET ?) a on s.sessionID = a.sessionID LEFT JOIN fpSessionMetadata m ON a.sessionID = m.sessionID WHERE (m.metadataName LIKE 'tag#%' OR m.metadataName IN ('Division', " +
			"'RealUserID', 'TransportType', 'username')) ORDER BY startTime DESC, a.sessionID, metadataName ASC";
	
	private static String UNANSWERED_COUNT_TOTAL_LOG = "SELECT COUNT(*) total FROM (SELECT DISTINCT a.sessionID FROM (SELECT m.sessionID as sessionID, metadataName, metadataValue, startTime FROM fpSession s JOIN fpSessionMetadata m ON s.sessionID = m.sessionID WHERE state = 1 " +
			"AND workgroupID = ? ORDER BY startTime DESC) a JOIN fpSessionMetadata m on a.sessionID = m.sessionID WHERE a.metadataName LIKE ? AND a.metadataValue LIKE ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME)) session";
	// */
	
	/* LOG QUERY VERSION 2 */
	private static String LOG_FILTER2 = "SELECT a.sessionID, startTime, endTime, agentJID, metadataName, metadataValue FROM (SELECT sessionID, startTime, endTime " +
			"FROM fpSession s WHERE state = 2 AND workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND " +
			"CAST(? AS DATETIME) AND EXISTS (SELECT f.sessionID FROM fpSessionMetadata f JOIN fpAgentSession a on f.sessionID = a.sessionID " +
			"WHERE f.sessionID = s.sessionID AND (metadataName LIKE ? AND metadataValue LIKE ?) AND agentJID LIKE ?) ORDER BY startTime DESC " +
			"LIMIT ? OFFSET ?) a LEFT JOIN fpSessionMetadata m ON m.sessionID = a.sessionID LEFT JOIN fpAgentSession ag ON ag.sessionID = " +
			"a.sessionID WHERE (m.metadataName LIKE 'tag#%' OR m.metadataName IN ('Division', 'RealUserID', 'TransportType', 'username'))";
	
	private static String COUNT_TOTAL_LOG2 = "SELECT count(sessionID) FROM fpSession s WHERE state = 2 AND workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN " +
			"CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND EXISTS (SELECT f.sessionID FROM fpSessionMetadata f JOIN fpAgentSession a on f.sessionID = a.sessionID WHERE " +
			"f.sessionID = s.sessionID AND (metadataName LIKE ? AND metadataValue LIKE ?) AND agentJID LIKE ?)";
	
	private static String UNANSWERED_LOG_FILTER2 = "SELECT a.sessionID, startTime, endTime, '-' agentJID, metadataName, metadataValue FROM (SELECT sessionID, startTime, " +
			"endTime FROM fpSession s WHERE state = 1 AND workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) ORDER BY " +
			"startTime DESC LIMIT ? OFFSET ?) a LEFT JOIN fpSessionMetadata m ON m.sessionID = a.sessionID WHERE m.metadataName IN ('Division', 'RealUserID', 'TransportType', 'username')";
	
	private static String UNANSWERED_COUNT_TOTAL_LOG2 = "SELECT count(sessionID) FROM fpSession s WHERE state = 1 AND workgroupID = ? AND FROM_UNIXTIME(startTime / 1000) BETWEEN " +
			"CAST(? AS DATETIME) AND CAST(? AS DATETIME)";
	
	private static String GET_TRANSCRIPT_PER_DAY = "SELECT transcript, notes, startTime, endTime FROM fpSession WHERE workgroupID = ? " +
			"AND FROM_UNIXTIME(startTime / 1000) BETWEEN CAST(? AS DATETIME) AND CAST(? AS DATETIME) AND transcript IS NOT NULL ORDER BY startTime ASC;";
	// */
	
	private static String LIST_METADATA = "SELECT DISTINCT s.sessionID, agentJID, metadataName, metadataValue FROM fpSession s	JOIN fpSessionMetadata m ON s.sessionID = m.sessionID " +
			"JOIN fpAgentSession ag ON ag.sessionID = s.sessionID WHERE s.sessionID = ? AND workgroupID = ? AND (m.metadataName LIKE 'tag#%' OR m.metadataName NOT IN ('channel', 'userID'))" +
			" ORDER BY agentJID, metadataName ASC";
	
	private static String DELETE_METADATA = "DELETE FROM fpSessionMetadata WHERE metadataName = ?";
	
	private static String GET_TRANSCRIPT = "SELECT sessionID, startTime, endTime, transcript, notes FROM fpSession WHERE sessionID = ? AND workgroupID = ?";
	
	public static final Map<String, String> DEFAULT_KEYS = new HashMap<String, String>();
	static {
		DEFAULT_KEYS.put("Tags", "tag#*");
		DEFAULT_KEYS.put("Agent Name", "agentName");
	}
	private static WorkgroupTranscriptManager instance;
	
	public static WorkgroupTranscriptManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupTranscriptManager();
		}
		
		return instance;
	}
	
	/**
	 * Fetch all possible transcript filter key in a workgroup
	 * 
	 * @param wgName
	 * @param wgId
	 * @return
	 */
	public Map<String, String> fetchFilterKey(String wgName, long wgId) {
		Map<String, String> buffer = new TreeMap<String, String>(DEFAULT_KEYS);
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(GET_FILTER_KEY);
			
			statement.setLong(1, wgId);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				buffer.put(result.getString(1), result.getString(1));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public int countFilteredLog(String wgName, long wgId, String filter, String value, String start, String end) {
		int counter = 0;
		
		// Need to replace '*' (asterix) with '%' to enable wildcard query
		value = value.replace('*', '%');
		filter = filter.replace('*', '%');
		String agentJid = "%";
		
		// Check whether the filter = RealUserID and replace @ with \\40
		if (filter.equalsIgnoreCase("RealUserID")) {
			value = value.replace("@", "\\40");
		} else if (filter.equalsIgnoreCase("agentName")) {
			
			/// If filter is agentName, use agentJID
			agentJid = value;
			
			filter = "%";
			value = "%";
		}
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(COUNT_TOTAL_LOG2);
			
			statement.setLong(1, wgId);
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, end + " " + MAX_HOURS);
			statement.setString(4, filter);
			statement.setString(5, value);
			statement.setString(6, agentJid);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				counter = result.getInt(1);
			}
			
			result.close();
		} catch (SQLException e) {
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return counter;
	}
	
	public ArrayList<WorkgroupTranscriptDTO> processTranscriptResultSet(ResultSet result) throws SQLException {
		ArrayList<WorkgroupTranscriptDTO> buffer = new ArrayList<WorkgroupTranscriptDTO>();
		
		String prevSessionId = "";
		String prevAgentName = "";
		WorkgroupTranscriptDTO transcript = null;
		while (result.next()) {
			String sessionId = result.getString("sessionID");
			
			// agentName is plain name, without workgroup name suffix and domain
			String agentName = "";
			try {
				agentName = result.getString("agentJID").split("-")[0];
			} catch (Exception e) {
				agentName = "";
			}
			
			if (!prevSessionId.equals(sessionId)) {
				prevSessionId = sessionId;
				prevAgentName = "";
				
				transcript = new WorkgroupTranscriptDTO(sessionId);
				transcript.setStartTime(result.getLong("startTime"));
				transcript.setEndTime(result.getLong("endTime"));
				
				buffer.add(transcript);
			}
			
			if (!prevAgentName.equals(agentName)) {
				prevAgentName = agentName;
				transcript.addAgent(agentName);
			}
			
			// Add the metadata here
			// Replace \40 with @, \40 can't be decoded by json.decode
			String metadataValue = result.getString("metadataValue");
			transcript.addMetadata(result.getString("metadataName"), metadataValue);
		}
		
		return buffer;
	}
	
	public ArrayList<WorkgroupTranscriptDTO> fetchTranscriptList(String wgName, long wgId, String filter, String value, String start, String end, int limit, int offset) {
		ArrayList<WorkgroupTranscriptDTO> buffer = new ArrayList<WorkgroupTranscriptDTO>();
		
		// Need to replace '*' (asterix) with '%' to enable wildcard query
		value = value.replace('*', '%');
		filter = filter.replace('*', '%');
		String agentJid = "%";
		
		// Check whether the filter = RealUserID and replace @ with \\40
		if (filter.equalsIgnoreCase("RealUserID")) {
			value = value.replace("@", "\\40");
		} else if (filter.equalsIgnoreCase("agentName")) {
			
			/// If filter is agentName, use agentJID
			agentJid = value;
			
			filter = "%";
			value = "%";
		}
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(LOG_FILTER2);
			
			statement.setLong(1, wgId);
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, end + " " + MAX_HOURS);
			statement.setString(4, filter);
			statement.setString(5, value);
			statement.setString(6, agentJid);
			
			statement.setInt(7, limit);
			statement.setInt(8, offset);
			
			ResultSet result = statement.executeQuery();
			
			buffer = processTranscriptResultSet(result);
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public int countUnansweredLog(String wgName, long wgId, String start, String end) {
		int counter = 0;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(UNANSWERED_COUNT_TOTAL_LOG2);
			
			statement.setLong(1, wgId);
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, end + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				counter = result.getInt(1);
			}
			
			result.close();
		} catch (SQLException e) {
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return counter;
	}
	
	public ArrayList<WorkgroupTranscriptDTO> fetchUnsansweredTranscriptList(String wgName, long wgId, String start, String end, int limit, int offset) {
		ArrayList<WorkgroupTranscriptDTO> buffer = new ArrayList<WorkgroupTranscriptDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(UNANSWERED_LOG_FILTER2);
			
			statement.setLong(1, wgId);
			statement.setString(2, start + " " + MIN_HOURS);
			statement.setString(3, end + " " + MAX_HOURS);
			statement.setInt(4, limit);
			statement.setInt(5, offset);
			
			ResultSet result = statement.executeQuery();
			
			buffer = processTranscriptResultSet(result);
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public List<WorkgroupTranscriptDetailDTO> fetchDailyTranscripts(String wgName, long wgId, String date) {
		List<WorkgroupTranscriptDetailDTO> buffer = new ArrayList<WorkgroupTranscriptDetailDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(GET_TRANSCRIPT_PER_DAY);
			
			statement.setLong(1, wgId);
			statement.setString(2, date + " " + MIN_HOURS);
			statement.setString(3, date + " " + MAX_HOURS);
			
			ResultSet result = statement.executeQuery();
			
			WorkgroupTranscriptDetailDTO transcript = null;
			while (result.next()) {
				transcript = new WorkgroupTranscriptDetailDTO("");
				transcript.setBody(result.getString("transcript"));
				transcript.setNotes(result.getString("notes"));
				transcript.setStartTime(result.getLong("startTime"));
				transcript.setEndTime(result.getLong("endTime"));
				
				buffer.add(transcript);
			}
		} catch (Exception e) {
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public WorkgroupTranscriptDTO fetchTranscriptDetail(String wgName, long wgId, String sessionId) {
		WorkgroupTranscriptDTO transcript = null;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(LIST_METADATA);
			
			statement.setString(1, sessionId);
			statement.setLong(2, wgId);
			statement.execute();
			
			ResultSet result = statement.executeQuery();
			
			String prevAgentName = "";
			while (result.next()) {
				// agentName is plain name, without workgroup name suffix and domain
				String agentName = result.getString("agentJID").split("-")[0];
				
				if (transcript == null) {
					transcript = new WorkgroupTranscriptDTO(sessionId);
				}
				
				if (!prevAgentName.equals(agentName)) {
					prevAgentName = agentName;
					transcript.addAgent(agentName);
				}
				
				// Add the metadata here
				transcript.addMetadata(result.getString("metadataName"), result.getString("metadataValue"));
			}
			result.close();
			
			statement = con.prepareStatement(GET_TRANSCRIPT);
			
			statement.setString(1, sessionId);
			statement.setLong(2, wgId);
			statement.execute();
			
			result = statement.executeQuery();
			while (result.next()) {
				transcript.setBody(result.getString("transcript"));
				transcript.setNotes(result.getString("notes"));
				transcript.setStartTime(result.getLong("startTime"));
				transcript.setEndTime(result.getLong("endTime"));
			}
			result.close();
			
		} catch (Exception e) {
			// e.printStackTrace();
			transcript = null;
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return transcript;
	}
	
	public void deleteMetadata(String workgroupName, String metadataName) throws SQLException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection(workgroupName);
            pstmt = con.prepareStatement(DELETE_METADATA);
            pstmt.setString(1, metadataName);
            
            pstmt.executeUpdate();
        }
        catch (SQLException sqle) {
            throw sqle;
        }
        finally {
        	FastpathDBManager.closeConnection(pstmt, con);
        }
    }
}
