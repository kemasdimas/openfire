package com.kemasdimas.workgroupservice.component;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.http.HttpException;
import org.jivesoftware.openfire.fastpath.macros.Macro;
import org.jivesoftware.openfire.fastpath.macros.MacroGroup;
import org.jivesoftware.openfire.fastpath.macros.WorkgroupMacros;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.xmpp.packet.JID;

import com.kemasdimas.workgroupservice.dto.WorkgroupAutomessageTypeDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupTranscriptDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupTranscriptDetailDTO;
import com.kemasdimas.workgroupservice.manager.WorkgroupReportManager;
import com.kemasdimas.workgroupservice.manager.WorkgroupTranscriptManager;

public final class TranscriptServiceModule extends BaseWorkgroupServiceModule {
		
	protected static TranscriptServiceModule instance;
	private WorkgroupTranscriptManager transcriptManager;
	
	protected TranscriptServiceModule() {
		super();
		
		transcriptManager = WorkgroupTranscriptManager.getInstance();
	}
	
	public static TranscriptServiceModule getInstance() {
		if (instance == null) {
			instance = new TranscriptServiceModule();
		}
		
		return (TranscriptServiceModule) instance;
	}
	
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		// This is used to validate workgroup name!
		Workgroup wg = findWorkgroup(wgName);
		
		if ("list-transcript-filter".equals(type)) {
			return listTranscriptFilter(wg);
		} else if ("list-transcript".equals(type)) {
			// Transcript list is limited by date range
			String start = request.getParameter("start");
			String end = request.getParameter("end");
			if (isValidDate(start, end)) {
				String limit = request.getParameter("limit");
				String offset = request.getParameter("offset");
				String filter = request.getParameter("filter");
				String value = request.getParameter("value");
							
				if (filter == null || value == null || limit == null || offset == null) {
					throw new Exception("filter / value / limit / offset can not be null");
				}
				
				int limitInt = Integer.parseInt(limit);
				int offsetInt = Integer.parseInt(offset);
				
				return listTranscript(wg, filter, value, start, end, limitInt, offsetInt);
			}
		} else if ("list-unanswered-transcript".equals(type)) {
			String start = request.getParameter("start");
			String end = request.getParameter("end");
			if (isValidDate(start, end)) {
				String limit = request.getParameter("limit");
				String offset = request.getParameter("offset");
							
				if (limit == null || offset == null) {
					throw new Exception("limit / offset can not be null");
				}
				
				int limitInt = Integer.parseInt(limit);
				int offsetInt = Integer.parseInt(offset);
				
				return listUnansweredTranscript(wg, start, end, limitInt, offsetInt);
			}
		} else if ("get-transcript".equals(type)) {
			String sessionId = request.getParameter("sessionId");
			if (sessionId == null) {
				throw new Exception("sessionId can not be null");
			}
			
			return getTranscriptDetail(wg, sessionId);
		} else if ("daily-transcripts".equals(type)) {
			String date = request.getParameter("date");
			if (date == null) {
				throw new Exception("date can not be null");
			}
			
			return getDailyTranscript(wg, date);
		}
		
		throw new Exception("Type not supported");
	}
	
	private String listTranscriptFilter(Workgroup wg) {
		Map<String, String> result = transcriptManager.fetchFilterKey(wg.getWorkgroupName(), wg.getID());
		
		JSONArray jsonArray = new JSONArray();
		for (String key : result.keySet()) {            
            JSONObject json = new JSONObject();
            json.put("key", key);
            json.put("value", result.get(key));
            
            jsonArray.add(json);
        }
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", jsonArray.size());
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	private String listTranscript(Workgroup wg, String filter, String value, String start, String end, int limit, int offset) {
		// Special condition for query by agent, append the workgroupname to it
		if (filter.equals(transcriptManager.DEFAULT_KEYS.get("Agent Name"))) {
			value += "-" + wg.getWorkgroupName() + "%";
		}
		
		// Get the total Count
		int totalCount = transcriptManager.countFilteredLog(wg.getWorkgroupName(), wg.getID(), filter, value, start, end);
		ArrayList<WorkgroupTranscriptDTO> result = transcriptManager.
				fetchTranscriptList(wg.getWorkgroupName(), wg.getID(), filter, value, start, end, limit, offset);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", totalCount);
		finalJson.put("data", result);
		
		return finalJson.toString();
	}
	
	private String listUnansweredTranscript(Workgroup wg, String start, String end, int limit, int offset) {
		// Get the total Count
		int totalCount = transcriptManager.countUnansweredLog(wg.getWorkgroupName(), wg.getID(), start, end);
		ArrayList<WorkgroupTranscriptDTO> result = transcriptManager.
				fetchUnsansweredTranscriptList(wg.getWorkgroupName(), wg.getID(), start, end, limit, offset);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", totalCount);
		finalJson.put("data", result);
		
		return finalJson.toString();
	}
	
	private String getDailyTranscript (Workgroup wg, String date) throws HttpException {
		List<WorkgroupTranscriptDetailDTO> transcripts = transcriptManager.fetchDailyTranscripts(wg.getWorkgroupName(), wg.getID(), date);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", transcripts.size());
		finalJson.put("data", transcripts);
		
		return finalJson.toString();
	}
	
	private String getTranscriptDetail(Workgroup wg, String sessionId) throws HttpException {
		WorkgroupTranscriptDTO transcript = transcriptManager.fetchTranscriptDetail(wg.getWorkgroupName(), wg.getID(), sessionId);
		
		if (transcript == null) {
			throw new HttpException(404, "Transcript for " + sessionId + " can not be found.");
		}
		
		JSONObject json = new JSONObject();
		JSONArray agents = new JSONArray();
		JSONArray metadata = new JSONArray();
		
		json.put("sessionId", transcript.getSessionID());
		json.put("start", transcript.getStartTime());
		json.put("end", transcript.getEndTime());
		json.put("body", transcript.getBody());
		json.put("notes", transcript.getNotes());
		
		JSONObject reuse = null;
		// Iterate agent
		for (String agent : transcript.getAgentList()) {
			reuse = new JSONObject();
			reuse.put("name", agent);
			
			agents.add(reuse);
		}
		
		// Iterate metadata
		for (String key : transcript.getMetadata().keySet()) {
			reuse = new JSONObject();
			reuse.put("key", key);
			reuse.put("value", transcript.getMetadata().get(key));
			
			metadata.add(reuse);
		}
		
		json.put("agents", agents);
		json.put("metadata", metadata);
		
		return json.toString();
	}
	
	@Override
	public void close() {
		super.close();
		
		transcriptManager = null;
	}
	
}
