package com.kemasdimas.workgroupservice.dto;

public class WorkgroupSMSCompositeDTO extends WorkgroupSMSDto {
	
	WorkgroupSMSDto replySms = null;
	
	public WorkgroupSMSCompositeDTO() {
		
	}

	public WorkgroupSMSDto getReplySms() {
		return replySms;
	}

	public void setReplySms(WorkgroupSMSDto replySms) {
		this.replySms = replySms;
	}
	
	@Override
	public String toJSONString() {
		String processedString = super.toJSONString();
		
		if (replySms != null) {
			processedString = processedString.substring(0, processedString.length() - 1);
			
			StringBuffer buffer = new StringBuffer(processedString);
			buffer.append(",\"replySMS\":" + replySms.toJSONString() + "}");
			
			processedString = buffer.toString();
		}
		
		return processedString;
	}
}
