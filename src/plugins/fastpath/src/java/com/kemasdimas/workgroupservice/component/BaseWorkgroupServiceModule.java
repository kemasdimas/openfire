package com.kemasdimas.workgroupservice.component;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.http.HttpException;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupManager;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.JID;

public class BaseWorkgroupServiceModule {

	protected WorkgroupManager wgManager;
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	
	protected BaseWorkgroupServiceModule() {
		wgManager = WorkgroupManager.getInstance();
	}
	
	public String executeCommand(HttpServletRequest request) throws Exception {
		return "";
	}
	
	protected JID prepJID(String wgName, String agentName) {
		return new JID(agentName + "-" + wgName, 
				ComponentManagerFactory.getComponentManager().getServerName(), "");
	}
	
	protected Workgroup findWorkgroup(String wgName) throws Exception {
		Workgroup wg = wgManager.getWorkgroup(wgName);
		
		if (wg == null) {
			throw new HttpException(404, "Workgroup not found");
		} else {
			return wg;
		}
	}
	
	protected boolean isValidDate(String start, String end) throws Exception {
		if (start == null || end == null) {
			throw new Exception("start / end can not be null");
		}
		
		Date startDate = dateFormatter.parse(start);
		Date endDate = dateFormatter.parse(end);
		
		if (startDate.after(endDate)) {
			throw new Exception("start must be less than end");
		}
		
		return true;
	}
	
	public void close() {
		wgManager = null;
	}
	
}
