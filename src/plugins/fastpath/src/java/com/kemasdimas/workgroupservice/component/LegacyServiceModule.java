package com.kemasdimas.workgroupservice.component;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.kraken.BaseTransport;
import net.sf.kraken.KrakenPlugin;
import net.sf.kraken.TransportInstance;
import net.sf.kraken.registration.Registration;
import net.sf.kraken.registration.RegistrationManager;
import net.sf.kraken.session.TransportSession;
import net.sf.kraken.type.TransportLoginStatus;
import net.sf.kraken.web.ConfigManager;

import org.eclipse.jetty.http.HttpException;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.session.ComponentSession;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.JID;

import com.kemasdimas.workgroupservice.dto.WorkgroupTransportDTO;
import com.kemasdimas.workgroupservice.manager.WorkgroupTransportManager;

public final class LegacyServiceModule extends BaseWorkgroupServiceModule {

	private static final Logger Log = LoggerFactory.getLogger(LegacyServiceModule.class);
	
	protected static LegacyServiceModule instance;
	private UserManager userManager;
	private ConfigManager configManager;
	private WorkgroupTransportManager transportManager;
	
	protected LegacyServiceModule() {
		userManager = UserManager.getInstance();
		configManager = new ConfigManager();
		transportManager = WorkgroupTransportManager.getInstance();
	}
	
	public static LegacyServiceModule getInstance() {
		if (instance == null) {
			instance = new LegacyServiceModule();
		}
		
		return (LegacyServiceModule) instance;
	}
	
	@Override
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		// This is used to validate workgroup name!
		Workgroup wg = findWorkgroup(wgName);
		if ("create-transport".equals(type)) {
			String transportUserName = request.getParameter("transportName");
			String transportType = request.getParameter("transportType");
			String transportPassword = request.getParameter("transportPass");
			
			if (transportUserName == null || transportType == null || transportPassword == null) {
				throw new Exception("transportName / transportType / transportPass can not be null");
			}
			
			return createTransport(wgName, wg.getID(), transportUserName, transportType, transportPassword);
		} else if ("edit-transport".equals(type)) {
			String handlerName = request.getParameter("handlerName");
			String transportUserName = request.getParameter("transportName");
			String transportPassword = request.getParameter("transportPass");
			
			if (transportUserName == null || handlerName == null) {
				throw new Exception("transportName / handlerName can not be null");
			}
			
			return editTransport(wgName, wg.getID(), handlerName, transportUserName, transportPassword);
		} else if ("delete-transport".equals(type)) {
			String handler = request.getParameter("transportHandler");
			
			if (handler == null) {
				throw new Exception("Transport handler can not be null");
			}
			
			return deleteTransport(wg.getID(), handler, wgName);
		} else if ("list-transport".equals(type)) {
			String limit = request.getParameter("limit");
			String offset = request.getParameter("offset");
			
			if (limit == null || offset == null) {
				throw new Exception("Limit / offset can not be null");
			}
			
			int limitInt = Integer.parseInt(limit);
			int offsetInt = Integer.parseInt(offset);
			
			return listTransport(wg.getID(), limitInt, offsetInt, wgName);
		}
		
		throw new Exception("Type not supported");
	}
	
	/**
	 * List add registered transport in a workgroup
	 * 
	 * @param wgId
	 * @param limit
	 * @param offset
	 * @return
	 * @throws Exception
	 */
	private String listTransport(long wgId, int limit, int offset, String workgroupName) throws Exception {
		String serverName = ComponentManagerFactory.getComponentManager().getServerName();
		
		List<WorkgroupTransportDTO> transports = transportManager.fetchTransportInWorkgroup(wgId, limit, offset);
		StringBuffer buffer = new StringBuffer();
		
		// Use the kraken plugin to check online status
		final KrakenPlugin plugin =
	            (KrakenPlugin) XMPPServer.getInstance().getPluginManager().getPlugin("kraken");
		
		buffer.append("{\"total\": " + transportManager.countTransportAgent(wgId));
		buffer.append(", \"data\": [");
		for (int i = 0; i < transports.size(); i++) {
			WorkgroupTransportDTO transport = transports.get(i);
			boolean isLoggedIn = false;
			
			try {
				TransportInstance transportInstance = plugin.getTransportInstance(transport.getTransportType());
				BaseTransport baseTransport = transportInstance.getTransport();
				TransportSession sess = baseTransport.getSessionManager().getSession(transport.getUserId());
				
				isLoggedIn = sess.isLoggedIn() || sess.getLoginStatus() == TransportLoginStatus.LOGGING_IN;
			} catch (Exception e) {
			}
			
			buffer.append('{');
			buffer.append("\"userId\": \"" + transport.getUserId() + "\"");
			buffer.append(", \"transportType\": \"" + transport.getTransportType() + "\"");
			buffer.append(", \"transportUsername\": \"" + transport.getTransportUserName() + "\"");
			buffer.append(", \"isOnline\": " + isLoggedIn);
			buffer.append('}');
			buffer.append(',');
		}
		
		if (transports.size() > 0)
			buffer.setCharAt(buffer.length() - 1, ']');
		else
			buffer.append("]");
		
		buffer.append('}');
		
		return buffer.toString();
	}
	
	/**
	 * Delete a registered transport in a workgroup
	 * (implemented by deleting the user with certain JID)
	 * 
	 * @param wgId
	 * @param transportHandler
	 * @return
	 * @throws Exception
	 */
	private String deleteTransport(long wgId, String transportHandler, String workgroupName) throws Exception {
		if (transportManager.isTransportExists(transportHandler, wgId, workgroupName)) {
			String serverName = ComponentManagerFactory.getComponentManager().getServerName();
			
			// TODO: WRONG STEP! Seharusnya delete registration dari RegistrationManager dulu
			final User user = userManager.getUser(transportHandler);
			
			Collection<Registration> regs = RegistrationManager.getInstance().getRegistrations(new JID(transportHandler + "@" + serverName));
			if (regs.size() > 0) {
				Registration reg = regs.iterator().next();
				
				RegistrationManager.getInstance().deleteRegistration(reg);
			}
			
			// Delete the transport from wgLegacyAgent
			transportManager.deleteTransport(transportHandler, wgId, workgroupName);
			
			// User deletion must be inside a thread, this is a tedious task!
			new Thread(new Runnable() {
				@Override
				public void run() {
					userManager.deleteUser(user);
				}
			}).start();
		} else {
			throw new Exception("Destinated transport not exist");
		}
		
		
		return "\"ok\"";
	}
	
	/**
	 * Register a transport in a workgroup
	 * 
	 * @param wgName
	 * @param wgId
	 * @param transportName
	 * @param transportType
	 * @param transportPass
	 * @return
	 * @throws Exception
	 */
	private String createTransport(String wgName, long wgId, String transportName, String transportType, String transportPass) throws Exception {
		long creationDate = Calendar.getInstance().getTimeInMillis();
		JID agentJID = prepJID(wgName, transportType + creationDate);
		User user = null;
		
		try {
			user = userManager.createUser(agentJID.getNode(), "" + creationDate, transportName, null);
			String rv = configManager.addRegistration(agentJID.getNode(), transportType, transportName, transportPass, "");
			
			if (rv != null) {
				throw new Exception(rv);
			}
			
			// Add the transport to wgLegacyAgent
			transportManager.addTransport(agentJID.getNode(), wgId, wgName);
			
		} catch (Exception e) {
			if (user != null) {
				userManager.deleteUser(user);
			}
			
			throw e;
		}
		
		
		return "\"ok\"";
	}
	
	
	public String editTransport(String wgName, long wgId, String handlerName, String transportName, String transportPass) throws UserNotFoundException, HttpException {
		JID agentJID = prepJID(wgName, handlerName.split("-")[0]);
		User user = userManager.getUser(agentJID.toBareJID());
		
		Registration registration = RegistrationManager.getInstance().getRegistrations(agentJID).iterator().next();
		if (registration != null) {
			if (transportPass == null) {
				transportPass = registration.getPassword();
			}
			
			Integer regId = (int) registration.getRegistrationID();
			configManager.updateRegistration(regId, transportName, transportPass, "");
			user.setName(transportName);
			
			String compJid = registration.getTransportType().toString() + "." + ComponentManagerFactory.getComponentManager().getServerName();
			ComponentSession compSession = XMPPServer.getInstance().getSessionManager().
					getComponentSession(compJid);
			
			if (compSession != null) {
				// If the transport has an external component (spectrum), change the spectrum users data in database
				try {
					transportManager.editSpectrumTransport(agentJID.toBareJID(), transportName, transportPass);
				} catch (SQLException e) {
					Log.error("Edit spectrum database error.", e);
				}
			}
		} else {
			throw new HttpException(404, "Registration not found");
		}
		
		return "\"ok\""; 
	}
	
	@Override
	public void close() {
		userManager = null;
		configManager = null;
		transportManager = null;
		
		super.close();
	}
}
