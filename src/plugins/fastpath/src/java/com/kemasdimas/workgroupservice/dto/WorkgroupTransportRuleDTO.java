package com.kemasdimas.workgroupservice.dto;

public final class WorkgroupTransportRuleDTO {
	private long workgroupID;
	private long queueID;
	private int rulePosition;
	private String username;
	private String transportType;
	
	public WorkgroupTransportRuleDTO(long workgroupID, long queueID,
			int rulePosition, String username, String transportType) {
		
		super();
		this.workgroupID = workgroupID;
		this.queueID = queueID;
		this.rulePosition = rulePosition;
		this.username = username;
		this.transportType = transportType;
	}

	public long getWorkgroupID() {
		return workgroupID;
	}
	
	public void setWorkgroupID(long workgroupID) {
		this.workgroupID = workgroupID;
	}
	
	public long getQueueID() {
		return queueID;
	}
	
	public void setQueueID(long queueID) {
		this.queueID = queueID;
	}
	
	public int getRulePosition() {
		return rulePosition;
	}
	
	public void setRulePosition(int rulePosition) {
		this.rulePosition = rulePosition;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getTransportType() {
		return transportType;
	}
	
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}
	
}
