package com.kemasdimas.workgroupservice.dto;

public final class WorkgroupAutomessageTypeDTO {
	private String name;
	private String label;
	
	public WorkgroupAutomessageTypeDTO(String name, String label) {
		super();
		this.name = name;
		this.label = label;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return "Name: " + name + ", label: " + label;
	}
}
