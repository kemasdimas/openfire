package com.kemasdimas.workgroupservice.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;

import com.kemasdimas.workgroupservice.dto.WorkgroupReportItemDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupSMSCompositeDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupSMSDto;


/**
 * @author kemas
 */
public final class WorkgroupSmsManager {
	private static WorkgroupSmsManager instance;
	
	/**
	 * Field detail:
	 * 
	 * sent:
	 * -1	= it is incoming message from phone to server
	 * 0	= SMS in process of sending
	 * 1	= phone acknowledge the SMS sending operation
	 * 
	 * read:
	 * -1 	= it is outgoing message from server to phone
	 * 0	= new incoming message, unread by anyone
	 * 1	= incoming message, read by someone
	 */
	private static final String INSERT_INCOMING_SMS = "INSERT INTO fpSmsStorage (workgroup_id, from_address, address_name, to_address, message, is_read) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String INSERT_OUTGOING_SMS = "INSERT INTO fpSmsStorage (workgroup_id, from_address, to_address, message, sent, sent_by, reply_for) VALUES (?, ?, ?, ?, ?, ?, ?)";
	
	private static final String QUERY_REPLY_FOR_ID = "SELECT id, timestamp_at, message, sent_by FROM fpSmsStorage WHERE workgroup_id = ? AND reply_for = ? ORDER BY timestamp_at DESC LIMIT 1";
	
	private static final String QUERY_SMS_REGISTERED_NUMBER = "SELECT DISTINCT to_address FROM fpSmsStorage WHERE workgroup_id = ? AND sent = -1";
	private static final String QUERY_SMS_BY_CUSTOMER_NUMBER = "SELECT * FROM fpSmsStorage WHERE workgroup_id = ? AND ((to_address = ? AND from_address = ?) OR (from_address = ? AND to_address = ?)) ORDER BY timestamp_at ASC";
	
	private static final String QUERY_COUNT_SMS_BY_REGISTERED_NUMBER = "SELECT count(*) total FROM (SELECT * FROM ( SELECT from_address, timestamp_at FROM fpSmsStorage WHERE workgroup_id = ? AND sent = -1 AND to_address = ? " +
			"ORDER BY timestamp_at DESC) allsms GROUP BY from_address ORDER BY timestamp_at DESC) i ";
	private static final String QUERY_SMS_BY_REGISTERED_NUMBER = "SELECT i.*, s.sent, s.sent_by, s.message reply_message, s.timestamp_at reply_timestamp FROM (SELECT * FROM (SELECT id, from_address, address_name, to_address, " +
			"message, timestamp_at, is_read FROM fpSmsStorage WHERE workgroup_id = ? AND sent = -1 AND to_address = ? ORDER BY timestamp_at DESC) allsms GROUP BY from_address ORDER BY timestamp_at DESC) i " +
			"LEFT JOIN fpSmsStorage s on s.reply_for = i.id LIMIT ? OFFSET ?";
	
	private static final String QUERY_UPDATE_IS_SENT = "UPDATE fpSmsStorage SET sent = ? WHERE id = ? AND is_read = -1 AND workgroup_id = ?";
	private static final String QUERY_UPDATE_IS_READ = "UPDATE fpSmsStorage SET is_read = ? WHERE id IN(#) AND sent = -1 AND workgroup_id = ?";
	private static final String QUERY_UPDATE_ASSIGN_TO = "UPDATE fpSmsStorage SET assign_to = ? WHERE id = ? AND workgroup_id = ?";
	
	public static WorkgroupSmsManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupSmsManager();
		}
		
		return instance;
	}
	
	/**
	 * Get SMS reply for certain incoming SMS id, very useful to determine
	 * whether an SMS has been replied or not
	 * 
	 * @param workgroup
	 * @param incomingSmsId
	 * @return
	 */
	public WorkgroupSMSDto getReplyForId(final Workgroup workgroup, long incomingSmsId) {
		WorkgroupSMSDto replySms = null;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
			statement = con.prepareStatement(QUERY_REPLY_FOR_ID);
			
			statement.setLong(1, workgroup.getID());
			statement.setLong(2, incomingSmsId);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				replySms = new WorkgroupSMSDto();
				
				replySms.setId(result.getLong(1));
				replySms.setTimestamp(result.getTimestamp(2));
				replySms.setMessage(result.getString(3));
				replySms.setSentBy(result.getString(4));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return replySms;
	}
	
	/**
	 * Query registered SMS number that already have an SMS
	 * 
	 * @param workgroup
	 * @return
	 */
	public List<String> getRegisteredSmsNumbers(final Workgroup workgroup) {
		List<String> numbersBuffer = new ArrayList<String>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
			statement = con.prepareStatement(QUERY_SMS_REGISTERED_NUMBER);
			
			statement.setLong(1, workgroup.getID());
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				numbersBuffer.add(result.getString(1));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return numbersBuffer;
	}
	
	/**
	 * Query sms for certain customer number in a workgroup
	 * 
	 * @param workgroup
	 * @param registeredNumber
	 * @param customerNumber
	 * @return
	 */
	public List<WorkgroupSMSDto> getSmsByCustomerNumber(final Workgroup workgroup, String registeredNumber, String customerNumber) {
		List<WorkgroupSMSDto> smsesBuffer = new ArrayList<WorkgroupSMSDto>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
			statement = con.prepareStatement(QUERY_SMS_BY_CUSTOMER_NUMBER);
			
			statement.setLong(1, workgroup.getID());
			statement.setString(2, registeredNumber);
			statement.setString(3, customerNumber);
			statement.setString(4, registeredNumber);
			statement.setString(5, customerNumber);
			
			ResultSet result = statement.executeQuery();
			
			WorkgroupSMSCompositeDTO smsHolder = null;
			while (result.next()) {
				smsHolder = new WorkgroupSMSCompositeDTO();
				
				smsHolder.setId(result.getLong(1));
				smsHolder.setFromAddress(result.getString(3));
				smsHolder.setFromName(result.getString(4));
				smsHolder.setToAddress(result.getString(5));
				smsHolder.setMessage(result.getString(6));
				smsHolder.setTimestamp(result.getTimestamp(7));
				smsHolder.setIsSent(result.getByte(8));
				smsHolder.setIsRead(result.getByte(9));
				smsHolder.setSentBy(result.getString(10));
				smsHolder.setAssignTo(result.getString(12));
				
				smsesBuffer.add(smsHolder);
			}
			smsHolder = null;
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return smsesBuffer;
	}
	
	/**
	 * Query incoming SMS by certain registered number, this function supports pagination
	 * 
	 * @param workgroup
	 * @param registeredNumber
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<WorkgroupSMSCompositeDTO> getIncomingSmsByRegisteredNumber(final Workgroup workgroup, String registeredNumber, long limit, long offset) {
		List<WorkgroupSMSCompositeDTO> smsesBuffer = new ArrayList<WorkgroupSMSCompositeDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
			statement = con.prepareStatement(QUERY_SMS_BY_REGISTERED_NUMBER);
			
			statement.setLong(1, workgroup.getID());
			statement.setString(2, registeredNumber);
			statement.setLong(3, limit);
			statement.setLong(4, offset);
			
			ResultSet result = statement.executeQuery();
			
			WorkgroupSMSCompositeDTO smsHolder = null;
			WorkgroupSMSDto replySms = null;
			while (result.next()) {
				smsHolder = new WorkgroupSMSCompositeDTO();
				
				smsHolder.setId(result.getLong(1));
				smsHolder.setFromAddress(result.getString(2));
				smsHolder.setFromName(result.getString(3));
				smsHolder.setToAddress(result.getString(4));
				smsHolder.setMessage(result.getString(5));
				smsHolder.setTimestamp(result.getTimestamp(6));
				smsHolder.setIsRead(result.getByte(7));
				
				// Only fill the SMS reply parameter if it is not null.
				String replySentBy = result.getString(9);
				if (replySentBy != null && replySentBy.trim().length() > 0) {
					replySms = new WorkgroupSMSDto();
					
					replySms.setIsSent(result.getByte(8));
					replySms.setSentBy(replySentBy);
					replySms.setMessage(result.getString(10));
					replySms.setTimestamp(result.getTimestamp(11));
					
					smsHolder.setReplySms(replySms);
				}
				
				smsesBuffer.add(smsHolder);
			}
			replySms = null;
			smsHolder = null;
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return smsesBuffer;
	}
	
	/**
	 * Query stored incoming SMS count for certain registered number,
	 * joined with its SMS reply (if exists)
	 * 
	 * @param workgroup
	 * @param registeredNumber
	 * @return
	 */
	public long countSmsForRegisteredNumber(final Workgroup workgroup, String registeredNumber) {
		long smsCount = 0;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
			statement = con.prepareStatement(QUERY_COUNT_SMS_BY_REGISTERED_NUMBER);
			
			statement.setLong(1, workgroup.getID());
			statement.setString(2, registeredNumber);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				smsCount = result.getLong(1);
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return smsCount;
	}
	
	public void updateSmsAssignTo(final Workgroup workgroup, final long smsId, final String assignTo) {
		Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
            pstmt = con.prepareStatement(QUERY_UPDATE_ASSIGN_TO);
            pstmt.setString(1, assignTo);
            pstmt.setLong(2, smsId);
            pstmt.setLong(3, workgroup.getID());
            
            pstmt.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            FastpathDBManager.closeConnection(pstmt, con);
        }
	}
	
	public void updateSmsRead(final Workgroup workgroup, final long[] smsIds, final byte isRead) {
		Connection con = null;
        PreparedStatement pstmt = null;
        try {
        	StringBuffer buffer = new StringBuffer(smsIds.length * 2);
        	int idsLength = smsIds.length;
        	for (int i = 0; i < idsLength; i++) {
				buffer.append(smsIds[i]);
				
				if (i < idsLength - 1) {
					buffer.append(",");
				}
			}
        	
        	String replaced = QUERY_UPDATE_IS_READ.replace("#", buffer.toString());
        	
            con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
            pstmt = con.prepareStatement(replaced);
            pstmt.setByte(1, isRead);
            pstmt.setLong(2, workgroup.getID());
            
            pstmt.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            FastpathDBManager.closeConnection(pstmt, con);
        }
	}
	
	public void updateSmsSent(final Workgroup workgroup, final long smsId, final byte isSent) {
		Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
            pstmt = con.prepareStatement(QUERY_UPDATE_IS_SENT);
            pstmt.setByte(1, isSent);
            pstmt.setLong(2, smsId);
            pstmt.setLong(3, workgroup.getID());
            
            pstmt.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            FastpathDBManager.closeConnection(pstmt, con);
        }
	}
	
	/**
	 * Save incoming SMS
	 * 
	 * @param workgroup
	 * @param fromAddress
	 * @param addressName
	 * @param toAddress
	 * @param message
	 */
	public void saveIncomingSms(final Workgroup workgroup, final String fromAddress, final String addressName, final String toAddress, final String message) {
		Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
            pstmt = con.prepareStatement(INSERT_INCOMING_SMS);
            pstmt.setLong(1, workgroup.getID());
            pstmt.setString(2, fromAddress);
            pstmt.setString(3, addressName);
            pstmt.setString(4, toAddress);
            pstmt.setString(5, message);
            pstmt.setInt(6, 0);
            
            pstmt.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            FastpathDBManager.closeConnection(pstmt, con);
        }
	}
	
	/**
	 * Save outgoing SMS with it relation to the replied message
	 * 
	 * @param workgroup
	 * @param fromAddress
	 * @param toAddress
	 * @param message
	 * @param sentBy
	 * @param replyFor
	 */
	public void saveOutgoingSms(final Workgroup workgroup, final String fromAddress, final String toAddress, final String message, final String sentBy, final long replyFor) {
		Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection(workgroup.getWorkgroupName());
            pstmt = con.prepareStatement(INSERT_OUTGOING_SMS);
            pstmt.setLong(1, workgroup.getID());
            pstmt.setString(2, fromAddress);
            pstmt.setString(3, toAddress);
            pstmt.setString(4, message);
            pstmt.setInt(5, 0);
            pstmt.setString(6, sentBy);
            pstmt.setLong(7, replyFor);
            
            pstmt.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            FastpathDBManager.closeConnection(pstmt, con);
        }
	}
	
}