package com.kemasdimas.workgroupservice.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jivesoftware.xmpp.workgroup.AgentSession;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.xmpp.packet.JID;

import com.kemasdimas.workgroupservice.dto.WorkgroupAgentLoginHistoryDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupReportItemDTO;
import com.kemasdimas.workgroupservice.manager.WorkgroupReportManager;
import com.kemasdimas.workgroupservice.manager.WorkgroupTranscriptManager;
import com.kemasdimas.workgroupservice.manager.WorkgroupTransportManager;

public final class ReportServiceModule extends BaseWorkgroupServiceModule {
		
	protected static ReportServiceModule instance;
	private WorkgroupReportManager reportManager;
	private WorkgroupTranscriptManager transcriptManager;
	
	private static final ArrayList<String> UNDELETABLE_REPORT = new ArrayList<String>(10);
	{
		UNDELETABLE_REPORT.add("userID");
		UNDELETABLE_REPORT.add("username");
		UNDELETABLE_REPORT.add("TransportType");
		UNDELETABLE_REPORT.add("Division");
		UNDELETABLE_REPORT.add("imUsername");
		UNDELETABLE_REPORT.add("RealUserID");
	}
	
	protected ReportServiceModule() {
		super();
		
		reportManager = WorkgroupReportManager.getInstance();
		WorkgroupTransportManager.getInstance();
		transcriptManager = WorkgroupTranscriptManager.getInstance();
	}
	
	public static ReportServiceModule getInstance() {
		if (instance == null) {
			instance = new ReportServiceModule();
		}
		
		return (ReportServiceModule) instance;
	}
	
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		// This is used to validate workgroup name!
		Workgroup wg = findWorkgroup(wgName);
		String filter = request.getParameter("filter");
		
		if ("active-chat-report".equals(type)) {
			return activeChatReport(wg);
		} else if ("list-form-report".equals(type)) {
			return listFormReport(wg);
		} else if ("delete-report".equals(type)) {
			if (filter == null) {
				throw new Exception("filter can not be null");
			}
			
			return deleteReportData(wg, filter);
		}
		
		String start = request.getParameter("start");
		String end = request.getParameter("end");
		if (isValidDate(start, end)) {
			if ("time-report".equals(type)) {
				return timeReport(wg, start, end);
			} else if ("agent-report".equals(type)) {
				if (filter != null) {
					return agentDetailReport(wg, start, end, filter);
				} else {
					return agentReport(wg, start, end);
				}
			} else if ("division-report".equals(type)) {
				if (filter != null) {
					return divisionDetailReport(wg, start, end, filter);
				} else {
					return divisionReport(wg, start, end);
				}
			} else if ("tag-report".equals(type)) {
				if (filter != null) {
					return tagDetailReport(wg, start, end, filter);
				} else {
					return tagReport(wg, start, end);
				}
			} else if ("im-report".equals(type)) {				
				if (filter != null) {
					filter = filter.replace("-", "");
					return imDetailReport(wg, start, end, filter);
				} else {
					return imReport(wg, start, end);
				}
			} else if ("form-report".equals(type)) {
				if (filter != null) {
					return freeFormReport(wg, filter, start, end);
				} else {
					throw new Exception("Free form report requires 'filter' parameter");
				}
			} else if ("login-report".equals(type)) {
				if (start.equalsIgnoreCase(end)) {
					return agentLoginHistoryReport(wg, start);
				} else {
					throw new Exception("Start / End date must be the same date");
				}
			}
		}
		
		throw new Exception("Type not supported");
	}
	
	private String prepareList(List<WorkgroupReportItemDTO> data) {
		JSONObject finalJson = new JSONObject();
		finalJson.put("data", data);
		
		return finalJson.toString();
	}
	
	private String prepareTabular(List<WorkgroupReportItemDTO> data, List<WorkgroupReportItemDTO> tabular) {
		JSONObject finalJson = new JSONObject();
		finalJson.put("data", data);
		finalJson.put("tabular", tabular);
		
		return finalJson.toString();
	}
	
	private String deleteReportData(Workgroup wg, String filter) throws Exception {
		if (UNDELETABLE_REPORT.contains(filter)) {
			throw new Exception("Report for " + filter + " can't be deleted.");
		}
		
		transcriptManager.deleteMetadata(wg.getWorkgroupName(), filter);
		
		return "\"ok\"";
	}
	
	/**
	 * Query agent login history
	 * 
	 * @param wg
	 * @return
	 */
	private String agentLoginHistoryReport(Workgroup wg, String start) {
		long totalHistories = reportManager.countLoginHistories(wg.getWorkgroupName(), start);
		List<WorkgroupAgentLoginHistoryDTO> histories = reportManager.fetchLoginHistories(wg.getWorkgroupName(), start);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", totalHistories);
		finalJson.put("data", histories);
		
		return finalJson.toString();
	}
	
	/**
	 * Query the active chat count in a workgroup including active agents
	 * 
	 * @param wg
	 * @return
	 */
	private String activeChatReport(Workgroup wg) {
		int currentChat = 0;
		
		JSONArray agentsArray = new JSONArray();
		JSONObject buffer = null;
		for (AgentSession session : wg.getAgentAvailableSessions()) {
			int chatCount = session.getChatsInfo(wg).size();
			
			buffer = new JSONObject();
			buffer.put("name", session.getUsername().split("-")[0]);
			buffer.put("presence", session.getPresence().getStatus());
			buffer.put("chatCount", chatCount);
			agentsArray.add(buffer);
			
			currentChat += chatCount;
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("current", currentChat);
		finalJson.put("online-agents", agentsArray);
		
		return finalJson.toString();
	}
	
	private String freeFormReport(Workgroup wg, String metadataName, String start, String end) {
		List<WorkgroupReportItemDTO> jsonArray = new ArrayList<WorkgroupReportItemDTO>(0);
		
		List<WorkgroupReportItemDTO> tabArray = reportManager.
				fetchCountPerMetadata(wg.getWorkgroupName(), wg.getID(), start, end, metadataName);
		
		return prepareTabular(jsonArray, tabArray);
	}
	
	/**
	 * Get queriable metadata from database
	 * 
	 * @param wg
	 * @return list of queriable metadata in json formatted string
	 */
	private String listFormReport(Workgroup wg) {
		List<String> metadataNames = reportManager.fetchFormMetadataList(wg.getWorkgroupName(), wg.getID());
		Map<String, List<String>> metadataGroup = new HashMap<String, List<String>>();
		metadataGroup.put("form", new ArrayList<String>());
		metadataGroup.put("survey", new ArrayList<String>());
		
		for (String name : metadataNames) {
			if (name.startsWith("survey_")) {
				metadataGroup.get("survey").add(name);
			} else {
				metadataGroup.get("form").add(name);
			}
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("list", metadataGroup);
		
		return finalJson.toString();
	}
	
	private String imDetailReport(Workgroup wg, String start, String end, String filter) {
		List<WorkgroupReportItemDTO> jsonArray = reportManager.getInstance().
				fetchCountPerTimeframeByIm(wg.getWorkgroupName(), wg.getID(), start, end, filter);
		
		return prepareList(jsonArray);
	}
	
	private String imReport(Workgroup wg, String start, String end) {
		List<WorkgroupReportItemDTO> jsonArray = reportManager.
				fetchCountPerTimeframe(wg.getWorkgroupName(), wg.getID(), start, end);
		
		List<WorkgroupReportItemDTO> tabArray = reportManager.
				fetchCountPerIm(wg.getWorkgroupName(), wg.getID(), start, end);
		
		return prepareTabular(jsonArray, tabArray);
	}
	
	private String tagDetailReport(Workgroup wg, String start, String end, String filter) {
		List<WorkgroupReportItemDTO> jsonArray = reportManager.getInstance().
				fetchCountPerTimeframeByTag(wg.getWorkgroupName(), wg.getID(), start, end, filter);
		
		return prepareList(jsonArray);
	}
	
	private String tagReport(Workgroup wg, String start, String end) {
		List<WorkgroupReportItemDTO> jsonArray = reportManager.getInstance().
				fetchCountPerTimeframe(wg.getWorkgroupName(), wg.getID(), start, end);
		
		List<WorkgroupReportItemDTO> tabArray = reportManager.getInstance().
				fetchCountPerTag(wg.getWorkgroupName(), wg.getID(), start, end);
		
		return prepareTabular(jsonArray, tabArray);
	}
	
	private String divisionDetailReport(Workgroup wg, String start, String end, String filter) {
		List<WorkgroupReportItemDTO> jsonArray = reportManager.getInstance().
				fetchCountPerTimeframeByDivision(wg.getWorkgroupName(), wg.getID(), start, end, filter);
		
		return prepareList(jsonArray);
	}
	
	private String divisionReport(Workgroup wg, String start, String end) {
		List<WorkgroupReportItemDTO> jsonArray = reportManager.getInstance().
				fetchCountPerTimeframe(wg.getWorkgroupName(), wg.getID(), start, end);
		
		List<WorkgroupReportItemDTO> tabArray = reportManager.getInstance().
				fetchCountPerDivision(wg.getWorkgroupName(), wg.getID(), start, end);
		
		return prepareTabular(jsonArray, tabArray);
	}
	
	private String agentDetailReport(Workgroup wg, String start, String end, String agentName) {
		JID agentJID = prepJID(wg.getWorkgroupName(), agentName);
		List<WorkgroupReportItemDTO> jsonArray = reportManager.getInstance().
				fetchCountPerTimeframeByAgent(wg.getWorkgroupName(), wg.getID(), start, end, agentJID.toBareJID());
		
		return prepareList(jsonArray);
	}
	
	private String agentReport(Workgroup wg, String start, String end) {
		List<WorkgroupReportItemDTO> jsonArray = reportManager.getInstance().
				fetchCountPerTimeframe(wg.getWorkgroupName(), wg.getID(), start, end);
		
		List<WorkgroupReportItemDTO> tabArray = reportManager.getInstance().
				fetchCountPerAgent(wg.getWorkgroupName(), wg.getID(), start, end);
		
		return prepareTabular(jsonArray, tabArray);
	}
	
	private String timeReport(Workgroup wg, String start, String end) {
		List<WorkgroupReportItemDTO> ansArray = reportManager.getInstance().
				fetchCountPerTimeframe(wg.getWorkgroupName(), wg.getID(), start, end);
		
		List<WorkgroupReportItemDTO> unansArray = reportManager.getInstance().
				fetchUnansweredCountPerTimeframe(wg.getWorkgroupName(), wg.getID(), start, end);
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("answered", ansArray);
		finalJson.put("unanswered", unansArray);
		
		return finalJson.toString();
	}
	
	@Override
	public void close() {
		super.close();
		
		reportManager = null;
		transcriptManager = null;
	}
	
}
