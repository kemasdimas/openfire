package com.kemasdimas.workgroupservice.dto;

import org.json.simple.JSONAware;

public final class WorkgroupAgentLoginHistoryDTO implements JSONAware {
	String agentJid;
	long joinTime;
	
	public WorkgroupAgentLoginHistoryDTO(String agentJid, long joinTime) {
		super();
		
		this.agentJid = agentJid;
		this.joinTime = joinTime;
	}

	@Override
	public String toJSONString() {
		String agentJidRaw[] = agentJid.split("@");
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("{");
		buffer.append("\"agentName\":\"" + agentJidRaw[0] + "\"");
		buffer.append(",\"joinTime\":" + joinTime);
		buffer.append("}");
		
		return buffer.toString();
	}
}
