package com.kemasdimas.workgroupservice.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.jivesoftware.database.ConnectionProvider;
import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.openfire.auth.AuthFactory;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;
import org.jivesoftware.xmpp.workgroup.utils.FastpathMultiDBConnectionProvider;

import com.kemasdimas.workgroupservice.dto.WorkgroupUserDTO;

/**
 * This class rely on global database connection
 * @author kemas
 *
 */
public final class WorkgroupDatabaseManager {
	
	private final static String CREATE_USER = "CREATE USER ?@'%'";
	private final static String UPDATE_USER = "UPDATE mysql.user set Password=PASSWORD(?) where USER = ?";
	private final static String GRANT_PRIV = "INSERT INTO mysql.db " +
			"(Host,Db,User,Select_priv,Insert_priv,Update_priv,Delete_priv,Create_priv,Drop_priv,Execute_priv) " +
			"VALUES('%', ?, ?, 'Y','Y','Y','Y','Y','Y','Y')";
	private final static String DELETE_USER = "DELETE FROM mysql.user WHERE USER = ?";
	private final static String DELETE_PRIV = "DELETE FROM mysql.db WHERE USER = ?";
	
	private final static String INSERT_DBASE_MAP = "INSERT INTO wgDatabaseMap (workgroup_name, database_host, database_host_schema, database_username, database_password) " +
			"VALUES (?, ?, ?, ?, ?)";
	private final static String DELETE_DBASE_MAP = "UPDATE wgDatabaseMap SET workgroup_name = ? WHERE workgroup_name = ?";
	
	private final static String DELETE_LOGIN_CACHE = "DELETE FROM wgLoginCache WHERE username LIKE ?";
	private final static String DELETE_LEGACY_AGENT = "DELETE FROM wgLegacyAgent WHERE userid LIKE ?";
	
	private final static String DROP_DATABASE = "DROP DATABASE ?";
	
	private final static String OPENFIRE_DB_COMMAND = "/usr/bin/openfire_db";
	
	private static WorkgroupDatabaseManager instance;
	
	private boolean verbose = true;
	
	public static WorkgroupDatabaseManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupDatabaseManager();
		}
		
		return instance;
	}
	
	private boolean executeDbCommand(String type, String rootUsername, String rootPassword, String db, String timestamp, String host) throws Exception {
		Process proc = Runtime.getRuntime().exec(new String[] { OPENFIRE_DB_COMMAND, type, rootUsername, rootPassword, db, timestamp, host });
		int rV = proc.waitFor();
		
		if (verbose) {
			InputStream inputstream = proc.getErrorStream();
			InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
			BufferedReader bufferedreader = new BufferedReader(inputstreamreader);

			// read the output
			String line;
			while ((line = bufferedreader.readLine()) != null) {
				System.out.println(line);
			}
		}
		
		return rV == 0;
	}
	
	public void createWorkgroupDatabase(String wgName, String hostname) throws Exception {		
		// Getting the root username from constant
		String rootUsername = JiveGlobals.getXMLProperty("database.defaultProvider.username");
        String rootPassword = JiveGlobals.getXMLProperty("database.defaultProvider.password");
		String dbaseHost	= JiveGlobals.getXMLProperty("database.defaultProvider.serverURL");
		dbaseHost = dbaseHost.split("//")[1].split(":")[0];
        
		// Conver WGname to lowercase!
		wgName = wgName.toLowerCase();
		
		String db = "wg_" + wgName;
		String user = "user_" + wgName;
		String password = wgName + "-" + (new Date().getTime()) / 2500;
		
		if (!executeDbCommand("clone", rootUsername, rootPassword, db, "-", dbaseHost)) {
			throw new Exception("Database cloning error.");
		}
		
		Connection con = null;
		PreparedStatement statement = null;
		boolean abort = false;
		try {
			con = FastpathDBManager.getTransactionConnection();
			
			// Create user
			// If user creation is error, abort all action!
			statement = con.prepareStatement(CREATE_USER.replace("?", user));
			statement.executeUpdate();
			
			// Create database map
			statement = con.prepareStatement(INSERT_DBASE_MAP);
			statement.setString(1, wgName);
			// Hostname is fixed to localhost
			// TODO: could be dynamic?
			statement.setString(2, dbaseHost);
			statement.setString(3, db);
			statement.setString(4, user);
			statement.setString(5, AuthFactory.encryptPassword(password));
			statement.executeUpdate();
			
			// Update user
			statement = con.prepareStatement(UPDATE_USER);
			statement.setString(1, password);
			statement.setString(2, user);
			statement.executeUpdate();
			
			// Grant user privileges to access database
			statement = con.prepareStatement(GRANT_PRIV);
			statement.setString(1, db);
			statement.setString(2, user);
			statement.executeUpdate();
			
			statement = con.prepareStatement("FLUSH PRIVILEGES");
			statement.executeUpdate();
			
		} catch (SQLException e) {
			abort = true;
		} finally {
			FastpathDBManager.closeTransactionConnection(statement, con, abort);
		}
		
		if (abort) {
			throw new Exception("Error creating database.");
		}
	}
	
	public void deleteWorkgroupDatabaseMap(String wgName) throws Exception {
		Connection con = null;
		PreparedStatement statement = null;
		boolean abort = false;
		
		// Getting the root username from constant
		String rootUsername = JiveGlobals.getXMLProperty("database.defaultProvider.username");
        String rootPassword = JiveGlobals.getXMLProperty("database.defaultProvider.password");
        String dbaseHost	= JiveGlobals.getXMLProperty("database.defaultProvider.serverURL");
		dbaseHost = dbaseHost.split("//")[1].split(":")[0];
		
		String user = "user_" + wgName;
		String db = "wg_" + wgName;
		
		ConnectionProvider provider = FastpathDBManager.getProvider(wgName);
		
		// Only delete the database map if this wgName has a map!
		// 12 November 2012, major database connection change, default to not deleting any schema when it has no mapping
		if (provider instanceof FastpathMultiDBConnectionProvider) {
			FastpathMultiDBConnectionProvider providerTemp = (FastpathMultiDBConnectionProvider) provider;
			
			String password = providerTemp.getPassword();
			String host = providerTemp.getHost();
			
			if (!executeDbCommand("backup", rootUsername, rootPassword, db, String.valueOf(new Date().getTime()), host)) {
				throw new Exception("Database backup error!");
			}
			
			try {
				con = FastpathDBManager.getTransactionConnection();
							
				statement = con.prepareStatement(DELETE_DBASE_MAP);
				statement.setString(1, wgName + "-deleted");
				statement.setString(2, wgName);
				statement.executeUpdate();
				
				statement = con.prepareStatement(DELETE_USER);
				statement.setString(1, user);
				statement.executeUpdate();
				
				statement = con.prepareStatement(DELETE_PRIV);
				statement.setString(1, user);
				statement.executeUpdate();
				
				statement = con.prepareStatement(DROP_DATABASE.replace("?", db));
				statement.executeUpdate();
				
				statement = con.prepareStatement(DELETE_LEGACY_AGENT);
				statement.setString(1, "%-" + wgName);
				statement.executeUpdate();
				
				statement = con.prepareStatement(DELETE_LOGIN_CACHE);
				statement.setString(1, "%-" + wgName);
				statement.executeUpdate();
				
				statement = con.prepareStatement("FLUSH PRIVILEGES");
				statement.executeUpdate();
				
				FastpathDBManager.removeProvider(wgName);
				
			} catch (SQLException e) {
				abort = true;
			} finally {
				FastpathDBManager.closeTransactionConnection(statement, con, abort);
			}
		} else {
			try {
				con = FastpathDBManager.getTransactionConnection();
				
				statement = con.prepareStatement(DELETE_LEGACY_AGENT);
				statement.setString(1, "%-" + wgName);
				statement.executeUpdate();
				
				statement = con.prepareStatement(DELETE_LOGIN_CACHE);
				statement.setString(1, "%-" + wgName);
				statement.executeUpdate();
			} catch (SQLException e) {
				abort = true;
			} finally {
				FastpathDBManager.closeTransactionConnection(statement, con, abort);
			}
		}
	}
}
