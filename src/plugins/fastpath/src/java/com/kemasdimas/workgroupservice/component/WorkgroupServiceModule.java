package com.kemasdimas.workgroupservice.component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.jivesoftware.openfire.fastpath.util.WorkgroupUtils;
import org.jivesoftware.openfire.group.Group;
import org.jivesoftware.openfire.group.GroupAlreadyExistsException;
import org.jivesoftware.openfire.group.GroupManager;
import org.jivesoftware.openfire.group.GroupNotFoundException;
import org.jivesoftware.openfire.lockout.LockOutManager;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.NotFoundException;
import org.jivesoftware.xmpp.workgroup.RequestQueue;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.Workgroup.Status;
import org.jivesoftware.xmpp.workgroup.routing.RoutingManager;
import org.jivesoftware.xmpp.workgroup.routing.RoutingRule;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.xmpp.component.ComponentManagerFactory;

import com.kemasdimas.workgroupservice.dto.WorkgroupTransportDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupTransportRuleDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupUserDTO;
import com.kemasdimas.workgroupservice.manager.WorkgroupAgentManager;
import com.kemasdimas.workgroupservice.manager.WorkgroupDatabaseManager;
import com.kemasdimas.workgroupservice.manager.WorkgroupTransportManager;

public final class WorkgroupServiceModule extends BaseWorkgroupServiceModule {
		
	protected static WorkgroupServiceModule instance;
	private RoutingManager routingManager;
	private WorkgroupTransportManager transportManager;
	private WorkgroupDatabaseManager dbaseManager;
	
	public WorkgroupServiceModule() {
		super();
		
		routingManager = RoutingManager.getInstance();
		transportManager = WorkgroupTransportManager.getInstance();
		dbaseManager = WorkgroupDatabaseManager.getInstance();
	}
	
	public static WorkgroupServiceModule getInstance() {
		if (instance == null) {
			instance = new WorkgroupServiceModule();
		}
		
		return (WorkgroupServiceModule) instance;
	}
	
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		String transportHandler = request.getParameter("transportHandler");
		
		if (wgName == null) {
			throw new Exception("workgroupName can not be null");
		}
		
		if ("create".equals(type)) {
			String initialPassword = request.getParameter("initialPassword");
			String adminEmail = request.getParameter("adminEmail");
			
			if (initialPassword == null || adminEmail == null) {
				throw new Exception("initialPassword / adminEmail can not be null");
			}
			
			return createWorkgroup(wgName, initialPassword, adminEmail);
		}
		
		Workgroup wg = findWorkgroup(wgName);
				
		if ("disable".equals(type)) {
			return setWorkgroupStatus(wg, Workgroup.Status.CLOSED);
		} else if ("enable".equals(type)) {
			return setWorkgroupStatus(wg, Workgroup.Status.READY);
		} else if ("delete".equals(type)) {
			return deleteWorkgroup(wg);
		} else if ("available".equals(type)) {
			return isWorkgroupAvailable(wgName);
		} else if ("update-analytics-id".equals(type)) {
			String analyticsId = request.getParameter("analyticsId");
			
			if (analyticsId == null) {
				throw new Exception("analyticsId can not be null");
			}
			
			return updateWorkgroupAnalyticsAccount(wg, analyticsId);
		} else if ("create-rule".equals(type) || "edit-rule".equals(type)) {
			String channelName = request.getParameter("channelName");
			
			if (channelName == null || transportHandler == null) {
				throw new Exception("channelName / transportHandler can not be null");
			}
			
			if ("create-rule".equals(type)) {
				return createRule(wg, channelName, transportHandler);
			} else {
				return editRule(wg, channelName, transportHandler);
			}
		} else if ("delete-rule".equals(type)) {
			
			if (request.getParameter("rulePosition") != null) {
				int rulePosition = Integer.parseInt(request.getParameter("rulePosition"));
				return deleteRule(wg, rulePosition);
			} else {
				if (transportHandler == null) {
					throw new Exception("transportHandler can not be null");
				}
				
				return deleteRule(wg, transportHandler);
			}
		} else if ("list-rule".equals(type)) {
			return listRule(wg);
		}
		
		throw new Exception("Type not supported");
	}
	
	private String updateWorkgroupAnalyticsAccount(Workgroup wg, String analyticsId) {
		if (analyticsId.equalsIgnoreCase("null")) {
			analyticsId = null;
		}
		
		wg.setAnalyticsAccountId(analyticsId);
		
		return "\"ok\"";
	}
	
	/**
	 * Create routing rule for a workgroup, based on transport handler
	 * 
	 * @param wg
	 * @param channelName
	 * @param transportHandler
	 * @return
	 * @throws NotFoundException
	 */
	private String createRule(Workgroup wg, String channelName, String transportHandler) throws NotFoundException {
		if (!transportHandler.startsWith("widget") && !transportManager.isTransportExists(transportHandler, wg.getID(), wg.getWorkgroupName())) {
			throw new NotFoundException("Specified transport can not be found");
		}
		
		RequestQueue queue = wg.getRequestQueue(channelName);
		
		// Only create rule for channel variable, see ChatBot.java
		String query = "channel:\"" + transportHandler.replace("-", "") + "\"";
		int rulesCount = routingManager.getRoutingRules(wg).size() + 1;
		
		routingManager.addRoutingRule(wg, queue.getID(), rulesCount, query);
		
		return "\"ok\"";
	}
	
	/**
	 * Delete routing rule from a workgroup
	 * 
	 * @param wg
	 * @param rulePosition
	 * @return
	 */
	private String deleteRule(Workgroup wg, int rulePosition) {
		Collection<RoutingRule> rules = routingManager.getRoutingRules(wg);
		
		routingManager.removeRoutingRule(wg, rulePosition);
		for (RoutingRule rule : rules) {
			if (rule.getPosition() > rulePosition) {
				routingManager.updateRoutingRule(wg, rule.getPosition(), rule.getPosition() - 1);
			}
		}
		
		return "\"ok\""; 
	}
	
	/**
	 * Delete routing rule from a workgroup
	 * 
	 * @param wg
	 * @param String transportHandler string value
	 * @return
	 */
	private String deleteRule(Workgroup wg, String transportHandler) {
		Collection<RoutingRule> rules = routingManager.getRoutingRules(wg);
		int minusAccumulator = 0;
		
		for (RoutingRule rule : rules) {
			if (rule.getQuery().contains(transportHandler.replace("-", ""))) {
				routingManager.removeRoutingRule(wg, rule.getPosition());
				
				minusAccumulator++;
			} else if (minusAccumulator > 0) {
				routingManager.updateRoutingRule(wg, rule.getPosition(), rule.getPosition() - minusAccumulator);
			}
		}
		
		return "\"ok\""; 
	}
	
	/**
	 * Edit routing rule for a workgroup, based on transport handler
	 * 
	 * @param wg
	 * @param channelName
	 * @param transportHandler
	 * @return
	 * @throws NotFoundException
	 */
	private String editRule(Workgroup wg, String channelName, String transportHandler) throws NotFoundException {
		
		if (channelName != null) {
			deleteRule(wg, transportHandler);
			createRule(wg, channelName, transportHandler);
		}
		
		return "\"ok\"";
	}
	
	/**
	 * List workgroup rules to route specified transport to a queue
	 * 
	 * @param wg
	 * @return
	 * @throws NotFoundException
	 */
	private String listRule(Workgroup wg) throws NotFoundException {
		ArrayList<WorkgroupTransportRuleDTO> transportRules = 
				transportManager.fetchTransportRuleInWorkgroup(wg.getID(), 
						wg.getWorkgroupName());
		
		JSONArray jsonArray = new JSONArray();
		for (WorkgroupTransportRuleDTO rule : transportRules) {
			String queueName = "";
			try {
				queueName = wg.getRequestQueue(rule.getQueueID()).getName();
			} catch (Exception e) {
				continue;
			}
			int order = rule.getRulePosition();
			String username = rule.getUsername();
			String transportType = rule.getTransportType();
			
			JSONObject json = new JSONObject();
			json.put("order", order);
			json.put("username", username);
			json.put("transportType", transportType);
			json.put("queueName", queueName);
			
			jsonArray.add(json);
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", jsonArray.size());
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	/**
	 * Check whether a workgroup is available for use
	 * 
	 * @param wgName
	 * @return
	 */
	private String isWorkgroupAvailable(String wgName) {
		try {
			Workgroup wg = findWorkgroup(wgName);
			
			return "false";
		} catch (Exception e) {
			return "true";
		}
	}
	
	/**
	 * Set the workgroup status to disable / enable certain workgroup
	 * 
	 * @param wgName
	 * @param wgStatus
	 * @return
	 * @throws Exception
	 */
	private String setWorkgroupStatus(Workgroup wg, Status wgStatus) throws Exception {
		wg.setStatus(wgStatus);
	
		User theUser = null;
		// Also disable all the user in this workgroup
		int count = WorkgroupAgentManager.getInstance().countUserInWorkgroup(wg.getWorkgroupName());
		List<WorkgroupUserDTO> users = WorkgroupAgentManager.getInstance().fetchUserInWorkgroup(wg.getWorkgroupName(), count, 0);
		
		for (WorkgroupUserDTO user : users) {
			if (wgStatus == Status.CLOSED) {
				LockOutManager.getInstance().disableAccount(user.getUsername(), null, null);
			} else {
				LockOutManager.getInstance().enableAccount(user.getUsername());
			}
		}
		
		count = WorkgroupTransportManager.getInstance().countTransportAgent(wg.getID());
		List<WorkgroupTransportDTO> transports = WorkgroupTransportManager.getInstance().fetchTransportInWorkgroup(wg.getID(), count, 0);
		
		for (WorkgroupTransportDTO transport : transports) {
			if (wgStatus == Status.CLOSED) {
				LockOutManager.getInstance().disableAccount(transport.getUserId(), null, null);
			} else {
				LockOutManager.getInstance().enableAccount(transport.getUserId());
			}
		}
		
		return "\"ok\"";
	}
	
	/**
	 * Delete a workgroup from database ***BEWARE!!!
	 * 
	 * @param wgName
	 * @return
	 * @throws Exception
	 */
	private String deleteWorkgroup(Workgroup wg) throws Exception {
		String wgName = wg.getWorkgroupName();
		
		wgManager.deleteWorkgroup(wg);
		
		// Delete the database map (only the mapping, not the schema)
		dbaseManager.deleteWorkgroupDatabaseMap(wgName);
		
		// Also delete the corresponding group and users
		try {
			GroupManager groupManager = GroupManager.getInstance();
			Group group = groupManager.getGroup(wgName + "-group");
			groupManager.deleteGroup(group);
		} catch (GroupNotFoundException e) {
		}
		
		int userCount = WorkgroupAgentManager.getInstance().countUserInWorkgroup(wgName);
		List<WorkgroupUserDTO> wgUsers = WorkgroupAgentManager.getInstance().
				fetchUserInWorkgroup(wgName, userCount, 0);
		
		for (WorkgroupUserDTO user : wgUsers) {
			try {
				UserManager.getInstance().deleteUser(
						UserManager.getInstance().getUser(user.getUsername()));
			} catch (UserNotFoundException e) {
			}
		}
		
		return "\"ok\"";
	}
	
	/**
	 * Create a workgroup using certain name
	 * 
	 * @param wgName
	 * @return
	 * @throws Exception
	 */
	private String createWorkgroup(String wgName, String initialPassword, String adminEmail) throws Exception {
		if ("false".equals(isWorkgroupAvailable(wgName))) {
			throw new Exception("Workgroup name already used.");
		}
		
		// Prepare the database
		// 12 November 2012, major database connection change, default to not creating a new schema
		// each time new workgroup is created!
//		String hostname = ComponentManagerFactory.getComponentManager().getServerName();
//		dbaseManager.createWorkgroupDatabase(wgName, hostname);
		
		Map<String, String> errors = WorkgroupUtils.createWorkgroup(wgName, 
				"Created at: " + Calendar.getInstance().getTimeInMillis(), "");
		
		// Also create the corresponding group
		try {
			GroupManager groupManager = GroupManager.getInstance();
			Group group = groupManager.createGroup(wgName + "-group");
			
			// TODO: enable contact sharing! with groupname = "Internal Group"
			group.getProperties().put("sharedRoster.showInRoster", "onlyGroup");
            group.getProperties().put("sharedRoster.displayName", "Internal Group");
            group.getProperties().put("sharedRoster.groupList", "");
		} catch (GroupAlreadyExistsException groupEx) {
			
		}
		
		// Create default admin-workgroupname
		try {
			Workgroup wg = findWorkgroup(wgName);
			AgentServiceModule.getInstance().createAgent(wg, "admin", initialPassword, "Workgroup Admin", adminEmail);
		} catch (Exception e) { }
		
		if (errors.size() != 0) {
			Iterator<Entry<String, String>> itr = errors.entrySet().iterator();
			StringBuffer buffer = new StringBuffer("[");
			
			while (itr.hasNext()) {
				buffer.append(itr.next());
				buffer.append(",");
			}
			buffer.setCharAt(buffer.length() - 1, ']');
			
			throw new Exception(buffer.toString());
		}
		
		return "\"ok\"";
	}	
	
	@Override
	public void close() {
		routingManager = null;
		transportManager = null;
		dbaseManager = null;
		
		super.close();
	}
}
