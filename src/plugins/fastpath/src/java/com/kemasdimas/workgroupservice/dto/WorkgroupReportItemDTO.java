package com.kemasdimas.workgroupservice.dto;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public final class WorkgroupReportItemDTO implements JSONAware {
	String label;
	long value;
	
	public WorkgroupReportItemDTO(String label, long value) {
		super();
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public String toJSONString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("{");
		buffer.append("\"" + JSONObject.escape(label) + "\"");
		buffer.append(":");
		buffer.append(value);
		buffer.append("}");
		
		return buffer.toString();
	}
}
