package com.kemasdimas.workgroupservice.dto;

public class WorkgroupPreviousSessionDTO {
	public int sessionCount;
	public long previousTimestamp;
	public long firstTimestamp;
	
	public WorkgroupPreviousSessionDTO(int sessionCount, long previousTimestamp, long firstTimestamp) {
		this.sessionCount = sessionCount;
		this.previousTimestamp = previousTimestamp;
		this.firstTimestamp = firstTimestamp;
	}
}
