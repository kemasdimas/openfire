package com.kemasdimas.workgroupservice.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.openfire.fastpath.settings.chat.KeyEnum;
import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;

import com.kemasdimas.workgroupservice.dto.WorkgroupAutomessageDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupAutomessageTypeDTO;

public final class WorkgroupChannelManager {
	private final static String GET_AUTOMESSAGES = "SELECT * FROM fpChatSetting WHERE workgroupNode = ? AND queueID = ?";
	private final static List<WorkgroupAutomessageTypeDTO> MESSAGE_NAMES = new ArrayList<WorkgroupAutomessageTypeDTO>();
	static {
		MESSAGE_NAMES.add(new WorkgroupAutomessageTypeDTO(KeyEnum.welcome_message.toString(), "Division greetings"));
		MESSAGE_NAMES.add(new WorkgroupAutomessageTypeDTO(KeyEnum.bye_message.toString(), "Division goodbye message"));
	}
	
	private static WorkgroupChannelManager instance;
	
	public static WorkgroupChannelManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupChannelManager();
		}
		
		return instance;
	}
	
	public ArrayList<WorkgroupAutomessageTypeDTO> fetchAutomessageType() {
		return (ArrayList<WorkgroupAutomessageTypeDTO>) MESSAGE_NAMES;
	}
	
	public ArrayList<WorkgroupAutomessageDTO> fetchAutomessageInWorkgroup(String wgName, long queueIdd) {
		ArrayList<WorkgroupAutomessageDTO> buffer = new ArrayList<WorkgroupAutomessageDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(GET_AUTOMESSAGES);
			
			statement.setString(1, wgName);
			statement.setLong(2, queueIdd);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				buffer.add(new WorkgroupAutomessageDTO(
						result.getString("workgroupNode"), 
						result.getLong("queueID"), 
						result.getString("name"),
						result.getString("label"), 
						result.getString("value")));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
}
