package com.kemasdimas.workgroupservice.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class WorkgroupTranscriptDTO implements JSONAware {
	private String sessionID;
	private long startTime;
	private long endTime;
	private String body;
	private String notes;
	private ArrayList<String> agents = new ArrayList<String>();
	private Map<String, String> metadata = new TreeMap<String, String>();
	
	public WorkgroupTranscriptDTO(String sessionID) {
		this.sessionID = sessionID;
		
		body = null;
		notes = null;
		startTime = -1;
		endTime = -1;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public List<String> getAgentList() {
		return agents;
	}
	
	public void addAgent(String name) {
		agents.add(name);
	}
	
	public Map<String, String> getMetadata() {
		return metadata;
	}
	
	public void addMetadata(String key, String value) {
		metadata.put(key, value);
	}

	@Override
	public String toJSONString() {
		// Build agents string
		StringBuffer agentBuffer = new StringBuffer();
		for (String agent : agents) {
			agentBuffer.append("{");
			agentBuffer.append("\"name\":\"" + agent + "\"");
			agentBuffer.append("},");
		}
		if (agentBuffer.length() > 0) {
			agentBuffer.deleteCharAt(agentBuffer.length() - 1);
		}
		
		// Build metadata string
		StringBuffer metaBuffer = new StringBuffer();
		for (String key : metadata.keySet()) {
			metaBuffer.append("{");
			metaBuffer.append("\"key\":\"" + key + "\"");
			metaBuffer.append(",\"value\":\"" + metadata.get(key) + "\"");
			metaBuffer.append("},");
		}
		if (metaBuffer.length() > 0) {
			metaBuffer.deleteCharAt(metaBuffer.length() - 1);
		}
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("{");
		buffer.append("\"sessionId\":\"" + sessionID + "\"");
		buffer.append(",\"start\":" + startTime);
		buffer.append(",\"end\":" + endTime);
		buffer.append(",\"agents\":[" + agentBuffer.toString() + "]");
		buffer.append(",\"metadata\":[" + metaBuffer.toString() + "]");
		buffer.append("}");
		
		return buffer.toString();
	}
}
