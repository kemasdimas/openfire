package com.kemasdimas.workgroupservice.dto;

public final class WorkgroupTransportDTO {

	private String userId;
	private String transportType;
	private String transportUserName;
	
	public WorkgroupTransportDTO(String userId, String transportType,
			String transportUserName) {
		super();
		this.userId = userId;
		this.transportType = transportType;
		this.transportUserName = transportUserName;
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getTransportType() {
		return transportType;
	}
	
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}
	
	public String getTransportUserName() {
		return transportUserName;
	}
	
	public void setTransportUserName(String transportUserName) {
		this.transportUserName = transportUserName;
	}
	
}
