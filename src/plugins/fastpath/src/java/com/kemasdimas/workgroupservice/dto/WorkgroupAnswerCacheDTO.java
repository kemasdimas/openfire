package com.kemasdimas.workgroupservice.dto;

import java.util.HashMap;

public final class WorkgroupAnswerCacheDTO {

	private String sessionId;
	private HashMap<String, String> metaData;
	
	public WorkgroupAnswerCacheDTO(String sessionId, HashMap<String, String> metaData) {
		super();
		
		this.sessionId = sessionId;
		this.metaData = metaData;
	}

	public String getSessionId() {
		return sessionId;
	}
	
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public HashMap<String, String> getMetaData() {
		return metaData;
	}
	
	public void setMetaData(HashMap<String, String> metaData) {
		this.metaData = metaData;
	}
	
}
