package com.kemasdimas.workgroupservice.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;
import org.xmpp.component.ComponentManagerFactory;

import com.kemasdimas.workgroupservice.dto.WorkgroupTransportDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupTransportRuleDTO;

public final class WorkgroupTransportManager {
	// Queries below use global openfire database
	private final static String INSERT_TRANSPORT_AGENT = "INSERT INTO wgLegacyAgent (userId, workgroupId) VALUES " +
			"(?, ?)";
	private final static String DELETE_TRANSPORT_AGENT = "DELETE FROM wgLegacyAgent WHERE " +
			"userId=? AND workgroupId=?";
	private final static String COUNT_TRANSPORT_AGENT = "SELECT COUNT(*) FROM wgLegacyAgent WHERE " +
			"userId=? AND workgroupId=?";
	private final static String LOAD_TRANSPORT_AGENT = "SELECT userId, transportType, username FROM " +
			"(SELECT userId, CONCAT(userId, ?) jid FROM wgLegacyAgent WHERE workgroupId = ?) usr " +
			"JOIN ofGatewayRegistration reg on usr.jid = reg.jid LIMIT ? OFFSET ?";
	private final static String COUNT_TRANSPORT_AGENT_LIST = "SELECT COUNT(*) FROM " +
			"(SELECT userId, CONCAT(userId, ?) jid FROM wgLegacyAgent WHERE workgroupId = ?) usr " +
			"JOIN ofGatewayRegistration reg on usr.jid = reg.jid";
	private final static String TRANSPORT_RULE_EXTENSION = "SELECT REPLACE(user.username, '-', ''), reg.username, reg.transportType FROM ofUser user JOIN " +
			"ofGatewayRegistration reg ON reg.jid = CONCAT(user.username, ?) WHERE REPLACE(user.username, '-', '') IN (#array#)";
	
	// Queries using the spectrum database
	private final static String SPECTRUM_EDIT_TRANSPORT = 
			"UPDATE spectrum_users SET uin=?, password=? WHERE jid=?";
	
	// Queries below use local workgroup database
	private final static String TRANSPORT_RULE = "SELECT workgroupID, queueID, rulePosition, REPLACE(REPLACE(query, '\"', ''), 'channel:' , '') query FROM " +
			"fpRouteRule rule  WHERE workgroupID = ? ORDER BY rulePosition ASC;";
	private final static String GET_TRANSPORT_QUEUE = "SELECT queueID FROM fpRouteRule WHERE REPLACE(REPLACE(query, '\"', ''), 'channel:' , '')" +
			" = REPLACE(?, '-', '') ORDER BY rulePosition LIMIT 1;";
	
	private static WorkgroupTransportManager instance;
	
	public static WorkgroupTransportManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupTransportManager();
		}
		
		return instance;
	}
	
	/**
	 * 
	 * *This function must use 2 different database connections because it needs to query from the global database
	 * and also the workgroup local database
	 * 
	 * @param wgId
	 * @param workgroupName
	 * @return
	 */
	public ArrayList<WorkgroupTransportRuleDTO> fetchTransportRuleInWorkgroup(long wgId, String workgroupName) {
		String serverName = ComponentManagerFactory.getComponentManager().getServerName();
		ArrayList<WorkgroupTransportRuleDTO> buffer = new ArrayList<WorkgroupTransportRuleDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		boolean isFailed = false;
		try {
			con = FastpathDBManager.getConnection(workgroupName);
			statement = con.prepareStatement(TRANSPORT_RULE);
			//statement.setString(1, "@" + serverName);
			statement.setLong(1, wgId);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				// Temporary hold the userID
				WorkgroupTransportRuleDTO rule = new WorkgroupTransportRuleDTO(
					result.getLong(1), 
					result.getLong(2), 
					result.getInt(3), 
					result.getString(4), 
					"");
				
				buffer.add(rule);
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			buffer.clear();
			isFailed = true;
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		if (!isFailed && buffer.size() > 0) {
			ArrayList<String> stringBuff = new ArrayList<String>();
			for (WorkgroupTransportRuleDTO rule : buffer) {
				stringBuff.add(rule.getUsername());
			}
			
			Map<String, String[]> extensionsMap = getTransportDetail(stringBuff, serverName);
			
			String[] values = null;
			
			// Replace the username / transporttype in buffer
			for (int i = 0; i < buffer.size(); i++) {
				// It is possible to have no corresponding extensionsMap
				// because the transport handler might be deleted without automatically deleting a queue
				WorkgroupTransportRuleDTO item = buffer.get(i); 
				values = extensionsMap.get(item.getUsername());
				if (values != null) {
					item.setUsername(values[0]);
					item.setTransportType(values[1]);
				} else {
					buffer.remove(item);
					i--;
				}
			}
			
			extensionsMap.clear();
		}
		
		return buffer;
	}
	
	public Map<String, String[]> getTransportDetail(List<String> buffer, String serverName) {
		// Preparing the statement replacement for IN
		int count = buffer.size();
		StringBuilder inReplace = new StringBuilder();
		for (int i = 0; i < count; i++) {
			inReplace.append("?,");
		}
		
		String sqlStatement = TRANSPORT_RULE_EXTENSION.replace("#array#", inReplace.substring(0, inReplace.length() - 1));
		Map<String, String[]> extensionsMap = new HashMap<String, String[]>();
		
		Connection con = null;
		PreparedStatement statement = null;
		// Fetch the additional data from database, using the global database connection
		try {
			con = FastpathDBManager.getConnection();
			statement = con.prepareStatement(sqlStatement);
			statement.setString(1, "@" + serverName);
			for (int i = 0; i < count; i++){
				statement.setString(i + 2, buffer.get(i));
			}
			
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				extensionsMap.put(result.getString(1), new String[] {result.getString(2), result.getString(3)});
			}
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			buffer.clear();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return extensionsMap;
	}
	
	public long getTransportQueue(String transportHandler, String workgroupName) {
		long rV = -1;
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(workgroupName);
			statement = con.prepareStatement(GET_TRANSPORT_QUEUE);
			statement.setString(1, transportHandler);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				rV = result.getLong(1);
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			rV = -1;
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return rV;
	}
	
	//#region Functions below are using global database connection
	
	public void addTransport(String userId, long workgroupId, String workgroupName) throws SQLException {
		modifyTransportTable(userId, workgroupId, INSERT_TRANSPORT_AGENT, workgroupName);
	}
	
	public void deleteTransport(String userId, long workgroupId, String workgroupName) throws SQLException {
		modifyTransportTable(userId, workgroupId, DELETE_TRANSPORT_AGENT, workgroupName);
	}
	
	public ArrayList<WorkgroupTransportDTO> fetchTransportInWorkgroup(long wgId, int limit, int offset) {
		String serverName = ComponentManagerFactory.getComponentManager().getServerName();
		ArrayList<WorkgroupTransportDTO> buffer = new ArrayList<WorkgroupTransportDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection();
			statement = con.prepareStatement(LOAD_TRANSPORT_AGENT);
			statement.setString(1, "@" + serverName);
			statement.setLong(2, wgId);
			statement.setInt(3, limit);
			statement.setInt(4, offset);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				buffer.add(new WorkgroupTransportDTO(
						result.getString(1), 
						result.getString(2), 
						result.getString(3)));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public int countTransportAgent(long workgroupId) {
		String serverName = ComponentManagerFactory.getComponentManager().getServerName();
		
		int rV = 0;
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection();
			statement = con.prepareStatement(COUNT_TRANSPORT_AGENT_LIST);
			statement.setString(1, "@" + serverName);
			statement.setLong(2, workgroupId);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				rV = result.getInt(1);
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			rV = 0;
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return rV;
	}
	
	public boolean isTransportExists(String userId, long workgroupId, String workgroupName) {
		boolean rV = false;
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection();
			statement = con.prepareStatement(COUNT_TRANSPORT_AGENT);
			statement.setString(1, userId);
			statement.setLong(2, workgroupId);
			
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				if (result.getInt(1) > 0) {
					rV = true;
					break;
				}
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			rV = false;
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return rV;
	}
	
	public void editSpectrumTransport(String jid, String newUsername, String newPassword) throws SQLException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection();
            pstmt = con.prepareStatement(SPECTRUM_EDIT_TRANSPORT);
            pstmt.setString(1, newUsername);
            pstmt.setString(2, newPassword);
            pstmt.setString(3, jid);
            
            pstmt.executeUpdate();
        }
        catch (SQLException sqle) {
            throw sqle;
        }
        finally {
        	FastpathDBManager.closeConnection(pstmt, con);
        }
    }
	
	private void modifyTransportTable(String userId, long workgroupId, String statement, String workgroupName) throws SQLException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = FastpathDBManager.getConnection();
            pstmt = con.prepareStatement(statement);
            pstmt.setString(1, userId);
            pstmt.setLong(2, workgroupId);
            
            pstmt.executeUpdate();
        }
        catch (SQLException sqle) {
            throw sqle;
        }
        finally {
        	FastpathDBManager.closeConnection(pstmt, con);
        }
    }
}
