/**
 * $RCSfile$
 * $Revision: 1710 $
 * $Date: 2005-07-26 11:56:14 -0700 (Tue, 26 Jul 2005) $
 *
 * Copyright (C) 2004-2008 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kemasdimas.workgroupservice;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpException;
import org.jivesoftware.admin.AuthCheckFilter;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.fastpath.FastpathPlugin;
import org.jivesoftware.openfire.user.UserAlreadyExistsException;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.Log;

import com.kemasdimas.workgroupservice.component.AgentServiceModule;
import com.kemasdimas.workgroupservice.component.LegacyServiceModule;
import com.kemasdimas.workgroupservice.component.MacroServiceModule;
import com.kemasdimas.workgroupservice.component.QueueServiceModule;
import com.kemasdimas.workgroupservice.component.ReportServiceModule;
import com.kemasdimas.workgroupservice.component.SettingServiceModule;
import com.kemasdimas.workgroupservice.component.SmsServiceModule;
import com.kemasdimas.workgroupservice.component.TranscriptServiceModule;
import com.kemasdimas.workgroupservice.component.WorkgroupServiceModule;


/**
 * Servlet that addition/deletion/modification of the users info in the system.
 * Use the <b>type</b>
 * parameter to specify the type of action. Possible values are <b>add</b>,<b>delete</b> and
 * <b>update</b>. <p>
 * <p/>
 * The request <b>MUST</b> include the <b>secret</b> parameter. This parameter will be used
 * to authenticate the request. If this parameter is missing from the request then
 * an error will be logged and no action will occur.
 *
 * @author Justin Hunt
 */
public class WorkgroupServiceServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private FastpathPlugin plugin;
    
	private WorkgroupServiceModule workgroupModule;
    private AgentServiceModule agentModule;
    private LegacyServiceModule transportModule;
    private QueueServiceModule queueModule;
    private SettingServiceModule settingModule;
    private MacroServiceModule macroModule;
    private ReportServiceModule reportModule;
    private TranscriptServiceModule transcriptModule;
    private SmsServiceModule smsModule;
    
    @Override
	public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        plugin = (FastpathPlugin) XMPPServer.getInstance().getPluginManager().getPlugin("fastpath");
        
        workgroupModule = WorkgroupServiceModule.getInstance();
        agentModule = AgentServiceModule.getInstance();
        transportModule = LegacyServiceModule.getInstance();
        queueModule = QueueServiceModule.getInstance();
        settingModule = SettingServiceModule.getInstance();
        macroModule = MacroServiceModule.getInstance();
        reportModule = ReportServiceModule.getInstance();
        transcriptModule = TranscriptServiceModule.getInstance();
        smsModule = SmsServiceModule.getInstance();
        
        // Exclude this servlet from requiring the user to login
        AuthCheckFilter.addExclude("fastpath/workgroupservice");
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        // Printwriter for writing out responses to browser
        PrintWriter out = response.getWriter();

        if (!plugin.getAllowedIPs().isEmpty()) {
            // Get client's IP address
            String ipAddress = request.getHeader("x-forwarded-for");
            if (ipAddress == null) {
                ipAddress = request.getHeader("X_FORWARDED_FOR");
                if (ipAddress == null) {
                    ipAddress = request.getHeader("X-Forward-For");
                    if (ipAddress == null) {
                        ipAddress = request.getRemoteAddr();
                    }
                }
            }
            if (!plugin.getAllowedIPs().contains(ipAddress)) {
                Log.warn("User service rejected service to IP address: " + ipAddress);
                replyError("RequestNotAuthorized",response, out);
                return;
            }
        }

        String secret = request.getParameter("secret");
        String type = request.getParameter("type");
        //No defaults, add, delete, update only
        //type = type == null ? "image" : type;
       
       // Check that our plugin is enabled.
        if (!plugin.isEnabled()) {
            replyError("WorkgroupServiceDisabled",response, out);
            return;
        }
       
        // Check this request is authorised
        if (secret == null || !secret.equals(plugin.getSecret())){
            Log.warn("An unauthorised user service request was received: " + request.getQueryString());
            replyError("RequestNotAuthorised",response, out);
            return;
        }

        // Check the request type and process accordingly
        // ORDER of this if else is very influential
        try {
        	if (type.contains("report")) {
        		replyMessage(reportModule.executeCommand(request), response, out);
        	} else if (type.contains("channel")) {
        		replyMessage(queueModule.executeCommand(request), response, out);
        	} else if (type.contains("agent")) {
        		replyMessage(agentModule.executeCommand(request), response, out);
        	} else if (type.contains("transport")) {
        		replyMessage(transportModule.executeCommand(request), response, out);
        	} else if (type.contains("setting")) {
        		replyMessage(settingModule.executeCommand(request), response, out);
        	} else if (type.contains("macro")) {
        		replyMessage(macroModule.executeCommand(request), response, out);
        	} else if (type.contains("transcript")) {
        		replyMessage(transcriptModule.executeCommand(request), response, out);
        	} else if (type.startsWith("sms-")) {
        		replyMessage(smsModule.executeCommand(request), response, out);
        	} else {
        		replyMessage(workgroupModule.executeCommand(request), response, out);
        	}
        }
        catch (UserAlreadyExistsException e) {
            replyError("UserAlreadyExistsException",response, out);
        }
        catch (UserNotFoundException e) {
            replyError("UserNotFoundException",response, out);
        }
        catch (IllegalArgumentException e) {
            replyError("IllegalArgumentException",response, out);
        }
        catch (HttpException hex) {
        	replyError(hex.getStatus(), hex.getReason(), response, out);
        }
        catch (Exception e) {
            replyError(e.getMessage(), response, out);
        }
    }

    private void replyMessage(String message,HttpServletResponse response, PrintWriter out) {
    	if (message.contains("\\40")) {
    		message = message.replaceAll("\\\\40", "@");
    	}
    	if (message.contains("\\t")) {
    		message = message.replaceAll("\\t", " ");
    	}
    	if (message.contains("\\n")) {
    		message = message.replaceAll("\\n", " ");
    	}
    	if (message.contains("\\@")) {
    		message = message.replaceAll("\\\\@", "@");
    	}
    	
    	response.setContentType("application/json");
        out.print("{\"status\":200, \"result\":" + message + "}");
        out.flush();
    }

    private void replyError(String error, HttpServletResponse response, PrintWriter out){
        replyError(500, error, response, out);
    }
    
    private void replyError(int status, String error, HttpServletResponse response, PrintWriter out){
        response.setContentType("application/json");
        response.setStatus(status);
        out.print("{\"status\":" + status + ", \"result\":\"" + error + "\"}");
        out.flush();
    }
    
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
	public void destroy() {
    	settingModule.close();
    	workgroupModule.close();
    	agentModule.close();
    	transportModule.close();
    	queueModule.close();
    	reportModule.close();
    	transcriptModule.close();
    	
        super.destroy();
        // Release the excluded URL
        AuthCheckFilter.removeExclude("userService/userservice");
    }
}
