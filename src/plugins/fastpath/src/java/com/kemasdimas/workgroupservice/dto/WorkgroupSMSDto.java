package com.kemasdimas.workgroupservice.dto;

import java.util.Date;

import org.json.simple.JSONAware;

public class WorkgroupSMSDto implements JSONAware {
	protected long id = -1;
	
	protected String fromAddress = "";
	protected String fromName = "";
	protected String toAddress = "";
	
	protected String message = "";
	protected Date timestamp;
	
	protected byte isSent = -1;
	protected byte isRead = -1;
	
	protected String assignTo = "";
	protected String sentBy = "";
	protected long replyFor = -1;
	
	public WorkgroupSMSDto() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public byte getIsSent() {
		return isSent;
	}

	public void setIsSent(byte isSent) {
		this.isSent = isSent;
	}

	public byte getIsRead() {
		return isRead;
	}

	public void setIsRead(byte isRead) {
		this.isRead = isRead;
	}

	public String getSentBy() {
		return sentBy;
	}

	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	public long getReplyFor() {
		return replyFor;
	}

	public void setReplyFor(long replyFor) {
		this.replyFor = replyFor;
	}

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	@Override
	public String toJSONString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("{");
		buffer.append("\"id\":\"" + id + "\"");
		buffer.append(",\"fromAddress\":\"" + fromAddress + "\"");
		
		if (fromName != null) {
			buffer.append(",\"fromName\":\"" + fromName + "\"");
		} else {
			buffer.append(",\"fromName\":null");
		}
		
		buffer.append(",\"toAddress\":\"" + toAddress + "\"");
		buffer.append(",\"message\":\"" + message + "\"");
		buffer.append(",\"timestamp\":" + timestamp.getTime());
		buffer.append(",\"isSent\":" + isSent);
		buffer.append(",\"isRead\":" + isRead);
		
		if (sentBy != null) {
			buffer.append(",\"sentBy\":\"" + sentBy + "\"");
		} else {
			buffer.append(",\"sentBy\":null");
		}
		
		if (assignTo != null) {
			buffer.append(",\"assignTo\":\"" + assignTo + "\"");
		} else {
			buffer.append(",\"assignTo\":null");
		}
		
		buffer.append("}");
		
		return buffer.toString();
	}
}
