package com.kemasdimas.workgroupservice.component;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.http.HttpException;
import org.jivesoftware.openfire.fastpath.macros.Macro;
import org.jivesoftware.openfire.fastpath.macros.MacroGroup;
import org.jivesoftware.openfire.fastpath.macros.WorkgroupMacros;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public final class MacroServiceModule extends BaseWorkgroupServiceModule {
		
	protected static MacroServiceModule instance;
	private WorkgroupMacros workgroupMacro;
	
	protected MacroServiceModule() {
		super();
		
		WorkgroupManager.getInstance();
	}
	
	public static MacroServiceModule getInstance() {
		if (instance == null) {
			instance = new MacroServiceModule();
		}
		
		return (MacroServiceModule) instance;
	}
	
	public String executeCommand(HttpServletRequest request) throws Exception {
		String wgName = request.getParameter("workgroupName");
		String type = request.getParameter("type");
		
		if (wgName == null) {
			throw new Exception("Workgroup name can not be null");
		}
		
		// Make sure the workgroup is loaded, then load the macro
		if (workgroupMacro == null) {
			workgroupMacro = WorkgroupMacros.getInstance();
		}
		
		// This is used to validate workgroup name!
		Workgroup wg = findWorkgroup(wgName);
		
		if ("list-macro-group".equals(type)) {
			return listMacroGroup(wg);
		}
		
		String groupTitle = request.getParameter("groupTitle");
		
		if (groupTitle == null) {
			throw new Exception("groupTitle can not be null");
		}
		
		if ("list-macro".equals(type)) {
			return listMacro(wg, groupTitle);
		} else if ("add-macro-group".equals(type)) {
			String parentGroup = request.getParameter("parentGroup");
			if (parentGroup == null) {
				throw new Exception("parentGroup can not be null");
			}
			
			return addMacroGroup(wg, groupTitle, parentGroup);
		} else if ("edit-macro-group".equals(type)) {
			String newTitle = request.getParameter("newTitle");
			if (newTitle == null) {
				throw new Exception("newTitle can not be null");
			}
			
			return editMacroGroup(wg, groupTitle, newTitle);
		} else if ("delete-macro-group".equals(type)) {
			String parentGroup = request.getParameter("parentGroup");
			if (parentGroup == null) {
				throw new Exception("parentGroup can not be null");
			}
			
			return deleteMacroGroup(wg, groupTitle, parentGroup);
		} else if ("add-macro-response".equals(type)) { 
			String macroTitle = request.getParameter("macroTitle");
			String macroResponse = request.getParameter("macroResponse");
			
			if (macroTitle == null || macroResponse == null) {
				throw new Exception("macroTitle / macroResponse can not be null");
			}
			
			return addMacroResponse(wg, groupTitle, macroTitle, macroResponse);
		} else if ("edit-macro-response".equals(type)) { 
			int macroIndex = Integer.parseInt(request.getParameter("macroIndex"));
			String macroTitle = request.getParameter("macroTitle");
			String macroResponse = request.getParameter("macroResponse");
			
			if (macroTitle == null || macroResponse == null) {
				throw new Exception("macroTitle / macroResponse can not be null");
			}
			
			return editMacroResponse(wg, groupTitle, macroTitle, macroResponse, macroIndex);
		} else if ("delete-macro-response".equals(type)) { 
			int macroIndex = Integer.parseInt(request.getParameter("macroIndex"));
			
			return deleteMacroResponse(wg, groupTitle, macroIndex);
		}
		
		throw new Exception("Type not supported");
	}
	
	private String replaceEnter(String raw) {
		return raw;
	}
	
	private String addMacroResponse(Workgroup wg, String group, String title, String response) throws Exception {
		MacroGroup parentGroup = workgroupMacro.getMacroGroup(wg, group);
		if (parentGroup == null) {
			throw new Exception("Parent group not found");
		}
		
		Macro macro = new Macro();
		macro.setType(Macro.TEXT);
		macro.setTitle(title);
		macro.setResponse(replaceEnter(response));
		parentGroup.addMacro(macro);
		workgroupMacro.saveMacros(wg);
		
		return "\"ok\"";
	}
	
	private String editMacroResponse(Workgroup wg, String group, String title, String response, int index) throws Exception {
		MacroGroup parentGroup = workgroupMacro.getMacroGroup(wg, group);
		if (parentGroup == null) {
			throw new Exception("Parent group not found");
		}
		
		Macro macro = parentGroup.getMacro(index);
		if (macro == null) {
			throw new Exception("Macro not found");
		}
		macro.setTitle(title);
		macro.setResponse(replaceEnter(response));
		workgroupMacro.saveMacros(wg);
		
		return "\"ok\"";
	}
	
	private String deleteMacroResponse(Workgroup wg, String group, int index) throws Exception {
		MacroGroup parentGroup = workgroupMacro.getMacroGroup(wg, group);
		if (parentGroup == null) {
			throw new Exception("Parent group not found");
		}
		
		Macro macro = parentGroup.getMacro(index);
		if (macro == null) {
			throw new Exception("Macro not found");
		}
		parentGroup.removeMacro(macro);
		workgroupMacro.saveMacros(wg);
		
		return "\"ok\"";
	}
	
	private String addMacroGroup(Workgroup wg, String title, String parent) throws Exception {
		MacroGroup parentGroup = workgroupMacro.getMacroGroup(wg, parent);
		MacroGroup existingGroup = workgroupMacro.getMacroGroup(wg, title);
		if (parentGroup == null) {
			throw new Exception("Parent group not found");
		}
		if (existingGroup != null) {
			throw new Exception("Already exists group: " + title);
		}
		
		MacroGroup group = new MacroGroup();
        group.setTitle(title);
        parentGroup.addMacroGroup(group);
        workgroupMacro.saveMacros(wg);
		
		return "\"ok\"";
	}
	
	private String editMacroGroup(Workgroup wg, String oldTitle, String newTitle) throws Exception {
		MacroGroup group = workgroupMacro.getMacroGroup(wg, oldTitle);
		MacroGroup existingGroup = workgroupMacro.getMacroGroup(wg, newTitle);
		if (group == null) {
			throw new Exception("Group not found");
		}
		if (existingGroup != null) {
			throw new Exception("Already exists group: " + newTitle);
		}
		
		group.setTitle(newTitle);
        workgroupMacro.saveMacros(wg);
		
        return "\"ok\"";
	}
	
	private String deleteMacroGroup(Workgroup wg, String title, String parent) throws Exception {
		MacroGroup parentGroup = workgroupMacro.getMacroGroup(wg, parent);
		MacroGroup group = workgroupMacro.getMacroGroup(wg, title);
		
		if (parentGroup == null) {
			throw new Exception("Parent group not found");
		}
		if (group == null) {
			throw new Exception("Group not found");
		}
		
		parentGroup.removeMacroGroup(group);
        workgroupMacro.saveMacros(wg);
		
		return "\"ok\"";
	}
	
	private String listMacro(Workgroup wg, String groupTitle) throws Exception {
		MacroGroup group = workgroupMacro.getMacroGroup(wg, 
				URLDecoder.decode(groupTitle, "UTF-8"));
		
		if (group == null) {
			throw new HttpException(404, "Group not found");
		}
		
		List<Macro> macros = group.getMacros();
		JSONArray jsonArray = new JSONArray();
		int counter = 0;
		for (Macro macro : macros) {
			JSONObject json = new JSONObject();
			
			json.put("title", macro.getTitle());
			json.put("body", macro.getResponse());
			json.put("index", counter++);
			
			jsonArray.add(json);
		}
		
		JSONObject finalJson = new JSONObject();
		finalJson.put("total", jsonArray.size());
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	private String listMacroGroup(Workgroup wg) throws Exception {
		MacroGroup rootGroup = workgroupMacro.getMacroGroup(wg);
		JSONArray jsonArray = traverseWorkgroup(new JSONArray(), rootGroup, 0);
		
	    JSONObject finalJson = new JSONObject();
		finalJson.put("data", jsonArray);
		
		return finalJson.toString();
	}
	
	private JSONArray traverseWorkgroup(JSONArray macroArray, MacroGroup parentGroup, int tab) throws UnsupportedEncodingException {
		List<MacroGroup> macroGroups = parentGroup.getMacroGroups();
		
		for (MacroGroup group : macroGroups) {
			JSONObject innerGroup = new JSONObject();
			innerGroup.put("title", group.getTitle());
			innerGroup.put("tab", tab);
			innerGroup.put("parent", parentGroup.getTitle());
			
			if (!group.getMacroGroups().isEmpty()) {
				innerGroup.put("groups", traverseWorkgroup(new JSONArray(), group, tab + 1));
			}
			
			macroArray.add(innerGroup);
		}
		
		return macroArray;
	}
	
	@Override
	public void close() {
		super.close();
		
		workgroupMacro = null;
	}
	
}
