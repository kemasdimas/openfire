package com.kemasdimas.workgroupservice.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jivesoftware.xmpp.workgroup.utils.FastpathDBManager;

import com.kemasdimas.workgroupservice.dto.WorkgroupUserDTO;

/**
 * This class rely on global database connection
 * @author kemas
 *
 */
public final class WorkgroupAgentManager {
	private final static String LOAD_WORKGROUP_USER = "SELECT name, email, username FROM ofUser u " +
			"WHERE username LIKE ? AND NOT EXISTS (SELECT userId FROM wgLegacyAgent l WHERE u.username = l.userId) " +
			"ORDER BY username ASC LIMIT ? OFFSET ?";
	private final static String COUNT_WORKGROUP_USER = "SELECT COUNT(*) FROM ofUser u " +
			"WHERE username LIKE ? AND NOT EXISTS (SELECT userId FROM wgLegacyAgent l WHERE u.username = l.userId)";
	private final static String GET_AGENT_QUEUE = "SELECT agentID, agentJID, q.name FROM fpAgent a JOIN fpQueueAgent qa " +
			"on a.agentID = qa.objectID JOIN (SELECT * FROM fpQueue WHERE workgroupID = ?) q on qa.queueID = q.queueID " +
			"ORDER BY agentID ASC;";
	
	private static WorkgroupAgentManager instance;
	
	public static WorkgroupAgentManager getInstance() {
		if (instance == null) {
			instance = new WorkgroupAgentManager();
		}
		
		return instance;
	}
	
	public int countUserInWorkgroup(String wgName) {
		int counter = 0;
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection();
			statement = con.prepareStatement(COUNT_WORKGROUP_USER);
			statement.setString(1, "%-" + wgName);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				counter = result.getInt(1);
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return counter;
	}
	
	public ArrayList<WorkgroupUserDTO> fetchUserInWorkgroup(String wgName, int limit, int offset) {
		ArrayList<WorkgroupUserDTO> buffer = new ArrayList<WorkgroupUserDTO>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection();
			statement = con.prepareStatement(LOAD_WORKGROUP_USER);
			statement.setString(1, "%-" + wgName);
			statement.setInt(2, limit);
			statement.setInt(3, offset);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				buffer.add(new WorkgroupUserDTO(
						result.getString(1), 
						result.getString(2), 
						result.getString(3)));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public Map<Long, ArrayList<String>> fetchAgentQueue(long workgroupId, String wgName) {
		Map<Long, ArrayList<String>> buffer = new HashMap<Long, ArrayList<String>>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = FastpathDBManager.getConnection(wgName);
			statement = con.prepareStatement(GET_AGENT_QUEUE);
			statement.setLong(1, workgroupId);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				Long id = result.getLong("agentID");
				
				if (buffer.get(id) == null) {
					buffer.put(id, new ArrayList<String>());
				}
				
				buffer.get(id).add(result.getString("name"));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			FastpathDBManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
}
