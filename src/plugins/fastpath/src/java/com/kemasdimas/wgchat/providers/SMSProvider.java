package com.kemasdimas.wgchat.providers;

import java.util.List;

import org.dom4j.Element;
import org.jivesoftware.xmpp.workgroup.AgentNotFoundException;
import org.jivesoftware.xmpp.workgroup.AgentSession;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupProvider;
import org.xmpp.packet.IQ;
import org.xmpp.packet.PacketError;

import com.kemasdimas.workgroupservice.dto.WorkgroupSMSCompositeDTO;
import com.kemasdimas.workgroupservice.dto.WorkgroupSMSDto;
import com.kemasdimas.workgroupservice.manager.WorkgroupSmsManager;

public class SMSProvider extends BaseWgchatIqProvider implements WorkgroupProvider {

	public static final String GET_EXECUTE_KEYS[] = new String[] { "get-sms-reg-number", "get-sms-customer", "get-reg-number", "get-sms-reply" };
	public static final String SET_EXECUTE_KEYS[] = new String[] { "save-sms", "send-sms", "assign-sms", "read-sms" };
	
	@Override
	public boolean handleGet(IQ packet) {
		return hasMessageInitiationNames(packet, GET_EXECUTE_KEYS);
	}

	@Override
	public boolean handleSet(IQ packet) {
		return hasMessageInitiationNames(packet, SET_EXECUTE_KEYS);
	}

	private void saveIncomingSms(IQ packet, Workgroup workgroup) {
		IQ reply = IQ.createResultIQ(packet);
        Element iq = packet.getChildElement();
        
        String fromAddress = iq.attributeValue("from-address");
        String fromName = iq.attributeValue("from-name");
        String toAddress = iq.attributeValue("to-address");
        
        String message = iq.elementText("message");
        
        WorkgroupSmsManager.getInstance().saveIncomingSms(workgroup, fromAddress, fromName, toAddress, message);
        
        workgroup.send(reply);
	}
	
	private void sendOutgoingSms(IQ packet, Workgroup workgroup, String sentBy) {
		IQ reply = IQ.createResultIQ(packet);
        Element iq = packet.getChildElement();
        
        String fromAddress = iq.attributeValue("from-address");
        String toAddress = iq.attributeValue("to-address");
        long replyFor = Long.parseLong(iq.attributeValue("reply-for"));
        
        String message = iq.elementText("message");
        
        WorkgroupSmsManager.getInstance().saveOutgoingSms(workgroup, fromAddress, toAddress, message, sentBy, replyFor);
        
        // TODO: Send this new message to phone!
        
        
        workgroup.send(reply);
	}
	
	private void assignSmsTo(IQ packet, Workgroup workgroup, final String assignTo) {
		IQ reply = IQ.createResultIQ(packet);
        Element iq = packet.getChildElement();
        
        long smsId = 0;
        try {
			smsId = Long.parseLong(iq.attributeValue("sms-id"));
		} catch (Exception e) { }
        
        WorkgroupSmsManager.getInstance().updateSmsAssignTo(workgroup, smsId, assignTo);
        
        workgroup.send(reply);
	}
	
	private void markSmsRead(IQ packet, Workgroup workgroup) {
		IQ reply = IQ.createResultIQ(packet);
        Element iq = packet.getChildElement();
        
        byte isRead = 1;
        List<Element> elements = iq.elements("sms");
        
        long smsIds[] = new long[elements.size()];
        int counter = 0;
        for (Element element : elements) {
			long smsId = Long.parseLong(element.attributeValue("sms-id"));
			
			smsIds[counter] = smsId;
			counter++;
		}
        
        WorkgroupSmsManager.getInstance().updateSmsRead(workgroup, smsIds, isRead);
        
        workgroup.send(reply);
	}
	
	private void findSmsReplyForId(IQ packet, Workgroup workgroup) {
		IQ reply = IQ.createResultIQ(packet);
		Element iq = packet.getChildElement();
		
		long incomingSmsId = -1;
		try {
			incomingSmsId = Long.parseLong(iq.attributeValue("incoming-id"));
		} catch (Exception e) { }
		
		WorkgroupSMSDto replySms = WorkgroupSmsManager.getInstance().getReplyForId(workgroup, incomingSmsId);
		
		if (replySms != null) {
			Element iqSms = reply.setChildElement("reply-sms", NAMESPACE_WGCHAT);

			iqSms.addAttribute("timestamp", String.valueOf(replySms.getTimestamp().getTime()));
			iqSms.addAttribute("sent-by", replySms.getSentBy());
			
			iqSms.addElement("message").setText(replySms.getMessage());
		} else {
			// Send ERROR if not SMS reply existed
	        reply.setChildElement(packet.getChildElement().createCopy());
	        reply.setError(new PacketError(PacketError.Condition.item_not_found));
		}
        
        workgroup.send(reply);
	}
	
	private void listRegisteredNumber(IQ packet, Workgroup workgroup) {
		IQ reply = IQ.createResultIQ(packet);
		Element iq = packet.getChildElement();
		
		List<String> numbersList = WorkgroupSmsManager.getInstance().getRegisteredSmsNumbers(workgroup);
		
		Element iqSmsList = reply.setChildElement("number-list", NAMESPACE_WGCHAT);
		iqSmsList.addAttribute("total", String.valueOf(numbersList.size()));
		
		for (String number : numbersList) {
			iqSmsList.addElement("number").setText(number);
		}
		
		workgroup.send(reply);
	}
	
	private void listCustomerNumberSmses(IQ packet, Workgroup workgroup) {
		IQ reply = IQ.createResultIQ(packet);
		Element iq = packet.getChildElement();
		
		String registeredNumber = iq.attributeValue("registered-number");
		String customerNumber	= iq.attributeValue("customer-number");
		
		List<WorkgroupSMSDto> smsesList = WorkgroupSmsManager.getInstance().getSmsByCustomerNumber(workgroup, registeredNumber, customerNumber);
		
		Element iqSmsList = reply.setChildElement("sms-list", NAMESPACE_WGCHAT);
		iqSmsList.addAttribute("total", String.valueOf(smsesList.size()));
		
		for (WorkgroupSMSDto sms : smsesList) {
			Element iqSms = iqSmsList.addElement("sms");

			iqSms.addAttribute("id", String.valueOf(sms.getId()));
			iqSms.addAttribute("is-read", String.valueOf(sms.getIsRead()));
			iqSms.addAttribute("is-sent", String.valueOf(sms.getIsSent()));
			iqSms.addAttribute("timestamp", String.valueOf(sms.getTimestamp().getTime()));

			iqSms.addAttribute("from-address", String.valueOf(sms.getFromAddress()));
			iqSms.addAttribute("from-name", String.valueOf(sms.getFromName()));
			iqSms.addAttribute("to-address", String.valueOf(sms.getToAddress()));
			
			iqSms.addElement("message").setText(sms.getMessage());
			
			if (sms.getIsSent() != -1) {
				iqSms.addAttribute("sent-by", sms.getSentBy());
			}
			
			String assignTo = sms.getAssignTo();
			if (assignTo != null && assignTo.trim().length() > 0) {
				iqSms.addAttribute("assign-to", sms.getAssignTo());
			}
		}
		
		workgroup.send(reply);
	}
	
	private void listRegisteredNumberSmses(IQ packet, Workgroup workgroup) {
		IQ reply = IQ.createResultIQ(packet);
		Element iq = packet.getChildElement();
		
		String registeredNumber = iq.attributeValue("registered-number");
		long limit = 50;
		long offset = 0;
		
		try {
			limit = Long.parseLong(iq.attributeValue("limit"));
		} catch (Exception e) { }
		
		try {
			offset = Long.parseLong(iq.attributeValue("offset"));
		} catch (Exception e) { }
		
		long prevOffset = offset - limit;
		if (prevOffset < 0) {
			prevOffset = 0;
		}
		
		long totalSmses = WorkgroupSmsManager.getInstance().countSmsForRegisteredNumber(workgroup, registeredNumber);
		List<WorkgroupSMSCompositeDTO> smsesList = WorkgroupSmsManager.getInstance().getIncomingSmsByRegisteredNumber(workgroup, registeredNumber, limit, offset);
		
		Element iqSmsList = reply.setChildElement("sms-list", NAMESPACE_WGCHAT);
		iqSmsList.addAttribute("total", String.valueOf(totalSmses));
		iqSmsList.addAttribute("limit", String.valueOf(limit));
		iqSmsList.addAttribute("next-offset", String.valueOf(offset + limit));
		iqSmsList.addAttribute("prev-offset", String.valueOf(prevOffset));
		
		for (WorkgroupSMSCompositeDTO sms : smsesList) {
			Element iqSms = iqSmsList.addElement("sms");
			
			iqSms.addElement("message").setText(sms.getMessage());
			
			iqSms.addAttribute("id", String.valueOf(sms.getId()));
			iqSms.addAttribute("is-read", String.valueOf(sms.getIsRead()));
			iqSms.addAttribute("timestamp", String.valueOf(sms.getTimestamp().getTime()));
			
			iqSms.addAttribute("from-address", String.valueOf(sms.getFromAddress()));
			iqSms.addAttribute("from-name", String.valueOf(sms.getFromName()));
			iqSms.addAttribute("to-address", String.valueOf(sms.getToAddress()));
			
			if (sms.getReplySms() != null) {
				Element iqReplySms = iqSms.addElement("reply-sms");
				
				iqReplySms.addElement("message").setText(sms.getReplySms().getMessage());
				
				iqReplySms.addAttribute("timestamp", String.valueOf(sms.getReplySms().getTimestamp().getTime()));
				iqReplySms.addAttribute("is-sent", String.valueOf(sms.getReplySms().getIsSent()));
				iqReplySms.addAttribute("reply-by", sms.getReplySms().getSentBy());
			}
		}
		
		workgroup.send(reply);
	}
	
	@Override
	public void executeGet(IQ packet, Workgroup workgroup) {
		IQ reply;
		Element iq = packet.getChildElement();
		String name = iq.getName();
		
		try {
			AgentSession agentSession = workgroup.getAgentManager().getAgentSession(packet.getFrom());
			if (agentSession != null) {
				if ("get-sms-reg-number".equalsIgnoreCase(name)) {
					listRegisteredNumberSmses(packet, workgroup);
				} else if ("get-sms-customer".equalsIgnoreCase(name)) {
					listCustomerNumberSmses(packet, workgroup);
				} else if ("get-reg-number".equalsIgnoreCase(name)) {
					listRegisteredNumber(packet, workgroup);
				} else if ("get-sms-reply".equalsIgnoreCase(name)) {
					findSmsReplyForId(packet, workgroup);
				}
			} else {
				throw new AgentNotFoundException();
			}
		} catch (AgentNotFoundException e) {
			reply = IQ.createResultIQ(packet);
            reply.setChildElement(packet.getChildElement().createCopy());
            reply.setError(new PacketError(PacketError.Condition.item_not_found));
            workgroup.send(reply);
		}
	}

	@Override
	public void executeSet(IQ packet, Workgroup workgroup) {
		IQ reply;
		Element iq = packet.getChildElement();
		String name = iq.getName();
		
		try {
			if (name.equalsIgnoreCase("save-sms")) {
				saveIncomingSms(packet, workgroup);
			} else {
				try {
					AgentSession agentSession = workgroup.getAgentManager().getAgentSession(packet.getFrom());
					if (agentSession != null) {
						
						if (name.equalsIgnoreCase("send-sms")) {
							sendOutgoingSms(packet, workgroup, agentSession.getJID().getNode());
						} else if (name.equalsIgnoreCase("assign-sms")) {
							assignSmsTo(packet, workgroup, agentSession.getJID().getNode());
						} else if (name.equalsIgnoreCase("read-sms")) {
							markSmsRead(packet, workgroup);
						}
						
					} else {
						throw new AgentNotFoundException();
					}
				} catch (AgentNotFoundException e) {
					reply = IQ.createResultIQ(packet);
		            reply.setChildElement(packet.getChildElement().createCopy());
		            reply.setError(new PacketError(PacketError.Condition.item_not_found));
		            workgroup.send(reply);
				}
			}
		} catch (Exception e) { }
	}

	private boolean hasMessageInitiationNames(IQ packet, String keys[]) {
        Element iq = packet.getChildElement();
        String name = iq.getName();
        
        for (String key : keys) {
        	if (key.equals(name)) {
        		return true;
        	}
		}
        
        return false;
    }
}
