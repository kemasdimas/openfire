package com.kemasdimas.wgchat.packet;

import org.dom4j.Element;
import org.xmpp.packet.IQ;

public class SlotNotification extends IQ {

	public SlotNotification(int maxChat) {
		super();
		
		setType(IQ.Type.result);
		
		Element element = this.setChildElement("slot-notification", "http://wgchat.com/providers");
		element.addAttribute("maxchats", maxChat + "");
	}
}
