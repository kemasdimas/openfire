package com.kemasdimas.wgchat.providers;

import java.io.UnsupportedEncodingException;
import java.util.List;

import net.sf.kraken.BaseTransport;
import net.sf.kraken.KrakenPlugin;
import net.sf.kraken.TransportInstance;
import net.sf.kraken.session.TransportSession;

import org.dom4j.Element;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSettings;
import org.jivesoftware.openfire.fastpath.settings.chat.ChatSettingsManager;
import org.jivesoftware.openfire.fastpath.settings.chat.KeyEnum;
import org.jivesoftware.xmpp.workgroup.AgentNotFoundException;
import org.jivesoftware.xmpp.workgroup.AgentSession;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupProvider;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.Message;
import org.xmpp.packet.PacketError;

import com.kemasdimas.workgroupservice.dto.WorkgroupTransportDTO;
import com.kemasdimas.workgroupservice.manager.WorkgroupTransportManager;

public class MessageInitiationProvider extends BaseWgchatIqProvider implements WorkgroupProvider {

	private static String serverName = ComponentManagerFactory.getComponentManager().getServerName();
	
	@Override
	public boolean handleGet(IQ packet) {
		return hasMessageInitiationNames(packet, "online-im-list");
	}

	@Override
	public boolean handleSet(IQ packet) {
		return hasMessageInitiationNames(packet, "initiate-message");
	}

	private void sendOneWayMessage(IQ packet, Workgroup workgroup, AgentSession session) throws UnsupportedEncodingException {
		Element iq = packet.getChildElement();
		
		String message = iq.attributeValue("message");
		String transportType = iq.attributeValue("transportType");
		String destinationID = iq.attributeValue("destinationID").replace("@", "\\40") + "@" + transportType + "." + serverName;
		String handler = iq.attributeValue("handler");
		
		Message msg = new Message();
		msg.setType(Message.Type.chat);
		msg.setFrom(workgroup.getJID());
		msg.setTo(XMPPServer.getInstance().createJID(handler, destinationID));
		msg.setBody(message);
		
		Message autoMsg = msg.createCopy();
		
		workgroup.send(msg);
		
		// Sending automessage after initiation message, to indicate one way message to customer.
		ChatSettings settings = ChatSettingsManager.getInstance().getChatSettings(workgroup);
		String autoOneWay = settings.getChatSetting(KeyEnum.one_way_message_info).getValue();
		if (autoOneWay != null && autoOneWay.trim().length() > 0) {
			autoMsg.setBody(autoOneWay);
			workgroup.send(autoMsg);
		}
	}
	
	private void sendOnlineIMPacket(IQ packet, Workgroup workgroup, AgentSession session) {
		IQ reply = IQ.createResultIQ(packet);
		
		// TODO: Query online IM list for this agent
		int totalImAccounts = WorkgroupTransportManager.getInstance().
				countTransportAgent(workgroup.getID());
		List<WorkgroupTransportDTO> imAccounts = WorkgroupTransportManager.getInstance().
				fetchTransportInWorkgroup(workgroup.getID(), totalImAccounts, 0);
		
		Element imList = reply.setChildElement("online-im-list", NAMESPACE_WGCHAT);
		
		// Use the kraken plugin to check online status
		final KrakenPlugin plugin =
	            (KrakenPlugin) XMPPServer.getInstance().getPluginManager().getPlugin("kraken");
		
		// Building the list
		for (WorkgroupTransportDTO transport : imAccounts) {
			String handler = transport.getUserId();
			boolean isLoggedIn = false;
			
			try {
				TransportInstance transportInstance = plugin.getTransportInstance(transport.getTransportType());
				BaseTransport baseTransport = transportInstance.getTransport();
				TransportSession sess = baseTransport.getSessionManager().getSession(transport.getUserId());
				
				isLoggedIn = sess.isLoggedIn();
			} catch (Exception e) {
			}
			
			// Checking online im account
			if (isLoggedIn) {
				Element account = imList.addElement("im-account");
				
				account.addElement("username").setText(transport.getTransportUserName());
				account.addElement("type").setText(transport.getTransportType());
				account.addElement("handler").setText(handler);
			}
		}
		
		workgroup.send(reply);
	}
	
	@Override
	public void executeGet(IQ packet, Workgroup workgroup) {
		IQ reply;
		Element iq = packet.getChildElement();
		
		try {
			AgentSession agentSession = workgroup.getAgentManager().getAgentSession(packet.getFrom());
			if (agentSession != null) {
				sendOnlineIMPacket(packet, workgroup, agentSession);
			} else {
				throw new AgentNotFoundException();
			}
		} catch (AgentNotFoundException e) {
			reply = IQ.createResultIQ(packet);
            reply.setChildElement(packet.getChildElement().createCopy());
            reply.setError(new PacketError(PacketError.Condition.item_not_found));
            workgroup.send(reply);
		}
	}

	@Override
	public void executeSet(IQ packet, Workgroup workgroup) {
		IQ reply;
		Element iq = packet.getChildElement();
		
		try {
			AgentSession agentSession = workgroup.getAgentManager().getAgentSession(packet.getFrom());
			if (agentSession != null) {
				sendOneWayMessage(packet, workgroup, agentSession);
			} else {
				throw new AgentNotFoundException();
			}
		} catch (AgentNotFoundException e) {
			reply = IQ.createResultIQ(packet);
            reply.setChildElement(packet.getChildElement().createCopy());
            reply.setError(new PacketError(PacketError.Condition.item_not_found));
            workgroup.send(reply);
		} catch (UnsupportedEncodingException e) {
		}
	}

	private boolean hasMessageInitiationNames(IQ packet, String key) {
        Element iq = packet.getChildElement();
        String name = iq.getName();

        return key.equals(name);
    }
}
