package com.kemasdimas.wgchat.providers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;
import org.jivesoftware.openfire.fastpath.macros.Macro;
import org.jivesoftware.openfire.fastpath.macros.MacroGroup;
import org.jivesoftware.openfire.fastpath.macros.WorkgroupMacros;
import org.jivesoftware.xmpp.workgroup.AgentNotFoundException;
import org.jivesoftware.xmpp.workgroup.AgentSession;
import org.jivesoftware.xmpp.workgroup.Workgroup;
import org.jivesoftware.xmpp.workgroup.WorkgroupProvider;
import org.jivesoftware.xmpp.workgroup.request.UserRequest;
import org.xmpp.packet.IQ;
import org.xmpp.packet.PacketError;

public class TranscriptTagProvider extends BaseWgchatIqProvider implements WorkgroupProvider {

	@Override
	public boolean handleGet(IQ packet) {
		return hasMessageInitiationNames(packet, "list-tags");
	}

	@Override
	public boolean handleSet(IQ packet) {
		return hasMessageInitiationNames(packet, "save-tags");
	}
	
	private void clearTags(UserRequest request) {
    	boolean tagFound = false;
    	Iterator<String> iterator = request.getMetaData().keySet().iterator();
    	ArrayList<String> storedTagsKey = new ArrayList<String>();
    	
    	while (iterator.hasNext()) {
    		String key = iterator.next();
    		
    		if (key.contains("tag#")) {
    			storedTagsKey.add(key);
    			tagFound = true;
    		}
    	}
    	
    	for (String key : storedTagsKey) {
			request.getMetaData().remove(key);
		}
    	
    	if (tagFound) {
    		request.saveMetadata();
    	}
    }
    
    /**
     * Used to save tags to the database
     * e.g {#tags1} {#tags2}
     * 
     * @param body
     * @param request
     */
    private void saveTags(String body, UserRequest request) {
    	String[] tags = body.split("((\\{#))|}");
    	List<String> buffer;
    	
    	int counter = 0;
    	String trimmed = "";
    	
    	// Clearing tags if it is existed
    	if (tags.length > 0) {
    		clearTags(request);
    	}
    	
    	for (String tag : tags) {
			trimmed = tag.trim();
			
    		if (trimmed.length() > 0) {
    			buffer = new ArrayList<String>();
    			buffer.add(trimmed);
    			
    			counter++;
        		request.getMetaData().put("tag#" + counter, buffer);
			}
		}
    	
    	if (counter > 0) {
    		request.saveMetadata();
    	}
    }
	
	private void saveTags(IQ packet, Workgroup workgroup, AgentSession session) {
		IQ reply = IQ.createResultIQ(packet);
		Element iq = packet.getChildElement();
		String sessionId = iq.attributeValue("sessionID");
		String tags = iq.attributeValue("tags");
		
		UserRequest request = workgroup.getUserRequest(sessionId);
		saveTags(tags, request);
		
		// Send saved category state, just to make sure
		sendWorkgroupTags(packet, workgroup, session);
	}
	
	private void sendWorkgroupTags(IQ packet, Workgroup workgroup, AgentSession session) {
		IQ reply = IQ.createResultIQ(packet);
		Element iq = packet.getChildElement();
		String sessionId = iq.attributeValue("sessionID");
		
		Element tagsList = reply.setChildElement("list-tags", NAMESPACE_WGCHAT);
		
		// Get workgroup macro for tags, could possibly be 'tags' or 'Tags'
		MacroGroup group = WorkgroupMacros.getInstance().getMacroGroup(workgroup, "Tags");
		if (group == null) {
			group = WorkgroupMacros.getInstance().getMacroGroup(workgroup, "tags");
		}
		
		// If tags exists, proceed
		UserRequest request = workgroup.getUserRequest(sessionId);
		if (group != null && request != null) {
			
			// Iterate the tags in metadata
			Iterator<String> metadataKeys = request.getMetaData().keySet().iterator();
			String buffer = "";
			while (metadataKeys.hasNext()) {
				String key = metadataKeys.next();
				
				if (key.contains("tag#")) {
					buffer += "{#" + request.getMetaData().get(key).get(0) + "} ";
				}
			}
			
			for (Macro macro : group.getMacros()) {
				String body = macro.getResponse();
				Element elMacro = tagsList.addElement("tag");
				
				elMacro.addElement("title").setText(macro.getTitle());
				elMacro.addElement("body").setText(body);
				
				// Check wheteher the tags has already inside the metadata
				elMacro.addElement("isChecked").setText(buffer.contains(body) ? "true" : "false");
			}
		}
		
		workgroup.send(reply);
	}
	
	@Override
	public void executeGet(IQ packet, Workgroup workgroup) {
		IQ reply;
		Element iq = packet.getChildElement();
		
		try {
			AgentSession agentSession = workgroup.getAgentManager().getAgentSession(packet.getFrom());
			if (agentSession != null) {
				sendWorkgroupTags(packet, workgroup, agentSession);
			} else {
				throw new AgentNotFoundException();
			}
		} catch (AgentNotFoundException e) {
			reply = IQ.createResultIQ(packet);
            reply.setChildElement(packet.getChildElement().createCopy());
            reply.setError(new PacketError(PacketError.Condition.item_not_found));
            workgroup.send(reply);
		}
	}

	@Override
	public void executeSet(IQ packet, Workgroup workgroup) {
		IQ reply;
		Element iq = packet.getChildElement();
		
		try {
			AgentSession agentSession = workgroup.getAgentManager().getAgentSession(packet.getFrom());
			if (agentSession != null) {
				saveTags(packet, workgroup, agentSession);
			} else {
				throw new AgentNotFoundException();
			}
		} catch (AgentNotFoundException e) {
			reply = IQ.createResultIQ(packet);
            reply.setChildElement(packet.getChildElement().createCopy());
            reply.setError(new PacketError(PacketError.Condition.item_not_found));
            workgroup.send(reply);
		}
	}

	private boolean hasMessageInitiationNames(IQ packet, String key) {
        Element iq = packet.getChildElement();
        String name = iq.getName();

        return key.equals(name);
    }
}
