INSERT INTO ofVersion (name, version) VALUES ('fastpath', 1);

CREATE TABLE `wgDatabaseMap` (
  `id` int(11) NOT NULL auto_increment,
  `workgroup_name` varchar(160) character set utf8 NOT NULL,
  `database_host` varchar(50) character set utf8 NOT NULL,
  `database_host_schema` varchar(160) character set utf8 NOT NULL,
  `database_username` varchar(160) NOT NULL,
  `database_password` varchar(160) NOT NULL,
  `workgroup_host_ip` varchar(50) default NULL,
  `workgroup_host_name` varchar(160) default NULL,
  `timestamp` timestamp NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY UNIQUE_NAME (workgroup_name),
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1 COMMENT='Database mapping for each workgroup (multidb)';

ALTER TABLE fpChatSetting ADD COLUMN queueID INT(11) NULL AFTER workgroupNode;
ALTER TABLE fpChatSetting CHANGE COLUMN queueID queueID INT(11) NOT NULL DEFAULT -1;
ALTER TABLE fpChatSetting ADD UNIQUE INDEX uniquesetting (workgroupNode ASC, queueID ASC, name ASC) ;

CREATE TABLE `fpChatbotRequest` (
  `customerID` varchar(160) NOT NULL,
  `startTime` char(15) NOT NULL,
  `workgroupID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
