INSERT INTO ofVersion (name, version) VALUES ('fastpath', 2);

CREATE TABLE `fpSmsStorage` (
  `id` bigint(30) NOT NULL AUTO_INCREMENT,
  `workgroup_id` bigint(20) NOT NULL,
  `from_address` varchar(35) NOT NULL,
  `address_name` varchar(70) DEFAULT NULL,
  `to_address` varchar(35) NOT NULL,
  `message` varchar(255) NOT NULL,
  `timestamp_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sent` tinyint(1) NOT NULL DEFAULT '-1',
  `is_read` tinyint(1) NOT NULL DEFAULT '-1',
  `sent_by` varchar(64) DEFAULT NULL,
  `reply_for` bigint(30) DEFAULT NULL,
  `assign_to` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB

/* TEST DATA */
/*
-- Query: SELECT * FROM fpath_workgroup.fpSmsStorage
LIMIT 0, 1000

-- Date: 2013-01-09 13:53
*/
/*
INSERT INTO `fpSmsStorage` (`id`,`workgroup_id`,`from_address`,`address_name`,`to_address`,`message`,`timestamp_at`,`sent`,`is_read`,`sent_by`,`reply_for`) VALUES (1,7,'123','Kemas','456','Halo','2013-01-09 13:50:37',-1,0,NULL,NULL);
INSERT INTO `fpSmsStorage` (`id`,`workgroup_id`,`from_address`,`address_name`,`to_address`,`message`,`timestamp_at`,`sent`,`is_read`,`sent_by`,`reply_for`) VALUES (2,7,'123','Kemas','456','Gimana?','2013-01-09 13:51:31',-1,0,NULL,NULL);
INSERT INTO `fpSmsStorage` (`id`,`workgroup_id`,`from_address`,`address_name`,`to_address`,`message`,`timestamp_at`,`sent`,`is_read`,`sent_by`,`reply_for`) VALUES (3,7,'456',NULL,'123','Baik Pak','2013-01-09 13:52:31',0,-1,'kemas-wgchat',2);
INSERT INTO `fpSmsStorage` (`id`,`workgroup_id`,`from_address`,`address_name`,`to_address`,`message`,`timestamp_at`,`sent`,`is_read`,`sent_by`,`reply_for`) VALUES (4,7,'656','Dani','456','Haaaalal','2013-01-09 13:53:06',-1,0,NULL,NULL);
*/