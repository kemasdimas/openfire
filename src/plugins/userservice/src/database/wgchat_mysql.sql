INSERT INTO ofVersion (name, version) VALUES ('userservice', 0);

CREATE TABLE `wgLegacyAgent` (
  `userId` varchar(100) NOT NULL,
  `workgroupId` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `uniquePair` (`workgroupId`,`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Mapping for legacy transport handler to a workgroup.';

CREATE TABLE `wgLoginCache` (
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;