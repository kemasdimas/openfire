package org.jivesoftware.openfire.plugin.userService;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.muc.MUCRoom;
import org.jivesoftware.openfire.muc.MUCUser;
import org.jivesoftware.openfire.muc.MultiUserChatManager;
import org.jivesoftware.openfire.muc.MultiUserChatService;
import org.jivesoftware.openfire.muc.spi.LocalMUCUser;
import org.jivesoftware.openfire.user.UserManager;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Message.Type;
import org.xmpp.packet.Packet;
import org.xmpp.packet.PacketExtension;
import org.xmpp.packet.Presence;

import com.kemasdimas.bots.BotzConnection;
import com.kemasdimas.bots.BotzPacketReceiver;

public class UserServiceRobot implements BotzPacketReceiver {

	static Logger Log = Logger.getLogger(UserServiceRobot.class);
	
	private BotzConnection bot;
	private MultiUserChatManager chatManager;
	private UserManager userManager;
	private HashMap<String, JID> roomMapping;
	private final static String workgroupCommand = "!help,!back,!bye,!current,!reset";
	private String hostname = "";
	
	public void initialize(BotzConnection bot) {
		this.bot = bot;
		roomMapping = new HashMap<String, JID>();
		chatManager = XMPPServer.getInstance().getMultiUserChatManager();
		userManager = XMPPServer.getInstance().getUserManager();
		hostname = ComponentManagerFactory.getComponentManager().getServerName();
	}

	/**
	 * Process fastpath invitation from workgroup chatbot, enabling the  kraken account for associated
	 * legacy account to enter the room on behalf of the legacy account.
	 * 
	 * @param invitationPacket
	 */
	private void processFastpathInvitation(Packet invitationPacket) {
		
		// Processing invitation to conference, this request should be handled so that Bot
		// is entering the chat room, behalf of the user.
		try {
			MultiUserChatService service = chatManager.getMultiUserChatService("conference");
			MUCRoom room = service.getChatRoom(((Message) invitationPacket).getFrom().getNode());
			String[] realAddress = invitationPacket.getTo().getResource().split("@");
			
			// userJid represent the Bot, with the real username address attached to resource
			JID userJid = new JID(bot.getUsername(), hostname, invitationPacket.getTo().getResource());
			MUCUser mucUser = service.getChatUser(userJid, room.getName());
			
			// Setting real user presence on a room
			Presence p = new Presence();
			p.setPriority(1);
			p.setFrom(userJid);
			p.setTo(new JID(((Message) invitationPacket).getFrom().getNode(), 
					((Message) invitationPacket).getFrom().getDomain(), bot.getUsername()));
			
			room.joinRoom(realAddress[0].replace("\\40", "@"), null, null, (LocalMUCUser) mucUser, p);
			
			// Add this user to the list
			roomMapping.put(invitationPacket.getTo().getResource(), room.getJID());
			
			realAddress = null;
			service = null;
			room = null;
			mucUser = null;
		} catch (Exception e) {
		}
	}
	
	/**
	 * Destroy a room and remove the associated user from roomMapping
	 * 
	 * @param realUser
	 */
	private void leaveRoom(String realUser) {
		JID roomJid = roomMapping.get(realUser);
		if (roomJid  == null) {
			return;
		}
		
		MultiUserChatService service = chatManager.getMultiUserChatService("conference");
		MUCRoom room = service.getChatRoom(roomJid.getNode());
		
		// Destroy the room that realUser currently in.
		try {			
			JID userJid = new JID(bot.getUsername(), hostname, realUser);
			
			Presence p = new Presence();
			p.setPriority(1);
			p.setFrom(userJid);
			p.setTo(roomJid);
			p.setType(Presence.Type.unavailable);
			
			service.processPacket(p);
			
			roomMapping.remove(realUser);
		} catch (Exception e) {
		}
		
		service = null;
		room = null;
		roomJid = null;
	}
	
	/**
	 * Process group message received from agent conference room
	 * and send the message to real user using the legacy transport
	 * 
	 * @param message
	 */
	private void processGroupMessage(Message message) {
		
		// Group Message received, check whether the message contains tag / repeated / none of them
		if (message.getExtension("repeated", "http://wgchat.com/repeated") == null && !message.getBody().contains("{#")) {	
			
			// Message sent from workgroup agent, should process with the specific command.
			String[] realAddress = message.getTo().getResource().split("@");
			
			Message m = new Message();
	        m.setType(Type.chat);
	        m.setFrom(new JID(bot.getUsername(), hostname, null));
	        m.setTo(new JID(realAddress[0], realAddress[1], null));
	        m.setBody(message.getBody());
	        
	        bot.sendPacket(m);
		}
	}
	
	/**
	 * Process chat message sent from / to legacy transport
	 * 
	 * @param message
	 */
	private void processChatMessage(Message message) {
		
		// Message received
		if (message.getFrom().getDomain().contains("workgroup.")) {
			String[] realAddress = message.getTo().getResource().split("@");
			
			if (realAddress[0].equalsIgnoreCase("null")) {
				return;
			}
			
			if (message.getExtension("session-end", "http://wgchat.com/") != null) {
				leaveRoom(message.getTo().getResource());
			}
			
			Message m = new Message();
	        m.setType(message.getType());
	        m.setFrom(message.getTo().toBareJID());
	        m.setTo(new JID(realAddress[0], realAddress[1], null));
	        m.setBody(message.getBody());
	        
	        bot.sendPacket(m);
		} else {
			// Check whether the user IM account is null / not
			if (message.getFrom().getNode().equalsIgnoreCase("null")) {
				return;
			}
			
			// Message came from the user
			String realUser = message.getFrom().getNode() + '@' + message.getFrom().getDomain();
			JID mockJid = new JID(message.getTo().getNode(), message.getTo().getDomain(), realUser);
			Message m = new Message();
			
			if (roomMapping.containsKey(realUser) && !message.getBody().equalsIgnoreCase("!bye")) {

				// User has been in the room, send message to the room						
				m.setType(Message.Type.groupchat);
				m.setTo(roomMapping.get(realUser));
				m.addExtension(new PacketExtension("repeated", "http://wgchat.com/repeated"));
			} else {
				
				// User hasn't joined a room
				// Only process !bye command, ignore another command
		        m.setType(message.getType());
		        String workgroupNode = UserServiceDB.getInstance().getWorkgroupName(message.getTo().getNode());
				m.setTo(new JID(workgroupNode, "workgroup." + hostname, null));
			}
			
			// Process !bye command here..
	        if ((roomMapping.containsKey(realUser) && message.getBody().equalsIgnoreCase("!bye"))) {
	        	leaveRoom(realUser);
	        }
			
			m.setFrom(mockJid);
	        m.setBody(message.getBody());
			bot.sendPacket(m);
		}
	}
	
	public void processIncoming(Packet packet) {
		if (packet instanceof Message) {
			if (((Message) packet).getChildElement("x", "http://jabber.org/protocol/muc#user") != null) {
				processFastpathInvitation(packet);
			} else if (((Message) packet).getType() == Message.Type.groupchat) { 
				processGroupMessage((Message) packet);
			} else if (((Message) packet).getBody() != null && ((Message) packet).getType() != Message.Type.error) {
				processChatMessage((Message) packet);
			} else {
				Log.debug("Invalid message type: " + packet.toXML());
			}
		} else if (packet instanceof Presence) { 
			Log.debug("Got presence: " + packet.toXML());
		}
	}

	public void processIncomingRaw(String rawText) {
	};

	public void terminate() {
		bot = null;
		chatManager = null;
		userManager = null;
		roomMapping.clear();
		roomMapping = null;
	};

}
