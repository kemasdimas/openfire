package org.jivesoftware.openfire.plugin.userService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jivesoftware.database.DbConnectionManager;

public final class UserServiceDB {

	private static UserServiceDB instance;
	private static final String GET_ALL_STATUS_MESSAGE = "SELECT userId, statusmessage FROM wgLegacyAgent";
	private static final String GET_IM_STATUS = "SELECT statusmessage FROM wgLegacyAgent WHERE userId = ? LIMIT 1";
	private static final String UPDATE_IM_STATUS = "UPDATE wgLegacyAgent SET statusmessage = ? WHERE userId = ?";
	private static final String GET_WORKGROUP_MAPPING = "SELECT a.userId, w.workgroupID, w.jid FROM " +
			"(SELECT * FROM wgLegacyAgent WHERE userId = ?) a JOIN fpWorkgroup w on a.workgroupId = w.workgroupID LIMIT 1";
	private static final String GET_LOGIN_CACHE = "SELECT * FROM wgLoginCache";
	private static final String INSERT_LOGIN_CACHE = "INSERT INTO wgLoginCache (username) VALUES (?)";
	private static final String DELETE_LOGIN_CACHE = "DELETE FROM wgLoginCache WHERE username = ?";
	
	private UserServiceDB() {
	}
	
	public static UserServiceDB getInstance() {
		if (instance == null) {
			instance = new UserServiceDB();
		}
		
		return instance;
	}
	
	public ArrayList<String> fetchCachedLogin() {
		ArrayList<String> buffer = new ArrayList<String>();
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DbConnectionManager.getConnection();
			statement = con.prepareStatement(GET_LOGIN_CACHE);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				buffer.add(result.getString(1));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnectionManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public void addLoginCache(String userId) {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(INSERT_LOGIN_CACHE);
            pstmt.setString(1, userId);
            
            pstmt.executeUpdate();
        } catch (Exception e) {
        } finally {
        	DbConnectionManager.closeConnection(pstmt, con);
        }
    }
	
	public void deleteLoginCache(String userId) {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(DELETE_LOGIN_CACHE);
            pstmt.setString(1, userId);
            
            pstmt.executeUpdate();
        } catch (Exception e) {
        } finally {
        	DbConnectionManager.closeConnection(pstmt, con);
        }
    }
	
	public String getWorkgroupName(final String userId) {
		String workgroupName = null;
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DbConnectionManager.getConnection();
			statement = con.prepareStatement(GET_WORKGROUP_MAPPING);
			statement.setString(1, userId);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				workgroupName = result.getString(3);
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			workgroupName = null;
		} finally {
			DbConnectionManager.closeConnection(statement, con);
		}
		
		return workgroupName;
	}
	
	public Map<String, String> fetchIMsStatus() {
		HashMap<String, String> buffer = new HashMap<String, String>(100);
		
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DbConnectionManager.getConnection();
			statement = con.prepareStatement(GET_ALL_STATUS_MESSAGE);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				buffer.put(result.getString(1), result.getString(2));
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnectionManager.closeConnection(statement, con);
		}
		
		return buffer;
	}
	
	public String getStatusMessage(final String userId) {
		String statusMessage = null;
		Connection con = null;
		PreparedStatement statement = null;
		
		try {
			con = DbConnectionManager.getConnection();
			statement = con.prepareStatement(GET_IM_STATUS);
			statement.setString(1, userId);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				statusMessage = result.getString(1);
			}
			
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			statusMessage = "";
		} finally {
			DbConnectionManager.closeConnection(statement, con);
		}
		
		return statusMessage;
	}
	
	public void saveStatusMessage(final String userId, String statusMessage) {
		Connection con = null;
        PreparedStatement pstmt = null;
        
        // if statusMessage length > 250, trim it!
        if (statusMessage.length() > 250) {
        	statusMessage = statusMessage.substring(0, 249);
        }
        
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(UPDATE_IM_STATUS);
            pstmt.setString(1, statusMessage);
            pstmt.setString(2, userId);
            
            pstmt.executeUpdate();
        } catch (Exception e) {
        } finally {
        	DbConnectionManager.closeConnection(pstmt, con);
        }
	}
	
}
