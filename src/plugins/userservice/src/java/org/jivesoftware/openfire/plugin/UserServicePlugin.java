/**
 * $Revision: 1722 $
 * $Date: 2005-07-28 15:19:16 -0700 (Thu, 28 Jul 2005) $
 *
 * Copyright (C) 2005-2008 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jivesoftware.openfire.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimerTask;

import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.group.Group;
import org.jivesoftware.openfire.group.GroupManager;
import org.jivesoftware.openfire.group.GroupNotFoundException;
import org.jivesoftware.openfire.lockout.LockOutManager;
import org.jivesoftware.openfire.plugin.userService.UserServiceDB;
import org.jivesoftware.openfire.plugin.userService.UserServiceRobot;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserAlreadyExistsException;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.Log;
import org.jivesoftware.util.PropertyEventDispatcher;
import org.jivesoftware.util.PropertyEventListener;
import org.jivesoftware.util.StringUtils;
import org.jivesoftware.util.TaskEngine;
import org.json.simple.JSONObject;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.JID;
import org.xmpp.packet.Presence;

import com.kemasdimas.bots.BotzConnection;
import com.kemasdimas.bots.BotzSessionAlreadyExistsException;

/**
 * Plugin that allows the administration of users via HTTP requests.
 *
 * @author Justin Hunt
 */
public class UserServicePlugin implements Plugin, PropertyEventListener {
    private UserManager userManager;
    private XMPPServer server;
    private String secret;
    private boolean enabled;
    private Collection<String> allowedIPs;
    private TimerTask autoLoginTask;
    private TimerTask statusUpdaterTask;
    
    public static String PARENT_PROPERTY_NAME = "clustering.isParent";
    private Map<String, BotzConnection> botConnectionMap = new HashMap<String, BotzConnection>();
    
    public void initializePlugin(PluginManager manager, File pluginDirectory) {
    	System.out.println("Starting UserService Plugin");
    	
        server = XMPPServer.getInstance();
        userManager = server.getUserManager();
        ComponentManagerFactory.getComponentManager().getServerName();
        
        secret = JiveGlobals.getProperty("plugin.userservice.secret", "");
        // If no secret key has been assigned to the user service yet, assign a random one.
        if (secret.equals("")){
            secret = StringUtils.randomString(8);
            setSecret(secret);
        }
        
        // See if the service is enabled or not.
        enabled = JiveGlobals.getBooleanProperty("plugin.userservice.enabled", false);

        // Get the list of IP addresses that can use this service. An empty list means that this filter is disabled.
        allowedIPs = StringUtils.stringToCollection(JiveGlobals.getProperty("plugin.userservice.allowedIPs", ""));

        // Listen to system property events
        PropertyEventDispatcher.addListener(this);
        
        // KEMAS DIMAS
        // Create a thread to autologin all user that resides in wgLoginCache
        autoLoginTask = new TimerTask() {
        	final boolean isParent = isParentNode();
        	boolean isFirstRun = true;
        	
			@Override
			public void run() {
				userAutoLogin(isParent, isFirstRun);
				isFirstRun = false;
			}
		};
		
		statusUpdaterTask = new TimerTask() {
			@Override
			public void run() {
				batchStatusChange();
			}
		};
		
		// Postpone login for 30s
		TaskEngine.getInstance().schedule(autoLoginTask, 1000 * 30);
		
		// Schedule status update every 5 minutes
		final int statusUpdateInterval = 5 * 60 * 1000;
		TaskEngine.getInstance().schedule(statusUpdaterTask, statusUpdateInterval, statusUpdateInterval);
    }

    private void userAutoLogin(boolean isParent, boolean isFirstRun) {
    	ArrayList<String> users = UserServiceDB.getInstance().fetchCachedLogin();
		String wgName;
		for (String username : users) {
			wgName = username.split("-")[1];
			
			try {
				// Due to disabled clustering, conditional below is obsolete
				// removed (isParent && isFirstRun) || !isLoggedIn(username)
				// instead, it only need to check first run flag
				if ((isFirstRun)) {
					loginUser(username, wgName);
				}
			} catch (Exception e) {
			} 
		}
    }
    
    public void destroyPlugin() {
        userManager = null;
        
        // Stop listening to system property events
        PropertyEventDispatcher.removeListener(this);
    }

    private void batchStatusChange() {
    	Map<String, String> statuses = UserServiceDB.getInstance().fetchIMsStatus();
    	
    	for (String username : statuses.keySet()) {
    		BotzConnection bot = botConnectionMap.get(username);
    		
    		if (bot != null) {
    			String statusMessage = statuses.get(username);
    		
    			Presence online = new Presence();
    	    	online.setPriority(1);
    	    	online.setStatus(statusMessage);
    	    	
    	    	bot.sendPacket(online);
    		}		
    	}
    	
    	System.out.println("Batch IM status update...");
    }
    
    public String getIMStatus(String username, String wgName) {
    	username = prepareUsername(username, wgName);
    	
    	String statusMessage = UserServiceDB.getInstance().getStatusMessage(username);
    	
		return statusMessage;
    }
    
    public void changeIMStatus(String username, String wgName, String statusMessage) 
    		throws UserNotFoundException {
    	
    	username = prepareUsername(username, wgName);
    	BotzConnection bot = botConnectionMap.get(username);
    	
    	if (bot != null) {
    		// if statusMessage is null, use previous status message
    		if (statusMessage == null) {
    			statusMessage = UserServiceDB.getInstance().getStatusMessage(username);
    		} else {
    			// Store status message to database
    			UserServiceDB.getInstance().saveStatusMessage(username, statusMessage);
    		}
    		
	    	Presence online = new Presence();
	    	online.setPriority(1);
	    	online.setStatus(statusMessage);
	    	
	    	bot.sendPacket(online);
    	} else {
    		UserServiceDB.getInstance().saveStatusMessage(username, statusMessage);
    	}
    }
    
    public void createUser(String username, String password, String name, String email, String groupNames)
            throws UserAlreadyExistsException
    {
        userManager.createUser(username, password, name, email);
        
        if (groupNames != null) {
            Collection<Group> groups = new ArrayList<Group>();
            StringTokenizer tkn = new StringTokenizer(groupNames, ",");
            while (tkn.hasMoreTokens()) {
                try {
                    groups.add(GroupManager.getInstance().getGroup(tkn.nextToken()));
                } catch (GroupNotFoundException e) {
                    // Ignore this group
                }
            }
            for (Group group : groups) {
                group.getMembers().add(server.createJID(username, null));
            }
        }
    }
    
    private boolean isLoggedIn(String username) {
    	JID jid = server.createJID(username, "UserSvcBot");
    	return (server.getSessionManager().getSession(jid) != null);
    }
    
    /**
     * KEMAS DIMAS
     * 
     * @param username
     * @throws UserNotFoundException
     * @throws BotzSessionAlreadyExistsException 
     */
    public void loginUser(String username, String wgName) 
    		throws UserNotFoundException, BotzSessionAlreadyExistsException, Exception {
    	
    	username = prepareUsername(username, wgName);
    	
    	// if user is disabled, return
    	if (LockOutManager.getInstance().isAccountDisabled(username)) {
    		return;
    	}
    	
    	// check whether the user is exist
    	userManager.getUser(username);
    	if (!isLoggedIn(username)) {
        	BotzConnection bot = new BotzConnection(new UserServiceRobot());
        	bot.login(username, "UserSvcBot");
        	
        	Presence online = new Presence();
        	online.setPriority(1);
        	online.setStatus("");
        	bot.sendPacket(online);
        	
        	// Add botz connection to map
        	botConnectionMap.put(username, bot);
        	
        	// When using SPECTRUM as KRAKEN replacement, userservice must manually
        	// send the presence to SPECTRUM external component
        	
//        	Matcher m = pattern.matcher(username);
//        	if (m.find()) {
//        		String transportType = m.group(1);
//            	Presence transportOnline = online.createCopy();
//            	transportOnline.setTo(transportType + "." + componenParent);
//            	bot.sendPacket(transportOnline);
//        	}
        	
        	UserServiceDB.getInstance().addLoginCache(username);
    	} else {
    		try {
				logoutUser(username, wgName);
			} catch (Exception e) {
			} finally {
				loginUser(username, wgName);
			}
    	}
    }
    
    /**
     * KEMAS DIMAS
     * 
     * @param username
     * @param wgName
     * @throws UserNotFoundException
     */
    public void logoutUser(String username, String wgName) 
    		throws UserNotFoundException {
    	
		JID jid = server.createJID(prepareUsername(username, wgName), "UserSvcBot");
		server.getSessionManager().getSession(jid).close();
		
		UserServiceDB.getInstance().deleteLoginCache(username);
		
		botConnectionMap.remove(username);
    }
    
    private String prepareUsername(String username, String wgName) {
    	
    	return username.split("-")[0] + "-" + wgName;
    }
    
    public void deleteUser(String username) throws UserNotFoundException{
        User user = getUser(username);
        userManager.deleteUser(user);
    }

    /**
     * Lock Out on a given username
     *
     * @param username the username of the local user to disable.
     * @throws UserNotFoundException if the requested user
     *         does not exist in the local server.
     */
    public void disableUser(String username) throws UserNotFoundException
    {
        User user = getUser(username);
        LockOutManager.getInstance().disableAccount(username, null, null);
    }

    /**
     * Remove the lockout on a given username
     *
     * @param username the username of the local user to enable.
     * @throws UserNotFoundException if the requested user
     *         does not exist in the local server.
     */
    public void enableUser(String username) throws UserNotFoundException
    {
        User user = getUser(username);
        LockOutManager.getInstance().enableAccount(username);
    }
    
    public void updateUser(String username, String password, String name, String email, String groupNames)
            throws UserNotFoundException
    {
        User user = getUser(username);
        if (password != null) user.setPassword(password);
        if (name != null) user.setName(name);
        if (email != null) user.setEmail(email);

        if (groupNames != null) {
            Collection<Group> newGroups = new ArrayList<Group>();
            StringTokenizer tkn = new StringTokenizer(groupNames, ",");
            while (tkn.hasMoreTokens()) {
                try {
                    newGroups.add(GroupManager.getInstance().getGroup(tkn.nextToken()));
                } catch (GroupNotFoundException e) {
                    // Ignore this group
                }
            }

            Collection<Group> existingGroups = GroupManager.getInstance().getGroups(user);
            // Get the list of groups to add to the user
            Collection<Group> groupsToAdd =  new ArrayList<Group>(newGroups);
            groupsToAdd.removeAll(existingGroups);
            // Get the list of groups to remove from the user
            Collection<Group> groupsToDelete =  new ArrayList<Group>(existingGroups);
            groupsToDelete.removeAll(newGroups);

            // Add the user to the new groups
            for (Group group : groupsToAdd) {
                group.getMembers().add(server.createJID(username, null));
            }
            // Remove the user from the old groups
            for (Group group : groupsToDelete) {
                group.getMembers().remove(server.createJID(username, null));
            }
        }
    }
    
    /**
     * Returns the the requested user or <tt>null</tt> if there are any
     * problems that don't throw an error.
     *
     * @param username the username of the local user to retrieve.
     * @return the requested user.
     * @throws UserNotFoundException if the requested user
     *         does not exist in the local server.
     */
    private User getUser(String username) throws UserNotFoundException {
        JID targetJID = server.createJID(username, null);
        // Check that the sender is not requesting information of a remote server entity
        if (targetJID.getNode() == null) {
            // Sender is requesting presence information of an anonymous user
            throw new UserNotFoundException("Username is null");
        }
        return userManager.getUser(targetJID.getNode());
    }
    
    /**
     * Returns the secret key that only valid requests should know.
     *
     * @return the secret key.
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Sets the secret key that grants permission to use the userservice.
     *
     * @param secret the secret key.
     */
    public void setSecret(String secret) {
        JiveGlobals.setProperty("plugin.userservice.secret", secret);
        this.secret = secret;
    }

    public Collection<String> getAllowedIPs() {
        return allowedIPs;
    }

    public void setAllowedIPs(Collection<String> allowedIPs) {
        JiveGlobals.setProperty("plugin.userservice.allowedIPs", StringUtils.collectionToString(allowedIPs));
        this.allowedIPs = allowedIPs;
    }

    /**
     * Returns true if the user service is enabled. If not enabled, it will not accept
     * requests to create new accounts.
     *
     * @return true if the user service is enabled.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Enables or disables the user service. If not enabled, it will not accept
     * requests to create new accounts.
     *
     * @param enabled true if the user service should be enabled.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        JiveGlobals.setProperty("plugin.userservice.enabled",  enabled ? "true" : "false");
    }

    public void propertySet(String property, Map<String, Object> params) {
        if (property.equals("plugin.userservice.secret")) {
            this.secret = (String)params.get("value");
        }
        else if (property.equals("plugin.userservice.enabled")) {
            this.enabled = Boolean.parseBoolean((String)params.get("value"));
        }
        else if (property.equals("plugin.userservice.allowedIPs")) {
            this.allowedIPs = StringUtils.stringToCollection((String)params.get("value"));
        }
    }

    public void propertyDeleted(String property, Map<String, Object> params) {
        if (property.equals("plugin.userservice.secret")) {
            this.secret = "";
        }
        else if (property.equals("plugin.userservice.enabled")) {
            this.enabled = false;
        }
        else if (property.equals("plugin.userservice.allowedIPs")) {
            this.allowedIPs = Collections.emptyList();
        }
    }

    public void xmlPropertySet(String property, Map<String, Object> params) {
        // Do nothing
    }

    public void xmlPropertyDeleted(String property, Map<String, Object> params) {
        // Do nothing
    }
    
    public boolean isParentNode() {
        return JiveGlobals.getXMLProperty(PARENT_PROPERTY_NAME, false);
    }
}
